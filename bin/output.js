var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CommonWindow = (function (_super) {
    __extends(CommonWindow, _super);
    function CommonWindow(game, soundManager) {
        var _this = _super.call(this, game) || this;
        _this.soundManager = soundManager;
        _this.initialize();
        return _this;
    }
    CommonWindow.prototype.initialize = function () {
        var _this = this;
        this.lobbyGroup = this.game.add.group(this);
        this.lobbyGroup.visible = false;
        this.gameGroup = this.game.add.group(this);
        this.switchGameSceneSignal = new Phaser.Signal();
        this.requestDataSignal = new Phaser.Signal();
        this.userBalance = 0;
        this.userBalanceText = this.game.add.bitmapText(330, 87, 'BetNumber', this.userBalance + "", 34, this);
        this.userBalanceText.anchor.set(0.5);
        this.userNameText = this.game.add.text(150, 15, "", { font: "30px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.userTotalBetGameText = this.game.add.bitmapText(660, 1045, 'BetNumber', "", 34, this.gameGroup);
        this.userTotalBetGameText.anchor.set(0.5);
        this.userTotalBetLobbyText = this.game.add.bitmapText(147, 1045, 'BetNumber', "", 34, this.lobbyGroup);
        this.userTotalBetLobbyText.anchor.set(0.5);
        this.userIcon = new SpriteButton(this.game, 28, 15, 'Lobby', 'UserIcon');
        this.add(this.userIcon);
        var gameRuleButton = new SpriteButton(this.game, 1480, 10, 'Button', 'HelpButton');
        this.add(gameRuleButton);
        var mailButton = new SpriteButton(this.game, 1635, 10, 'Button', 'MailButton');
        this.add(mailButton);
        var gameSettingButton = new SpriteButton(this.game, 1790, 10, 'Button', 'ListButton');
        this.add(gameSettingButton);
        var historicalLotteryButton = new SpriteButton(this.game, 1360, 983, 'Button', 'HistoricalLotteryButton');
        this.gameGroup.add(historicalLotteryButton);
        var betRecordButton = new SpriteButton(this.game, 1640, 983, 'Button', 'BetRecordButton');
        this.gameGroup.add(betRecordButton);
        var openBetPage = new SpriteButton(this.game, 40, 150, 'Button', 'BetPageButton');
        openBetPage.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        openBetPage.OnUp.add(function () {
            _this.switchGameSceneSignal.dispatch(false);
        }, this);
        this.gameGroup.add(openBetPage);
        var returnGame = new SpriteButton(this.game, 1720, 975, 'Button', 'ReturnGame');
        returnGame.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        returnGame.OnUp.add(function () {
            _this.switchGameSceneSignal.dispatch(true);
        }, this);
        this.lobbyGroup.add(returnGame);
        this.panelMask = this.game.add.graphics(0, 0, this);
        this.panelMask.beginFill(0x000000, 0.5);
        this.panelMask.drawRect(0, 0, 1920, 1080);
        this.panelMask.endFill();
        this.panelMask.inputEnabled = true;
        this.panelMask.visible = false;
        var userMessageGroup = this.game.add.group(this);
        userMessageGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, userMessageGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'UserMessageTitle', userMessageGroup).anchor.set(0.5);
        var userMessageClose = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        userMessageClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        userMessageClose.OnUp.add(function () {
            userMessageGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        userMessageGroup.add(userMessageClose);
        this.userIcon.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.userIcon.OnUp.add(function () {
            userMessageGroup.visible = true;
            _this.panelMask.visible = true;
        }, this);
        this.game.add.sprite(960, 650, 'Lobby', 'UserMessageLine_2', userMessageGroup).anchor.set(0.5);
        this.game.add.sprite(670, 400, 'Lobby', 'UserMessageIconFrame', userMessageGroup);
        this.game.add.sprite(870, 515, 'Lobby', 'UserMessageCoinFrame', userMessageGroup);
        this.game.add.sprite(870, 515, 'Lobby', 'Coin_Icon', userMessageGroup);
        this.userMessageIcon = this.game.add.sprite(676, 410, 'Lobby', 'UserIcon', userMessageGroup);
        this.userMessageIcon.scale.set(1.6);
        this.userMessageNameText = this.game.add.text(940, 410, "", { font: "48px Microsoft JhengHei", fill: "#ffffff" }, userMessageGroup);
        this.userMessageBalanceText = this.game.add.bitmapText(1070, 547, 'BetNumber', this.userBalance + "", 34, userMessageGroup);
        this.userMessageBalanceText.anchor.set(0.5);
        this.logOutSignal = new Phaser.Signal;
        var userMessageSwitchUserButton = new SpriteButton(this.game, 760, 760, 'Button', 'SwitchUser');
        userMessageSwitchUserButton.anchor.set(0.5);
        userMessageGroup.add(userMessageSwitchUserButton);
        userMessageSwitchUserButton.OnUp.add(function () {
            userMessageGroup.visible = false;
            _this.panelMask.visible = false;
            _this.logOutSignal.dispatch();
        }, this);
        var passwordChangeButton = new SpriteButton(this.game, 1160, 760, 'Button', 'PasswordChangeButton');
        passwordChangeButton.anchor.set(0.5);
        userMessageGroup.add(passwordChangeButton);
        passwordChangeButton.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        passwordChangeButton.OnUp.add(function () {
            userMessageGroup.visible = false;
            _this.passwordChangeGroup.visible = true;
        }, this);
        this.passwordChangeGroup = this.game.add.group(this);
        this.passwordChangeGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, this.passwordChangeGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'PasswordChangeTitle', this.passwordChangeGroup).anchor.set(0.5);
        var passwordChangePanelClose = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        passwordChangePanelClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        passwordChangePanelClose.OnUp.add(function () {
            _this.passwordChangeGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        this.passwordChangeGroup.add(passwordChangePanelClose);
        this.game.add.text(820, 360, "請輸入舊密碼:", { font: "50px Microsoft JhengHei", fill: "#ffffff" }, this.passwordChangeGroup).anchor.set(1, 0);
        this.game.add.sprite(850, 345, 'Button', 'DropDownButton_2', this.passwordChangeGroup);
        var oldPasswordInput = this.game.add.inputField(865, 355, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 480,
            cursorColor: '#ffffff',
            textAlign: 'left',
            max: "12",
            type: PhaserInput.InputType.password
        }, this.passwordChangeGroup);
        this.game.add.text(820, 490, "請輸入新密碼:", { font: "50px Microsoft JhengHei", fill: "#ffffff" }, this.passwordChangeGroup).anchor.set(1, 0);
        this.game.add.sprite(850, 475, 'Button', 'DropDownButton_2', this.passwordChangeGroup);
        var newPasswordInput = this.game.add.inputField(865, 485, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 480,
            cursorColor: '#ffffff',
            textAlign: 'left',
            max: "12",
            type: PhaserInput.InputType.password
        }, this.passwordChangeGroup);
        this.game.add.text(820, 620, "確認新密碼:", { font: "50px Microsoft JhengHei", fill: "#ffffff" }, this.passwordChangeGroup).anchor.set(1, 0);
        this.game.add.sprite(850, 605, 'Button', 'DropDownButton_2', this.passwordChangeGroup);
        var newPasswordConfirmInput = this.game.add.inputField(865, 615, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 480,
            cursorColor: '#ffffff',
            textAlign: 'left',
            max: "12",
            type: PhaserInput.InputType.password
        }, this.passwordChangeGroup);
        var confirmpasswordChangeButton = new SpriteButton(this.game, 960, 780, 'Button', 'ConfirmBet');
        confirmpasswordChangeButton.anchor.set(0.5);
        this.passwordChangeGroup.add(confirmpasswordChangeButton);
        confirmpasswordChangeButton.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        confirmpasswordChangeButton.OnUp.add(function () {
            if (oldPasswordInput.value == "" || newPasswordInput.value == "" || newPasswordConfirmInput.value == "") {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "密碼修改選項不能為空" }));
                return;
            }
            else if (newPasswordInput.value.length < 5 || newPasswordConfirmInput.value.length < 5) {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "密碼長度不得小於5" }));
            }
            else if (newPasswordInput.value != newPasswordConfirmInput.value) {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "兩次新密碼輸入不一樣，請重新輸入" }));
                newPasswordInput.resetText();
                newPasswordConfirmInput.resetText();
                return;
            }
            var postData = {
                strPwd: oldPasswordInput.value,
                strPwdN: newPasswordInput.value
            };
            _this.requestDataSignal.dispatch(DATA_URL.PASSWORD_CHANGE, JSON.stringify(postData));
            oldPasswordInput.resetText();
            newPasswordInput.resetText();
            newPasswordConfirmInput.resetText();
        }, this);
        var gameRuleGroup = this.game.add.group(this);
        gameRuleGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_1', null, gameRuleGroup).anchor.set(0.5);
        this.game.add.sprite(960, 90, 'Lobby', 'GameRuleTitle', gameRuleGroup).anchor.set(0.5);
        var gameRuleClose = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        gameRuleClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameRuleClose.OnUp.add(function () {
            gameRuleGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        gameRuleGroup.add(gameRuleClose);
        gameRuleButton.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameRuleButton.OnUp.add(function () {
            gameRuleGroup.visible = true;
            _this.panelMask.visible = true;
        }, this);
        var mailGroup = this.game.add.group(this);
        mailGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, mailGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'mailTitle', mailGroup).anchor.set(0.5);
        this.game.add.text(960, 400, "Line ID", { font: "bold 60px Arial", fill: "#ffffff" }, mailGroup).anchor.set(0.5);
        var mailClose = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        mailClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        mailClose.OnUp.add(function () {
            mailGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        mailGroup.add(mailClose);
        mailButton.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        mailButton.OnUp.add(function () {
            mailGroup.visible = true;
            _this.panelMask.visible = true;
        }, this);
        var gameSettingGroup = this.game.add.group(this);
        gameSettingGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, gameSettingGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'GameSettingTitle', gameSettingGroup).anchor.set(0.5);
        var gameSettingClose = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        gameSettingClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameSettingClose.OnUp.add(function () {
            gameSettingGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        gameSettingGroup.add(gameSettingClose);
        gameSettingButton.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameSettingButton.OnUp.add(function () {
            gameSettingGroup.visible = true;
            _this.panelMask.visible = true;
        }, this);
        this.game.add.text(550, 410, "音樂", { font: "38px Microsoft JhengHei", fill: "#ffffff" }, gameSettingGroup);
        this.game.add.sprite(700, 412, 'Lobby', 'GameSettingBar_Empty', gameSettingGroup);
        var musicFullBG = this.game.add.sprite(700, 412, 'Lobby', 'GameSettingBar_Full', gameSettingGroup);
        var musicMask = this.game.add.graphics(700, 400, gameSettingGroup);
        musicMask.beginFill(0xffffff, 0);
        musicMask.drawRect(0, 0, 634, 70);
        musicMask.endFill();
        musicFullBG.mask = musicMask;
        var musicPoint = this.game.add.sprite(1315, 433, 'Lobby', 'GameSettingPoint', gameSettingGroup);
        musicPoint.anchor.set(0.5);
        var musicTimerEvent;
        var musicTouch = this.game.add.graphics(700, 412, gameSettingGroup);
        musicTouch.beginFill(0x000000, 0);
        musicTouch.drawRect(0, 0, 634, 45);
        musicTouch.endFill();
        musicTouch.inputEnabled = true;
        musicTouch.events.onInputDown.add(function () {
            musicTimerEvent = _this.game.time.events.loop(20, function () {
                var inputX = _this.game.input.x;
                if (inputX > 1315) {
                    inputX = 1315;
                }
                if (inputX < 715) {
                    inputX = 715;
                }
                var newVolume = (inputX - 715) / 600;
                musicMask.scale.set(newVolume, 1);
                musicPoint.position.set(inputX, 433);
                _this.soundManager.changeSoundVolume(newVolume);
            }, _this);
        }, this);
        musicTouch.events.onInputUp.add(function () {
            _this.game.time.events.remove(musicTimerEvent);
        }, this);
        this.game.add.text(550, 550, "音效", { font: "38px Microsoft JhengHei", fill: "#ffffff" }, gameSettingGroup);
        this.game.add.sprite(700, 552, 'Lobby', 'GameSettingBar_Empty', gameSettingGroup);
        var soundEffectFullBG = this.game.add.sprite(700, 552, 'Lobby', 'GameSettingBar_Full', gameSettingGroup);
        var soundEffectMask = this.game.add.graphics(700, 552, gameSettingGroup);
        soundEffectMask.beginFill(0xffffff, 0);
        soundEffectMask.drawRect(0, 0, 634, 45);
        soundEffectMask.endFill();
        soundEffectFullBG.mask = soundEffectMask;
        var soundEffectPoint = this.game.add.sprite(1315, 573, 'Lobby', 'GameSettingPoint', gameSettingGroup);
        soundEffectPoint.anchor.set(0.5);
        var soundEffectTimerEvent;
        var soundEffectTouch = this.game.add.graphics(700, 540, gameSettingGroup);
        soundEffectTouch.beginFill(0x000000, 0);
        soundEffectTouch.drawRect(0, 0, 634, 70);
        soundEffectTouch.endFill();
        soundEffectTouch.inputEnabled = true;
        soundEffectTouch.events.onInputDown.add(function () {
            soundEffectTimerEvent = _this.game.time.events.loop(20, function () {
                var inputX = _this.game.input.x;
                if (inputX > 1315) {
                    inputX = 1315;
                }
                if (inputX < 715) {
                    inputX = 715;
                }
                var newVolume = (inputX - 715) / 600;
                soundEffectMask.scale.set(newVolume, 1);
                soundEffectPoint.position.set(inputX, 573);
                _this.soundManager.changeSoundEffectVolume(newVolume);
            }, _this);
        }, this);
        soundEffectTouch.events.onInputUp.add(function () {
            _this.game.time.events.remove(soundEffectTimerEvent);
        }, this);
        var specialEffectSwitch = true;
        this.game.add.text(550, 690, "特效", { font: "38px Microsoft JhengHei", fill: "#ffffff" }, gameSettingGroup);
        var specialEffectOption = this.game.add.sprite(700, 677, 'Lobby', 'GameSettingOption_On', gameSettingGroup);
        var specialEffectPoint = this.game.add.sprite(830, 665, 'Lobby', 'GameSettingPoint', gameSettingGroup);
        var specialEffectText = this.game.add.text(755, 692, "ON", { font: "bold 38px Microsoft JhengHei", fill: "#ffffff" }, gameSettingGroup);
        var specialEffectTouch = this.game.add.graphics(700, 677, gameSettingGroup);
        specialEffectTouch.beginFill(0xffffff, 0);
        specialEffectTouch.drawRect(0, 0, 202, 77);
        specialEffectTouch.endFill();
        specialEffectTouch.inputEnabled = true;
        specialEffectTouch.events.onInputUp.add(function () {
            specialEffectSwitch = !specialEffectSwitch;
            specialEffectOption.loadTexture('Lobby', specialEffectSwitch ? 'GameSettingOption_On' : 'GameSettingOption_Off');
            specialEffectPoint.position.set(specialEffectSwitch ? 830 : 670, 665);
            specialEffectText.position.set(specialEffectSwitch ? 755 : 785, 692);
            specialEffectText.setText(specialEffectSwitch ? "ON" : "OFF");
        }, this);
        this.historicalLotteryGroup = this.game.add.group(this);
        this.historicalLotteryGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_1', null, this.historicalLotteryGroup).anchor.set(0.5);
        this.game.add.sprite(960, 90, 'Lobby', 'HistoricalLotteryTitle', this.historicalLotteryGroup).anchor.set(0.5);
        var historicalLotteryClose = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        historicalLotteryClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        historicalLotteryClose.OnUp.add(function () {
            _this.historicalLotteryGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        this.historicalLotteryGroup.add(historicalLotteryClose);
        historicalLotteryButton.OnDown.add(function () {
            var date = new Date();
            var month = date.getMonth() + 1;
            var monthString = month < 10 ? "0" + month : "" + month;
            var day = date.getDate();
            var dayString = day < 10 ? "0" + day : "" + day;
            var postData = {
                dateOpen: date.getFullYear() + "-" + monthString + "-" + dayString
            };
            _this.requestDataSignal.dispatch(DATA_URL.OPEN_HISTORY, JSON.stringify(postData));
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.game.add.sprite(960, 220, 'Lobby', 'HistoricalLotteryFrame_Top', this.historicalLotteryGroup).anchor.set(0.5);
        this.game.add.text(330, 195, "時間", { font: "42px Microsoft JhengHei", fill: "#f2e200" }, this.historicalLotteryGroup);
        this.game.add.text(820, 195, "開獎號碼", { font: "42px Microsoft JhengHei", fill: "#f2e200" }, this.historicalLotteryGroup);
        this.game.add.text(1235, 195, "冠亞軍和", { font: "42px Microsoft JhengHei", fill: "#f2e200" }, this.historicalLotteryGroup);
        this.game.add.text(1600, 195, "龍虎", { font: "42px Microsoft JhengHei", fill: "#f2e200" }, this.historicalLotteryGroup);
        this.historicalLotteryFormScrollGroup = this.game.add.group(this.historicalLotteryGroup);
        var historicalLotteryScrollMask = this.game.add.graphics(86, 259, this.historicalLotteryGroup);
        historicalLotteryScrollMask.beginFill(0xffffff, 0);
        historicalLotteryScrollMask.drawRect(0, 0, 1748, 742);
        historicalLotteryScrollMask.endFill();
        historicalLotteryScrollMask.inputEnabled = true;
        historicalLotteryScrollMask.events.onInputDown.add(this.startHistoricalLotteryFormScroll, this);
        historicalLotteryScrollMask.events.onInputUp.add(this.stopHistoricalLotteryFormScroll, this);
        historicalLotteryScrollMask.events.onInputOut.add(this.stopHistoricalLotteryFormScroll, this);
        this.historicalLotteryFormScrollGroup.mask = historicalLotteryScrollMask;
        var betRecordGroup = this.game.add.group(this);
        betRecordGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_1', null, betRecordGroup).anchor.set(0.5);
        this.game.add.sprite(960, 90, 'Lobby', 'BettingRecordTitle', betRecordGroup).anchor.set(0.5);
        this.betRecordFormScrollGroup = this.game.add.group(betRecordGroup);
        var betRecordFormGroup = this.game.add.group(betRecordGroup);
        var betRecordYearOptionGroup = this.game.add.group(betRecordGroup);
        betRecordYearOptionGroup.visible = false;
        var betRecordMonthOptionGroup = this.game.add.group(betRecordGroup);
        betRecordMonthOptionGroup.visible = false;
        var betRecordDayOptionGroup = this.game.add.group(betRecordGroup);
        betRecordDayOptionGroup.visible = false;
        var betRecordClose = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        betRecordClose.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordClose.OnUp.add(function () {
            betRecordYearOptionGroup.visible = false;
            betRecordYearArrow.angle = 0;
            betRecordMonthOptionGroup.visible = false;
            betRecordMonthArrow.angle = 0;
            betRecordDayOptionGroup.visible = false;
            betRecordDayArrow.angle = 0;
            betRecordGroup.visible = false;
            _this.panelMask.visible = false;
        }, this);
        betRecordGroup.add(betRecordClose);
        betRecordButton.OnDown.add(function () {
            _this.updateBetRecordYearOption([date.getFullYear() - 1, date.getFullYear()]);
            var postData = {
                strDate: betRecordCurrentYear.text + "-" + betRecordCurrentMonth.text + "-" + betRecordCurrentDay.text
            };
            _this.requestDataSignal.dispatch(DATA_URL.DAY_BET_RECORD, JSON.stringify(postData));
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordButton.OnUp.add(function () {
            betRecordGroup.visible = true;
            _this.panelMask.visible = true;
        }, this);
        var date = new Date();
        var betRecordYearArrow = this.game.add.sprite(470, 225, 'Button', 'DropDownArrow', betRecordGroup);
        betRecordYearArrow.anchor.set(0.5);
        var betRecordYearButton = new SpriteButton(this.game, 265, 188, 'Button', 'DropDownButton_1');
        betRecordYearButton.OnDown.add(function () {
            betRecordMonthOptionGroup.visible = false;
            betRecordDayOptionGroup.visible = false;
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordYearButton.OnUp.add(function () {
            betRecordYearOptionGroup.visible = !betRecordYearOptionGroup.visible;
            betRecordYearArrow.angle = betRecordYearOptionGroup.visible ? 180 : 0;
        }, this);
        betRecordGroup.add(betRecordYearButton);
        this.game.add.text(87, 202, "西元年", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        this.game.add.text(220, 199, ":", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        var betRecordCurrentYear = this.game.add.text(320, 202, date.getFullYear() + "", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        this.betRecordYearOptionBG = new Array(10);
        this.betRecordYearOptionButton = new Array(10);
        this.betRecordYearOptionText = new Array(10);
        var _loop_1 = function (i) {
            this_1.betRecordYearOptionBG[i] = this_1.game.add.sprite(265, 271 + 66 * i, 'Button', 'DropDownBG_1', betRecordYearOptionGroup);
            this_1.betRecordYearOptionBG[i].visible = false;
            this_1.betRecordYearOptionButton[i] = new SpriteButton(this_1.game, 265, 271 + 66 * i, 'Button', 'DropDownBG_1', {
                Key: 'Button',
                OverFrame: 'DropDownSelected_1',
                OutFrame: 'DropDownBG_1',
                UpFrame: 'DropDownBG_1',
                DownFrame: 'DropDownSelected_1'
            });
            this_1.betRecordYearOptionButton[i].OnDown.add(function () {
                betRecordCurrentMonth.setText("");
                betRecordCurrentDay.setText("");
                _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this_1);
            this_1.betRecordYearOptionButton[i].OnUp.add(function () {
                betRecordYearOptionGroup.visible = false;
                betRecordYearArrow.angle = 0;
                betRecordCurrentYear.setText(_this.betRecordYearOptionText[i].text);
            }, this_1);
            this_1.betRecordYearOptionButton[i].visible = false;
            betRecordYearOptionGroup.add(this_1.betRecordYearOptionButton[i]);
            this_1.betRecordYearOptionText[i] = this_1.game.add.text(385, 307 + 66 * i, "", { font: "38px Microsoft JhengHei", fill: "#ffffff" }, betRecordYearOptionGroup);
            this_1.betRecordYearOptionText[i].anchor.set(0.5);
            this_1.betRecordYearOptionText[i].visible = false;
        };
        var this_1 = this;
        for (var i = 0; i < 10; i++) {
            _loop_1(i);
        }
        var betRecordMonthArrow = this.game.add.sprite(870, 225, 'Button', 'DropDownArrow', betRecordGroup);
        betRecordMonthArrow.anchor.set(0.5);
        var betRecordMonthButton = new SpriteButton(this.game, 665, 188, 'Button', 'DropDownButton_1');
        betRecordMonthButton.OnDown.add(function () {
            betRecordYearOptionGroup.visible = false;
            betRecordDayOptionGroup.visible = false;
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordMonthButton.OnUp.add(function () {
            betRecordMonthOptionGroup.visible = !betRecordMonthOptionGroup.visible;
            betRecordMonthArrow.angle = betRecordMonthOptionGroup.visible ? 180 : 0;
        }, this);
        betRecordGroup.add(betRecordMonthButton);
        this.game.add.text(570, 202, "月", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        this.game.add.text(620, 199, ":", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        var betRecordCurrentMonth = this.game.add.text(750, 202, ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1), { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        var betRecordMonthOptionBG = new Array(12);
        var betRecordMonthOptionButton = new Array(12);
        var betRecordMonthOptionText = new Array(12);
        var _loop_2 = function (i) {
            betRecordMonthOptionBG[i] = this_2.game.add.sprite(665, 271 + 66 * i, 'Button', 'DropDownBG_1', betRecordMonthOptionGroup);
            betRecordMonthOptionButton[i] = new SpriteButton(this_2.game, 665, 271 + 66 * i, 'Button', 'DropDownBG_1', {
                Key: 'Button',
                OverFrame: 'DropDownSelected_1',
                OutFrame: 'DropDownBG_1',
                UpFrame: 'DropDownBG_1',
                DownFrame: 'DropDownSelected_1'
            });
            betRecordMonthOptionButton[i].OnDown.add(function () {
                betRecordCurrentDay.setText("");
                _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this_2);
            betRecordMonthOptionButton[i].OnUp.add(function () {
                betRecordMonthOptionGroup.visible = false;
                betRecordMonthArrow.angle = 0;
                if (betRecordCurrentYear.text == "") {
                    window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇西元年" }));
                    return;
                }
                var dayLength = new Date(parseInt(betRecordCurrentYear.text), i + 1, 0).getDate();
                for (var day = 0; day < 31; day++) {
                    betRecordDayOptionBG[day].visible = day < dayLength ? true : false;
                    betRecordDayOptionButton[day].visible = day < dayLength ? true : false;
                    betRecordDayOptionText[day].visible = day < dayLength ? true : false;
                }
                betRecordCurrentMonth.setText(betRecordMonthOptionText[i].text);
            }, this_2);
            betRecordMonthOptionGroup.add(betRecordMonthOptionButton[i]);
            betRecordMonthOptionText[i] = this_2.game.add.text(785, 307 + 66 * i, (i < 9 ? "0" : "") + (i + 1), { font: "38px Microsoft JhengHei", fill: "#ffffff" }, betRecordMonthOptionGroup);
            betRecordMonthOptionText[i].anchor.set(0.5);
        };
        var this_2 = this;
        for (var i = 0; i < 12; i++) {
            _loop_2(i);
        }
        var betRecordDayArrow = this.game.add.sprite(1260, 225, 'Button', 'DropDownArrow', betRecordGroup);
        betRecordDayArrow.anchor.set(0.5);
        var betRecordDayButton = new SpriteButton(this.game, 1055, 188, 'Button', 'DropDownButton_1');
        betRecordDayButton.OnDown.add(function () {
            betRecordYearOptionGroup.visible = false;
            betRecordMonthOptionGroup.visible = false;
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordDayButton.OnUp.add(function () {
            betRecordDayOptionGroup.visible = !betRecordDayOptionGroup.visible;
            betRecordDayArrow.angle = betRecordDayOptionGroup.visible ? 180 : 0;
        }, this);
        betRecordGroup.add(betRecordDayButton);
        this.game.add.text(960, 202, "日", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        this.game.add.text(1010, 199, ":", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        var betRecordCurrentDay = this.game.add.text(1140, 202, (date.getDate() < 10 ? "0" : "") + date.getDate(), { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordGroup);
        var betRecordDayOptionBG = new Array(31);
        var betRecordDayOptionButton = new Array(31);
        var betRecordDayOptionText = new Array(31);
        var _loop_3 = function (i) {
            betRecordDayOptionBG[i] = this_3.game.add.sprite(1055 + 238 * (Math.floor(i / 11)), 271 + 66 * (i % 11), 'Button', 'DropDownBG_1', betRecordDayOptionGroup);
            betRecordDayOptionButton[i] = new SpriteButton(this_3.game, 1055 + 238 * (Math.floor(i / 11)), 271 + 66 * (i % 11), 'Button', 'DropDownBG_1', {
                Key: 'Button',
                OverFrame: 'DropDownSelected_1',
                OutFrame: 'DropDownBG_1',
                UpFrame: 'DropDownBG_1',
                DownFrame: 'DropDownSelected_1'
            });
            betRecordDayOptionButton[i].OnDown.add(function () {
                _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this_3);
            betRecordDayOptionButton[i].OnUp.add(function () {
                betRecordDayOptionGroup.visible = false;
                betRecordDayArrow.angle = 0;
                betRecordCurrentDay.setText(betRecordDayOptionText[i].text);
            }, this_3);
            betRecordDayOptionGroup.add(betRecordDayOptionButton[i]);
            betRecordDayOptionText[i] = this_3.game.add.text(1175 + 238 * (Math.floor(i / 11)), 307 + 66 * (i % 11), (i < 9 ? "0" : "") + (i + 1), { font: "38px Microsoft JhengHei", fill: "#ffffff" }, betRecordDayOptionGroup);
            betRecordDayOptionText[i].anchor.set(0.5);
        };
        var this_3 = this;
        for (var i = 0; i < 31; i++) {
            _loop_3(i);
        }
        var betRecordSearchButton = this.game.add.sprite(1350, 175, 'Button', 'ConfirmBet', betRecordGroup);
        betRecordSearchButton.inputEnabled = true;
        betRecordSearchButton.events.onInputDown.add(function () {
            if (betRecordCurrentYear.text == "" || betRecordCurrentMonth.text == "" || betRecordCurrentDay.text == "") {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇查詢的年／月／日" }));
                return;
            }
            var postData = {
                strDate: betRecordCurrentYear.text + "-" + betRecordCurrentMonth.text + "-" + betRecordCurrentDay.text
            };
            _this.requestDataSignal.dispatch(DATA_URL.DAY_BET_RECORD, JSON.stringify(postData));
            betRecordSearchButton.tint = 0x666666;
        }, this);
        betRecordSearchButton.events.onInputUp.add(function () {
            betRecordSearchButton.tint = 0xffffff;
        }, this);
        var betRecordTitleString = ["玩法", "組合", "賠率", "投注金額", "派彩金額"];
        this.game.add.sprite(960, 340, 'Lobby', 'BettingRecordFrame_Top', betRecordFormGroup).anchor.set(0.5);
        this.game.add.sprite(960, 972, 'Lobby', 'BettingRecordFrame_Bottom', betRecordFormGroup).anchor.set(0.5);
        for (var i = 0; i < 5; i++) {
            this.game.add.text(260 + 348 * i, 345, betRecordTitleString[i], { font: "42px Microsoft JhengHei", fill: "#f2e200" }, betRecordFormGroup).anchor.set(0.5);
        }
        this.game.add.text(260, 975, "合計", { font: "42px Microsoft JhengHei", fill: "#f2e200" }, betRecordFormGroup).anchor.set(0.5);
        this.betRecordTotalBetText = this.game.add.text(1304, 975, "", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordFormGroup);
        this.betRecordTotalBetText.anchor.set(0.5);
        this.betRecordTotalWinText = this.game.add.text(1652, 975, "", { font: "42px Microsoft JhengHei", fill: "#ffffff" }, betRecordFormGroup);
        this.betRecordTotalWinText.anchor.set(0.5);
        var betRecordScrollMask = this.game.add.graphics(86, 377, betRecordFormGroup);
        betRecordScrollMask.beginFill(0xffffff, 0);
        betRecordScrollMask.drawRect(0, 0, 1747, 558);
        betRecordScrollMask.endFill();
        betRecordScrollMask.inputEnabled = true;
        betRecordScrollMask.events.onInputDown.add(this.startBetRecordFormScroll, this);
        betRecordScrollMask.events.onInputUp.add(this.stopBetRecordFormScroll, this);
        betRecordScrollMask.events.onInputOut.add(this.stopBetRecordFormScroll, this);
        this.betRecordFormScrollGroup.mask = betRecordScrollMask;
    };
    CommonWindow.prototype.updateUserName = function (name) {
        this.userNameText.setText(name);
        this.userMessageNameText.setText(name);
    };
    CommonWindow.prototype.updateUserBalance = function (balance) {
        this.userBalance = balance;
        this.userBalanceText.setText(this.userBalance.toFixed(2));
        this.userMessageBalanceText.setText(this.userBalance.toFixed(2));
    };
    CommonWindow.prototype.updateUserTotalBet = function (userTotalBet) {
        this.userTotalBetGameText.setText(userTotalBet);
        this.userTotalBetLobbyText.setText(userTotalBet);
    };
    CommonWindow.prototype.checkUserBetIsSuccessed = function (bet) {
        return bet <= this.userBalance ? true : false;
    };
    CommonWindow.prototype.updateBetRecordYearOption = function (years) {
        var periodsLength = years.length;
        for (var i = 0; i < 10; i++) {
            this.betRecordYearOptionBG[i].visible = i < periodsLength ? true : false;
            this.betRecordYearOptionButton[i].visible = i < periodsLength ? true : false;
            this.betRecordYearOptionText[i].visible = i < periodsLength ? true : false;
            this.betRecordYearOptionText[i].setText(i < periodsLength ? years[i] + "" : "");
        }
    };
    CommonWindow.prototype.showBetRecord = function (data) {
        var rowLength = data.length;
        var formLength = rowLength > 7 ? rowLength : 7;
        var totalBet = 0;
        var totalWin = 0;
        this.betRecordFormScrollGroup.children.length = 0;
        this.betRecordFormScrollGroup.position.set(0);
        this.betRecordFormScrollLimit = rowLength > 7 ? -(rowLength - 7) * 79 : 0;
        for (var rowIndex = 0; rowIndex < formLength; rowIndex++) {
            this.game.add.sprite(960, 419 + 79 * rowIndex, 'Lobby', 'BettingRecordFrame_Middle', this.betRecordFormScrollGroup).anchor.set(0.5);
            if (rowIndex < rowLength) {
                var playName = "";
                var combination = "";
                var firstByte = data[rowIndex].str_bet_type.split("_")[0];
                var secondByte = data[rowIndex].str_bet_type.split("_")[1];
                switch (firstByte) {
                    case "1p2":
                        if (secondByte.split("").length == 3) {
                            playName = "冠亞";
                            firstByte = secondByte.split("n")[0];
                            secondByte = secondByte.split("n")[1];
                            combination = "冠" + firstByte + ", 亞" + secondByte;
                        }
                        else {
                            playName = "冠亞和";
                            switch (secondByte) {
                                case "b":
                                    combination = "大";
                                    break;
                                case "s":
                                    combination = "小";
                                    break;
                                case "o":
                                    combination = "單";
                                    break;
                                case "e":
                                    combination = "雙";
                                    break;
                                default:
                                    combination = secondByte;
                                    break;
                            }
                        }
                        break;
                    case "1p2p3":
                        playName = "冠亞季";
                        var thirdByte = secondByte.split("n")[2];
                        firstByte = secondByte.split("n")[0];
                        secondByte = secondByte.split("n")[1];
                        combination = "冠" + firstByte + ", 亞" + secondByte + ", 季" + thirdByte;
                        break;
                    default:
                        if (isNaN(parseInt(secondByte))) {
                            playName = "大小單雙";
                            combination = "第" + firstByte + "名";
                            switch (secondByte) {
                                case "b":
                                    combination += "大";
                                    break;
                                case "s":
                                    combination += "小";
                                    break;
                                case "o":
                                    combination += "單";
                                    break;
                                case "e":
                                    combination += "雙";
                                    break;
                                case "d":
                                    combination += "龍";
                                    break;
                                case "t":
                                    combination += "虎";
                                    break;
                            }
                        }
                        else {
                            playName = "名次";
                            combination = "第" + firstByte + "名" + secondByte + "號";
                        }
                        break;
                }
                this.game.add.text(260, 422 + 79 * rowIndex, playName, { font: "42px Microsoft JhengHei", fill: "#ffffff" }, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(608, 422 + 79 * rowIndex, combination, { font: "42px Microsoft JhengHei", fill: "#ffffff" }, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(956, 422 + 79 * rowIndex, data[rowIndex].num_rate, { font: "42px Microsoft JhengHei", fill: "#ffffff" }, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(1304, 422 + 79 * rowIndex, data[rowIndex].num_bet, { font: "42px Microsoft JhengHei", fill: "#ffffff" }, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(1652, 422 + 79 * rowIndex, (data[rowIndex].num_result > 0 ? "+" : "") + data[rowIndex].num_result, { font: "42px Microsoft JhengHei", fill: data[rowIndex].num_result > 0 ? "#00ff0d" : "#ff0019" }, this.betRecordFormScrollGroup).anchor.set(0.5);
                totalBet += parseFloat(data[rowIndex].num_bet);
                totalWin += parseFloat(data[rowIndex].num_result);
            }
        }
        this.betRecordTotalBetText.setText(totalBet.toFixed(4));
        this.betRecordTotalWinText.setText(totalWin.toFixed(4));
    };
    CommonWindow.prototype.startBetRecordFormScroll = function () {
        var _this = this;
        if (this.betRecordFormScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.betRecordFormScrollGroup.y;
        this.betRecordFormScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.betRecordFormScrollLimit) {
                scrollPosY = _this.betRecordFormScrollLimit;
            }
            _this.betRecordFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    CommonWindow.prototype.stopBetRecordFormScroll = function () {
        this.game.time.events.remove(this.betRecordFormScrollTimerEvent);
    };
    CommonWindow.prototype.openHistoricalLotteryForm = function (data) {
        if (data.length == 0) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "今日尚未開獎" }));
            return;
        }
        this.historicalLotteryGroup.visible = true;
        this.panelMask.visible = true;
        var formLength = data.length;
        this.historicalLotteryFormScrollGroup.children.length = 0;
        this.historicalLotteryFormScrollGroup.position.set(0);
        this.historicalLotteryFormScrollLimit = formLength > 9 ? -(formLength - 9) * 82 : 0;
        for (var i = 0; i < formLength; i++) {
            this.game.add.sprite(960, 302 + 82 * i, 'Lobby', i < formLength - 1 ? 'HistoricalLotteryFrame_Middle' : 'HistoricalLotteryFrame_Bottom', this.historicalLotteryFormScrollGroup).anchor.set(0.5);
            this.game.add.text(100, 283 + 82 * i, data[i].periods + "期", { font: "28px Microsoft JhengHei", fill: "#ffffff" }, this.historicalLotteryFormScrollGroup);
            this.game.add.text(628, 283 + 82 * i, data[i].time, { font: "28px Microsoft JhengHei", fill: "#ffffff" }, this.historicalLotteryFormScrollGroup).anchor.set(1, 0);
            for (var j = 0; j < 8; j++) {
                this.game.add.sprite(650 + 65 * j, 275 + 82 * i, 'Lobby', 'TodayNumber_' + (data[i].result[j]), this.historicalLotteryFormScrollGroup);
                if (j < 3) {
                    var firstSecondSumDataString = void 0;
                    switch (j) {
                        case 0:
                            firstSecondSumDataString = data[i].firstSecondSum + "";
                            break;
                        case 1:
                            firstSecondSumDataString = data[i].firstSecondSum > 9 ? "大" : "小";
                            break;
                        case 2:
                            firstSecondSumDataString = data[i].firstSecondSum % 2 != 0 ? "單" : "雙";
                            break;
                    }
                    this.game.add.text(1220 + 93 * j, 304 + 82 * i, firstSecondSumDataString, { font: "34px Microsoft JhengHei", fill: "#ffffff" }, this.historicalLotteryFormScrollGroup).anchor.set(0.5);
                }
                if (j < 4) {
                    this.game.add.text(1485 + 94 * j, 279 + 82 * i, data[i].dragonTiger[j] == "d" ? "龍" : "虎", { font: "34px Microsoft JhengHei", fill: "#ffffff" }, this.historicalLotteryFormScrollGroup);
                }
            }
        }
    };
    CommonWindow.prototype.startHistoricalLotteryFormScroll = function () {
        var _this = this;
        if (this.historicalLotteryFormScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.historicalLotteryFormScrollGroup.y;
        this.historicalLotteryFormScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.historicalLotteryFormScrollLimit) {
                scrollPosY = _this.historicalLotteryFormScrollLimit;
            }
            _this.historicalLotteryFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    CommonWindow.prototype.stopHistoricalLotteryFormScroll = function () {
        this.game.time.events.remove(this.historicalLotteryFormScrollTimerEvent);
    };
    CommonWindow.prototype.updateMissingNumberRankingData = function (data) {
        this.missingNumberRankingData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            this.missingNumberRankingData[rankIndex] = new Array(8);
            for (var rowIndex = 0; rowIndex < 8; rowIndex++) {
                this.missingNumberRankingData[rankIndex][rowIndex] = new Array(8);
                for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                    this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] = valueIndex < 7 ? data[rankIndex][rowIndex][valueIndex] : rowIndex + 1;
                }
            }
        }
        this.changeMissingNumberRankingSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    };
    CommonWindow.prototype.changeMissingNumberRankingSort = function (rankIndex, sortIndex) {
        this.changeMissingNumberArrow(sortIndex);
        this.missingNumberRankingData[rankIndex].sort(function (a, b) {
            if (b[sortIndex] == a[sortIndex]) {
                return a[7] - b[7];
            }
            else {
                return b[sortIndex] - a[sortIndex];
            }
        });
        for (var rowIndex = 0; rowIndex < 8; rowIndex++) {
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                if (valueIndex == 0) {
                    this.missingNumberRankingDataText[rowIndex][valueIndex].setText((this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberRankingData[rankIndex][rowIndex][valueIndex]));
                    this.missingNumberRankingDataText[rowIndex][valueIndex].tint = this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] > 0 ? 0x00ff0d : 0xff0019;
                }
                else if (valueIndex < 7) {
                    this.missingNumberRankingDataText[rowIndex][valueIndex].setText(this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] + "");
                }
                else {
                    this.missingNumberRankingHorseNumber[rowIndex].loadTexture('Lobby', 'TodayNumber_0' + this.missingNumberRankingData[rankIndex][rowIndex][valueIndex]);
                }
            }
        }
    };
    CommonWindow.prototype.updateMissingNumberSingleDoubleData = function (data) {
        this.missingNumberSingleDoubleData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            this.missingNumberSingleDoubleData[rankIndex] = new Array(2);
            for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
                this.missingNumberSingleDoubleData[rankIndex][rowIndex] = new Array(8);
                for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                    this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex] = valueIndex < 7 ? data[rankIndex][rowIndex][valueIndex] : rowIndex;
                }
            }
        }
        this.changeMissingNumberSingleDoubleSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    };
    CommonWindow.prototype.changeMissingNumberSingleDoubleSort = function (rankIndex, sortIndex) {
        this.changeMissingNumberArrow(sortIndex);
        this.missingNumberSingleDoubleData[rankIndex].sort(function (a, b) {
            if (b[sortIndex] == a[sortIndex]) {
                return a[7] - b[7];
            }
            else {
                return b[sortIndex] - a[sortIndex];
            }
        });
        var singleDoubleString = ["單", "雙"];
        for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                if (valueIndex == 0) {
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].setText(singleDoubleString[this.missingNumberSingleDoubleData[rankIndex][rowIndex][7]]);
                }
                else if (valueIndex == 1) {
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].setText((this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1]));
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].tint = this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }
                else {
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].setText(this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1] + "");
                }
            }
        }
    };
    CommonWindow.prototype.updateMissingNumberBigSmallData = function (data) {
        this.missingNumberBigSmallData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            this.missingNumberBigSmallData[rankIndex] = new Array(2);
            for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
                this.missingNumberBigSmallData[rankIndex][rowIndex] = new Array(8);
                for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                    this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex] = valueIndex < 7 ? data[rankIndex][rowIndex][valueIndex] : rowIndex;
                }
            }
        }
        this.changeMissingNumberBigSmallSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    };
    CommonWindow.prototype.changeMissingNumberBigSmallSort = function (rankIndex, sortIndex) {
        this.changeMissingNumberArrow(sortIndex);
        this.missingNumberBigSmallData[rankIndex].sort(function (a, b) {
            if (b[sortIndex] == a[sortIndex]) {
                return a[7] - b[7];
            }
            else {
                return b[sortIndex] - a[sortIndex];
            }
        });
        var bigSmallString = ["大", "小"];
        for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                if (valueIndex == 0) {
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].setText(bigSmallString[this.missingNumberBigSmallData[rankIndex][rowIndex][7]]);
                }
                else if (valueIndex == 1) {
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].setText((this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1]));
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].tint = this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }
                else {
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].setText(this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1] + "");
                }
            }
        }
    };
    CommonWindow.prototype.updateMissingNumberDragonTigerData = function (data) {
        this.missingNumberDragonTigerData = new Array(8);
        for (var rowIndex = 0; rowIndex < 8; rowIndex++) {
            this.missingNumberDragonTigerData[rowIndex] = new Array(8);
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                this.missingNumberDragonTigerData[rowIndex][valueIndex] = valueIndex < 7 ? data[rowIndex][valueIndex] : rowIndex;
            }
        }
        this.changeMissingNumberDragonTigerSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    };
    CommonWindow.prototype.changeMissingNumberDragonTigerSort = function (sortIndex) {
        this.changeMissingNumberArrow(sortIndex);
        this.missingNumberDragonTigerData.sort(function (a, b) {
            if (b[sortIndex] == a[sortIndex]) {
                return a[7] - b[7];
            }
            else {
                return b[sortIndex] - a[sortIndex];
            }
        });
        var dragonTigerString = ["冠軍龍", "冠軍虎", "亞軍龍", "亞軍虎", "季軍龍", "季軍虎", "第4名龍", "第4名虎"];
        for (var rowIndex = 0; rowIndex < 8; rowIndex++) {
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                if (valueIndex == 0) {
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].setText(dragonTigerString[this.missingNumberDragonTigerData[rowIndex][7]]);
                }
                else if (valueIndex == 1) {
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].setText((this.missingNumberDragonTigerData[rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberDragonTigerData[rowIndex][valueIndex - 1]));
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].tint = this.missingNumberDragonTigerData[rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }
                else {
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].setText(this.missingNumberDragonTigerData[rowIndex][valueIndex - 1] + "");
                }
            }
        }
    };
    CommonWindow.prototype.updateMissingNumberFirstSecondSumSingleDoubleData = function (data) {
        this.missingNumberFirstSecondSumSingleDoubleData = new Array(8);
        for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
            this.missingNumberFirstSecondSumSingleDoubleData[rowIndex] = new Array(2);
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex] = valueIndex < 7 ? data[rowIndex][valueIndex] : rowIndex;
            }
        }
        this.changeMissingNumberFirstSecondSumSingleDoubleSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    };
    CommonWindow.prototype.changeMissingNumberFirstSecondSumSingleDoubleSort = function (sortIndex) {
        this.changeMissingNumberArrow(sortIndex);
        this.missingNumberFirstSecondSumSingleDoubleData.sort(function (a, b) {
            if (b[sortIndex] == a[sortIndex]) {
                return a[7] - b[7];
            }
            else {
                return b[sortIndex] - a[sortIndex];
            }
        });
        var firstSecondSumSingleDoubleString = ["冠亞和單", "冠亞和雙"];
        for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                if (valueIndex == 0) {
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].setText(firstSecondSumSingleDoubleString[this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][7]]);
                }
                else if (valueIndex == 1) {
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].setText((this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1]));
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].tint = this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }
                else {
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].setText(this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1] + "");
                }
            }
        }
    };
    CommonWindow.prototype.updateMissingNumberFirstSecondSumBigSmallData = function (data) {
        this.missingNumberFirstSecondSumBigSmallData = new Array(8);
        for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
            this.missingNumberFirstSecondSumBigSmallData[rowIndex] = new Array(2);
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex] = valueIndex < 7 ? data[rowIndex][valueIndex] : rowIndex;
            }
        }
        this.changeMissingNumberFirstSecondSumBigSmallSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    };
    CommonWindow.prototype.changeMissingNumberFirstSecondSumBigSmallSort = function (sortIndex) {
        this.changeMissingNumberArrow(sortIndex);
        this.missingNumberFirstSecondSumBigSmallData.sort(function (a, b) {
            if (b[sortIndex] == a[sortIndex]) {
                return a[7] - b[7];
            }
            else {
                return b[sortIndex] - a[sortIndex];
            }
        });
        var firstSecondSumBigSmallString = ["冠亞和大", "冠亞和小"];
        for (var rowIndex = 0; rowIndex < 2; rowIndex++) {
            for (var valueIndex = 0; valueIndex < 8; valueIndex++) {
                if (valueIndex == 0) {
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].setText(firstSecondSumBigSmallString[this.missingNumberFirstSecondSumBigSmallData[rowIndex][7]]);
                }
                else if (valueIndex == 1) {
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].setText((this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1]));
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].tint = this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }
                else {
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].setText(this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1] + "");
                }
            }
        }
    };
    CommonWindow.prototype.changeMissingNumberArrow = function (sortIndex) {
        for (var i = 0; i < 7; i++) {
            this.missingNumberArrow[i].tint = i == sortIndex ? 0xff0019 : 0x00ff0d;
            this.missingNumberArrow[i].angle = i == sortIndex ? 0 : 180;
        }
    };
    CommonWindow.prototype.updateTodayNumberData = function (data) {
        this.todayNumberData = new Array(16);
        for (var i = 0; i < 16; i++) {
            this.todayNumberData[i] = new Array(8);
            var dataIndex = Math.floor(i / 2);
            for (var j = 0; j < 8; j++) {
                this.todayNumberData[i][j] = (i % 2 == 0) ? data[dataIndex].Win[j] : data[dataIndex].Lose[j];
                this.todayNumberDataText[i][j].setText(this.todayNumberData[i][j] + "");
                this.todayNumberDataText[i][j].tint = 0xffffff;
            }
        }
    };
    CommonWindow.prototype.changeTodayNumberTextColor = function (firstMin, firstMax, secondMin, secondMax, thirdMin, thirdMax) {
        for (var i = 0; i < 16; i++) {
            for (var j = 0; j < 8; j++) {
                if (this.todayNumberData[i][j] >= firstMin && this.todayNumberData[i][j] <= firstMax) {
                    this.todayNumberDataText[i][j].tint = 0xff0019;
                }
                else if (this.todayNumberData[i][j] >= secondMin && this.todayNumberData[i][j] <= secondMax) {
                    this.todayNumberDataText[i][j].tint = 0x0062ff;
                }
                else if (this.todayNumberData[i][j] >= thirdMin && this.todayNumberData[i][j] <= thirdMax) {
                    this.todayNumberDataText[i][j].tint = 0x00ff0d;
                }
                else {
                    this.todayNumberDataText[i][j].tint = 0xffffff;
                }
            }
        }
    };
    CommonWindow.prototype.updateDailyDragonBigLongDragonData = function (data) {
        var formLength = new Array(8);
        this.dailyDragonBigLongDragonData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            var periodsLength = new Array(7);
            this.dailyDragonBigLongDragonData[rankIndex] = new Array(7);
            for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonBigLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            for (var formIndex = 0; formIndex < formLength[rankIndex]; formIndex++) {
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", { font: "36px Microsoft JhengHei", fill: "#f2e200" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonBigLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.updateDailyDragonSmallLongDragonData = function (data) {
        var formLength = new Array(8);
        this.dailyDragonSmallLongDragonData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            var periodsLength = new Array(7);
            this.dailyDragonSmallLongDragonData[rankIndex] = new Array(7);
            for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonSmallLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            for (var formIndex = 0; formIndex < formLength[rankIndex]; formIndex++) {
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", { font: "36px Microsoft JhengHei", fill: "#f2e200" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonSmallLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.updateDailyDragonSingleLongDragonData = function (data) {
        var formLength = new Array(8);
        this.dailyDragonSingleLongDragonData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            var periodsLength = new Array(7);
            this.dailyDragonSingleLongDragonData[rankIndex] = new Array(7);
            for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonSingleLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            for (var formIndex = 0; formIndex < formLength[rankIndex]; formIndex++) {
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", { font: "36px Microsoft JhengHei", fill: "#f2e200" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonSingleLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.updateDailyDragonDoubleLongDragonData = function (data) {
        var formLength = new Array(8);
        this.dailyDragonDoubleLongDragonData = new Array(8);
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            var periodsLength = new Array(7);
            this.dailyDragonDoubleLongDragonData[rankIndex] = new Array(7);
            for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonDoubleLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            for (var formIndex = 0; formIndex < formLength[rankIndex]; formIndex++) {
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", { font: "36px Microsoft JhengHei", fill: "#f2e200" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonDoubleLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.updateDailyDragonDragonLongDragonData = function (data) {
        var formLength = new Array(4);
        this.dailyDragonDragonLongDragonData = new Array(4);
        for (var rankIndex = 0; rankIndex < 4; rankIndex++) {
            var periodsLength = new Array(7);
            this.dailyDragonDragonLongDragonData[rankIndex] = new Array(7);
            for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonDragonLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
        for (var rankIndex = 0; rankIndex < 4; rankIndex++) {
            for (var formIndex = 0; formIndex < formLength[rankIndex]; formIndex++) {
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", { font: "36px Microsoft JhengHei", fill: "#f2e200" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonDragonLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.updateDailyDragonTigerLongDragonData = function (data) {
        var formLength = new Array(4);
        this.dailyDragonTigerLongDragonData = new Array(4);
        for (var rankIndex = 0; rankIndex < 4; rankIndex++) {
            var periodsLength = new Array(7);
            this.dailyDragonTigerLongDragonData[rankIndex] = new Array(7);
            for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonTigerLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
        for (var rankIndex = 0; rankIndex < 8; rankIndex++) {
            for (var formIndex = 0; formIndex < formLength[rankIndex]; formIndex++) {
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", { font: "36px Microsoft JhengHei", fill: "#f2e200" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonTigerLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.changeDailyDragonFormDateText = function (sortIndex, rankIndex) {
        for (var rowIndex = 0; rowIndex < 7; rowIndex++) {
            switch (sortIndex) {
                case DAILY_DRAGON_PAGE.BIG_LONG_DRAGON:
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonBigLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON:
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonSmallLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON:
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonSingleLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON:
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonDoubleLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON:
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonDragonLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON:
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonTigerLongDragonData[rankIndex][rowIndex].date);
                    break;
            }
        }
    };
    CommonWindow.prototype.startDailyDragonFormScroll = function (sortIndex, rankIndex) {
        var _this = this;
        if (this.dailyDragonFormScrollLimit[sortIndex][rankIndex] == 0) {
            return;
        }
        var firstTouchPosX = this.game.input.x;
        var scrollPosX = this.dailyDragonFormScrollGroup[sortIndex][rankIndex].x;
        this.dailyDragonFormScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosX -= firstTouchPosX - _this.game.input.x;
            if (scrollPosX > 0) {
                scrollPosX = 0;
            }
            if (scrollPosX < _this.dailyDragonFormScrollLimit[sortIndex][rankIndex]) {
                scrollPosX = _this.dailyDragonFormScrollLimit[sortIndex][rankIndex];
            }
            _this.dailyDragonFormScrollGroup[sortIndex][rankIndex].position.set(scrollPosX, 0);
        }, this);
    };
    CommonWindow.prototype.stopDailyDragonFormScroll = function () {
        this.game.time.events.remove(this.dailyDragonFormScrollTimerEvent);
    };
    CommonWindow.prototype.updateTwoSideFormData = function (data) {
        this.twoSideFormData = data;
    };
    CommonWindow.prototype.changeTwoSideForm = function (formIndex) {
        var dataLength = this.twoSideFormData[formIndex].length;
        this.twoSideFormScrollGroup.children.length = 0;
        this.twoSideFormScrollGroup.position.set(0);
        this.twoSideFormScrollLimit = dataLength > 8 ? -(dataLength - 8) * 70 : 0;
        for (var i = 0; i < dataLength; i++) {
            this.game.add.sprite(960, 496 + 70 * i, 'Lobby', i < dataLength - 1 ? 'TwoSideFrame_Middle' : 'TwoSideFrame_Bottom', this.twoSideFormScrollGroup).anchor.set(0.5);
            for (var j = 0; j < 5; j++) {
                if (j == 0) {
                    this.game.add.text(297, 500 + 70 * i, this.twoSideFormData[formIndex][j].date, { font: "30px Microsoft JhengHei", fill: "#ffffff" }, this.twoSideFormScrollGroup).anchor.set(0.5);
                }
                else {
                    this.game.add.text(670 + 333 * (j - 1), 500 + 70 * i, this.twoSideFormData[formIndex][j].values[j - 1] + "", { font: "30px Microsoft JhengHei", fill: "#ffffff" }, this.twoSideFormScrollGroup).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.startTwoSideFormScroll = function () {
        var _this = this;
        if (this.twoSideFormScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.twoSideFormScrollGroup.y;
        this.twoSideFormScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.twoSideFormScrollLimit) {
                scrollPosY = _this.twoSideFormScrollLimit;
            }
            _this.twoSideFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    CommonWindow.prototype.stopTwoSideFormScroll = function () {
        this.game.time.events.remove(this.twoSideFormScrollTimerEvent);
    };
    CommonWindow.prototype.updateSingleDoubleFormData = function (data) {
        this.singleDoubleFormData = data;
    };
    CommonWindow.prototype.changeSingleDoubleForm = function (formIndex) {
        var dataLength = this.singleDoubleFormData[formIndex].length;
        this.singleDoubleFormScrollGroup.children.length = 0;
        this.singleDoubleFormScrollGroup.position.set(0);
        this.singleDoubleFormScrollLimit = dataLength > 8 ? -(dataLength - 8) * 70 : 0;
        for (var i = 0; i < dataLength; i++) {
            this.game.add.sprite(960, 496 + 70 * i, 'Lobby', i < dataLength - 1 ? 'TwoSideFrame_Middle' : 'TwoSideFrame_Bottom', this.singleDoubleFormScrollGroup).anchor.set(0.5);
            for (var j = 0; j < 5; j++) {
                if (j == 0) {
                    this.game.add.text(297, 500 + 70 * i, this.singleDoubleFormData[formIndex][j].date, { font: "30px Microsoft JhengHei", fill: "#ffffff" }, this.singleDoubleFormScrollGroup).anchor.set(0.5);
                }
                else {
                    this.game.add.text(670 + 333 * (j - 1), 500 + 70 * i, this.singleDoubleFormData[formIndex][j].values[j - 1] + "", { font: "30px Microsoft JhengHei", fill: "#ffffff" }, this.singleDoubleFormScrollGroup).anchor.set(0.5);
                }
            }
        }
    };
    CommonWindow.prototype.startSingleDoubleFormScroll = function () {
        var _this = this;
        if (this.singleDoubleFormScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.singleDoubleFormScrollGroup.y;
        this.singleDoubleFormScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.singleDoubleFormScrollLimit) {
                scrollPosY = _this.singleDoubleFormScrollLimit;
            }
            _this.singleDoubleFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    CommonWindow.prototype.stopSingleDoubleFormScroll = function () {
        this.game.time.events.remove(this.singleDoubleFormScrollTimerEvent);
    };
    CommonWindow.prototype.updateFirstSecondSumForm = function (data) {
        var dataLength = data.length;
        this.firstSecondSumFormScrollGroup.children.length = 0;
        this.firstSecondSumFormScrollGroup.position.set(0);
        this.firstSecondSumFormScrollLimit = dataLength > 9 ? -(dataLength - 9) * 70 : 0;
        for (var i = 0; i < dataLength; i++) {
            this.game.add.sprite(960, 406 + 70 * i, 'Lobby', i < dataLength - 1 ? 'FirstSecondSumFrame_Middle' : 'FirstSecondSumFrame_Bottom', this.firstSecondSumFormScrollGroup).anchor.set(0.5);
            for (var j = 0; j < 5; j++) {
                this.game.add.text(263 + 350 * j, 410 + 70 * i, j == 0 ? data[i].date : data[i].values[j - 1] + "", { font: "36px Microsoft JhengHei", fill: "#ffffff" }, this.firstSecondSumFormScrollGroup).anchor.set(0.5);
            }
        }
    };
    CommonWindow.prototype.startFirstSecondSumFormScroll = function () {
        var _this = this;
        if (this.firstSecondSumFormScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.firstSecondSumFormScrollGroup.y;
        this.firstSecondSumFormScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.firstSecondSumFormScrollLimit) {
                scrollPosY = _this.firstSecondSumFormScrollLimit;
            }
            _this.firstSecondSumFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    CommonWindow.prototype.stopFirstSecondSumFormScroll = function () {
        this.game.time.events.remove(this.firstSecondSumFormScrollTimerEvent);
    };
    CommonWindow.prototype.changeStatPage = function (targetSpriteButton, targetGroup) {
        this.currentSelectedStatPage.ResetButton();
        this.currentSelectedStatPage = targetSpriteButton;
        this.currentSelectedStatPage.SelectButton();
        this.currentShowGroup.visible = false;
        this.currentShowGroup = targetGroup;
        this.currentShowGroup.visible = true;
    };
    return CommonWindow;
}(Phaser.Group));
var DATA_URL;
(function (DATA_URL) {
    DATA_URL["CHECK_LOGIN"] = "MemberActs/chkLogin";
    DATA_URL["MEMBER_LOGIN"] = "Member/login";
    DATA_URL["MEMBER_LOGOUT"] = "MemberActs/logout";
    DATA_URL["GAME_LIST"] = "MemberActs/gameList";
    DATA_URL["GAME_RESULT"] = "MemberActs/gameResult/";
    DATA_URL["GAME_INFO"] = "MemberActs/gameInfo/";
    DATA_URL["GAME_BET"] = "MemberActs/gameBet/";
    DATA_URL["DAY_BET_RECORD"] = "MemberActs/dayBetRec/";
    DATA_URL["OPEN_HISTORY"] = "MemberActs/openHis/";
    DATA_URL["BET_DATA"] = "MemberActs/betData/";
    DATA_URL["GET_USER_INFO"] = "MemberActs/userInfo";
    DATA_URL["PASSWORD_CHANGE"] = "MemberActs/pwdChg/";
})(DATA_URL || (DATA_URL = {}));
var BET_PAGE;
(function (BET_PAGE) {
    BET_PAGE[BET_PAGE["RANKING"] = 0] = "RANKING";
    BET_PAGE[BET_PAGE["BIG_SMALL_SINGLE_DOUBLE"] = 1] = "BIG_SMALL_SINGLE_DOUBLE";
    BET_PAGE[BET_PAGE["FIRST_SECOND_SUM"] = 2] = "FIRST_SECOND_SUM";
    BET_PAGE[BET_PAGE["FIRST_SECOND"] = 3] = "FIRST_SECOND";
    BET_PAGE[BET_PAGE["FIRST_SECOND_THIRD"] = 4] = "FIRST_SECOND_THIRD";
})(BET_PAGE || (BET_PAGE = {}));
var RANKING;
(function (RANKING) {
    RANKING[RANKING["FIRST"] = 0] = "FIRST";
    RANKING[RANKING["SECOND"] = 1] = "SECOND";
    RANKING[RANKING["THIRD"] = 2] = "THIRD";
    RANKING[RANKING["FOURTH"] = 3] = "FOURTH";
    RANKING[RANKING["FIFTH"] = 4] = "FIFTH";
    RANKING[RANKING["SIXTH"] = 5] = "SIXTH";
    RANKING[RANKING["SEVENTH"] = 6] = "SEVENTH";
    RANKING[RANKING["EIGHTH"] = 7] = "EIGHTH";
    RANKING[RANKING["TOTAL"] = 8] = "TOTAL";
})(RANKING || (RANKING = {}));
var SOUND;
(function (SOUND) {
    SOUND[SOUND["WAIT_FOR_NEXT_GAME"] = 0] = "WAIT_FOR_NEXT_GAME";
    SOUND[SOUND["GAME"] = 1] = "GAME";
    SOUND[SOUND["GAME_END"] = 2] = "GAME_END";
    SOUND[SOUND["GAME_COUNTDOWN"] = 3] = "GAME_COUNTDOWN";
})(SOUND || (SOUND = {}));
var SOUND_EFFECT;
(function (SOUND_EFFECT) {
    SOUND_EFFECT[SOUND_EFFECT["BUTTON_DOWN"] = 0] = "BUTTON_DOWN";
})(SOUND_EFFECT || (SOUND_EFFECT = {}));
var MISSING_NUMBER_PAGE;
(function (MISSING_NUMBER_PAGE) {
    MISSING_NUMBER_PAGE[MISSING_NUMBER_PAGE["RANKING"] = 0] = "RANKING";
    MISSING_NUMBER_PAGE[MISSING_NUMBER_PAGE["SINGLE_DOUBLE"] = 1] = "SINGLE_DOUBLE";
    MISSING_NUMBER_PAGE[MISSING_NUMBER_PAGE["BIG_SMALL"] = 2] = "BIG_SMALL";
    MISSING_NUMBER_PAGE[MISSING_NUMBER_PAGE["DRAGON_TIGER"] = 3] = "DRAGON_TIGER";
    MISSING_NUMBER_PAGE[MISSING_NUMBER_PAGE["FIRST_SECOND_SUM_SINGLE_DOUBLE"] = 4] = "FIRST_SECOND_SUM_SINGLE_DOUBLE";
    MISSING_NUMBER_PAGE[MISSING_NUMBER_PAGE["FIRST_SECOND_SUM_BIG_SMALL"] = 5] = "FIRST_SECOND_SUM_BIG_SMALL";
})(MISSING_NUMBER_PAGE || (MISSING_NUMBER_PAGE = {}));
var MISSING_NUMBER_SORT_OPTION;
(function (MISSING_NUMBER_SORT_OPTION) {
    MISSING_NUMBER_SORT_OPTION[MISSING_NUMBER_SORT_OPTION["MISSING_LOSS_WIN"] = 0] = "MISSING_LOSS_WIN";
    MISSING_NUMBER_SORT_OPTION[MISSING_NUMBER_SORT_OPTION["TODAY_APPEAR"] = 1] = "TODAY_APPEAR";
    MISSING_NUMBER_SORT_OPTION[MISSING_NUMBER_SORT_OPTION["CURRENT_MISSING"] = 2] = "CURRENT_MISSING";
    MISSING_NUMBER_SORT_OPTION[MISSING_NUMBER_SORT_OPTION["WEEK_MISSING"] = 3] = "WEEK_MISSING";
    MISSING_NUMBER_SORT_OPTION[MISSING_NUMBER_SORT_OPTION["MOUTH_MISSING"] = 4] = "MOUTH_MISSING";
    MISSING_NUMBER_SORT_OPTION[MISSING_NUMBER_SORT_OPTION["HISTORICAL_RECORD_MISSING"] = 5] = "HISTORICAL_RECORD_MISSING";
})(MISSING_NUMBER_SORT_OPTION || (MISSING_NUMBER_SORT_OPTION = {}));
var DAILY_DRAGON_PAGE;
(function (DAILY_DRAGON_PAGE) {
    DAILY_DRAGON_PAGE[DAILY_DRAGON_PAGE["BIG_LONG_DRAGON"] = 0] = "BIG_LONG_DRAGON";
    DAILY_DRAGON_PAGE[DAILY_DRAGON_PAGE["SMALL_LONG_DRAGON"] = 1] = "SMALL_LONG_DRAGON";
    DAILY_DRAGON_PAGE[DAILY_DRAGON_PAGE["SINGLE_LONG_DRAGON"] = 2] = "SINGLE_LONG_DRAGON";
    DAILY_DRAGON_PAGE[DAILY_DRAGON_PAGE["DOUBLE_LONG_DRAGON"] = 3] = "DOUBLE_LONG_DRAGON";
    DAILY_DRAGON_PAGE[DAILY_DRAGON_PAGE["DRAGON_LONG_DRAGON"] = 4] = "DRAGON_LONG_DRAGON";
    DAILY_DRAGON_PAGE[DAILY_DRAGON_PAGE["TIGER_LONG_DRAGON"] = 5] = "TIGER_LONG_DRAGON";
})(DAILY_DRAGON_PAGE || (DAILY_DRAGON_PAGE = {}));
var TEST;
(function (TEST) {
    TEST["MISSING_NUMBER"] = "getMissingNumber";
    TEST["TODAY_NUMBER"] = "getTodayNumber";
    TEST["DAILY_LONG_DRAGON"] = "getDailyLongDragon";
    TEST["TWO_SIDE"] = "getTwoSide";
    TEST["SINGLE_DOUBLE"] = "getSingleDouble";
    TEST["FIRST_SECOND_SUM"] = "getFirstSecondSum";
})(TEST || (TEST = {}));
var FirstSecondBettingBoard = (function (_super) {
    __extends(FirstSecondBettingBoard, _super);
    function FirstSecondBettingBoard(game, group) {
        var _this = _super.call(this, game) || this;
        group.add(_this);
        _this.initialize();
        return _this;
    }
    FirstSecondBettingBoard.prototype.initialize = function () {
        this.game.add.sprite(0, 0, 'Lobby', 'SingleBettingBG_1', this);
        this.game.add.text(15, 17, "冠軍", { font: "bold 32px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.game.add.text(150, 17, "亞軍", { font: "bold 32px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.rateText = this.game.add.text(422, 37, "-", { font: "bold 22px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.rateText.anchor.set(1, 0);
        this.tempBet = 0;
        this.tempBetText = this.game.add.text(260, 37, "", { font: "22px Microsoft JhengHei", fill: "#00fc26" }, this);
        this.confirmBet = 0;
        this.confirmBetText = this.game.add.bitmapText(342, 21, 'BetNumber', "0", 30, this);
        this.confirmBetText.anchor.set(0.5);
        this.horseNumberIcon = new Array(2);
        for (var i = 0; i < 2; i++) {
            this.horseNumberIcon[i] = this.game.add.sprite(90 + 135 * i, 14, 'Lobby', 'BettingNumber_1', this);
        }
        this.horseNumber = new Array(2);
    };
    FirstSecondBettingBoard.prototype.showTempBet = function (bet) {
        this.tempBet += bet;
        this.tempBetText.setText("+" + this.tempBet);
    };
    FirstSecondBettingBoard.prototype.keepTempBet = function (bet) {
        this.tempBet = bet;
        this.tempBetText.setText("+" + this.tempBet);
    };
    FirstSecondBettingBoard.prototype.clearTempBet = function () {
        this.tempBet = 0;
        this.tempBetText.setText("");
    };
    FirstSecondBettingBoard.prototype.updateConfirmBet = function () {
        this.confirmBet += this.tempBet;
        this.confirmBetText.setText(this.confirmBet + "");
    };
    FirstSecondBettingBoard.prototype.clearConfirmBet = function () {
        this.confirmBet = 0;
        this.confirmBetText.setText(this.confirmBet + "");
    };
    FirstSecondBettingBoard.prototype.updateHorseNumber = function (horseNumber) {
        for (var i = 0; i < 2; i++) {
            this.horseNumberIcon[i].loadTexture('Lobby', 'BettingNumber_' + (horseNumber[i] + 1));
            this.horseNumber[i] = horseNumber[i];
        }
    };
    FirstSecondBettingBoard.prototype.updateRate = function (rate) {
        this.rateText.setText(rate + "");
    };
    FirstSecondBettingBoard.prototype.checkHorseNumberIsMatch = function (horseNumber) {
        var isMatch = true;
        for (var i = 0; i < 2; i++) {
            if (this.horseNumber[i] != horseNumber[i]) {
                isMatch = false;
                break;
            }
        }
        return isMatch;
    };
    FirstSecondBettingBoard.prototype.getLastBet = function () {
        return this.tempBet;
    };
    FirstSecondBettingBoard.prototype.getLastHorseNumber = function () {
        return this.horseNumber;
    };
    FirstSecondBettingBoard.prototype.getConfirmBet = function () {
        return this.confirmBet;
    };
    FirstSecondBettingBoard.prototype.getRate = function () {
        return parseFloat(this.rateText.text);
    };
    return FirstSecondBettingBoard;
}(Phaser.Group));
var FirstSecondThirdBettingBoard = (function (_super) {
    __extends(FirstSecondThirdBettingBoard, _super);
    function FirstSecondThirdBettingBoard(game, group) {
        var _this = _super.call(this, game) || this;
        group.add(_this);
        _this.initialize();
        return _this;
    }
    FirstSecondThirdBettingBoard.prototype.initialize = function () {
        this.game.add.sprite(0, 0, 'Lobby', 'SingleBettingBG_2', this);
        this.game.add.text(15, 17, "冠軍", { font: "bold 32px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.game.add.text(150, 17, "亞軍", { font: "bold 32px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.game.add.text(285, 17, "季軍", { font: "bold 32px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.rateText = this.game.add.text(575, 37, "-", { font: "bold 24px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.rateText.anchor.set(1, 0);
        this.tempBet = 0;
        this.tempBetText = this.game.add.text(395, 37, "", { font: "24px Microsoft JhengHei", fill: "#00fc26" }, this);
        this.confirmBet = 0;
        this.confirmBetText = this.game.add.bitmapText(477, 21, 'BetNumber', "0", 30, this);
        this.confirmBetText.anchor.set(0.5);
        this.horseNumberIcon = new Array(3);
        for (var i = 0; i < 3; i++) {
            this.horseNumberIcon[i] = this.game.add.sprite(90 + 135 * i, 14, 'Lobby', 'BettingNumber_1', this);
        }
        this.horseNumber = new Array(3);
    };
    FirstSecondThirdBettingBoard.prototype.showTempBet = function (bet) {
        this.tempBet += bet;
        this.tempBetText.setText("+" + this.tempBet);
    };
    FirstSecondThirdBettingBoard.prototype.keepTempBet = function (bet) {
        this.tempBet = bet;
        this.tempBetText.setText("+" + this.tempBet);
    };
    FirstSecondThirdBettingBoard.prototype.clearTempBet = function () {
        this.tempBet = 0;
        this.tempBetText.setText("");
    };
    FirstSecondThirdBettingBoard.prototype.updateConfirmBet = function () {
        this.confirmBet += this.tempBet;
        this.confirmBetText.setText(this.confirmBet + "");
    };
    FirstSecondThirdBettingBoard.prototype.clearConfirmBet = function () {
        this.confirmBet = 0;
        this.confirmBetText.setText(this.confirmBet + "");
    };
    FirstSecondThirdBettingBoard.prototype.updateHorseNumber = function (horseNumber) {
        for (var i = 0; i < 3; i++) {
            this.horseNumberIcon[i].loadTexture('Lobby', 'BettingNumber_' + (horseNumber[i] + 1));
            this.horseNumber[i] = horseNumber[i];
        }
    };
    FirstSecondThirdBettingBoard.prototype.updateRate = function (rate) {
        this.rateText.setText(rate + "");
    };
    FirstSecondThirdBettingBoard.prototype.checkHorseNumberIsMatch = function (horseNumber) {
        var isMatch = true;
        for (var i = 0; i < 3; i++) {
            if (this.horseNumber[i] != horseNumber[i]) {
                isMatch = false;
                break;
            }
        }
        return isMatch;
    };
    FirstSecondThirdBettingBoard.prototype.getLastBet = function () {
        return this.tempBet;
    };
    FirstSecondThirdBettingBoard.prototype.getLastHorseNumber = function () {
        return this.horseNumber;
    };
    FirstSecondThirdBettingBoard.prototype.getConfirmBet = function () {
        return this.confirmBet;
    };
    FirstSecondThirdBettingBoard.prototype.getRate = function () {
        return parseFloat(this.rateText.text);
    };
    return FirstSecondThirdBettingBoard;
}(Phaser.Group));
var GameEntry = (function () {
    function GameEntry() {
        this.game = new Phaser.Game(1920, 1080, Phaser.AUTO, "", { create: this.create });
    }
    GameEntry.prototype.create = function () {
        this.game.stage.disableVisibilityChange = true;
        if (this.game.device.android) {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        }
        else {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        }
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
        this.game.state.add("Preloader", Preloader);
        this.game.state.start("Preloader");
    };
    return GameEntry;
}());
new GameEntry();
var GameManager = (function (_super) {
    __extends(GameManager, _super);
    function GameManager(game, soundManager) {
        var _this = _super.call(this, game) || this;
        _this.soundManager = soundManager;
        _this.initialize();
        return _this;
    }
    GameManager.prototype.initialize = function () {
        this.endGameSignal = new Phaser.Signal();
        this.isGaming = false;
        this.isReadyToShowResult = false;
        this.startGroup = this.game.add.group(this);
        this.loopGroup = this.game.add.group(this);
        this.loopGroup.position.set(-1920, 0);
        this.endGroup = this.game.add.group(this);
        this.endGroup.position.set(-1880, 0);
        this.horseGroup = this.game.add.group(this);
        this.game.add.image(1920, 118, 'TopTrackLoop', null, this.startGroup).anchor.set(1, 0);
        this.topTrackLoop = this.game.add.tileSprite(1920, 118, 1920, 306, 'TopTrackLoop', null, this.loopGroup);
        this.topTrackLoop.anchor.set(1, 0);
        this.game.add.image(1880, 118, 'TopTrackEnd', null, this.endGroup).anchor.set(1, 0);
        this.game.add.image(1920, 395, 'BottomTrackLoop', null, this.startGroup).anchor.set(1, 0);
        this.game.add.sprite(1630, 403, 'Game', 'StartAndFinishLine_1', this.startGroup);
        this.game.add.sprite(1600, 403, 'Game', 'StartAndFinishLine_2', this.startGroup);
        this.bottomTrackLoop = this.game.add.tileSprite(1920, 395, 1920, 574, 'BottomTrackLoop', null, this.loopGroup);
        this.bottomTrackLoop.anchor.set(1, 0);
        this.game.add.image(1880, 395, 'BottomTrackEnd', null, this.endGroup).anchor.set(1, 0);
        this.game.add.sprite(1370, 550, 'Game', 'BeforeFinishLine_300M', this.endGroup);
        this.game.add.sprite(1320, 403, 'Game', 'BeforeFinishLine', this.endGroup);
        this.game.add.sprite(955, 550, 'Game', 'BeforeFinishLine_200M', this.endGroup);
        this.game.add.sprite(905, 403, 'Game', 'BeforeFinishLine', this.endGroup);
        this.game.add.sprite(550, 550, 'Game', 'BeforeFinishLine_100M', this.endGroup);
        this.game.add.sprite(500, 403, 'Game', 'BeforeFinishLine', this.endGroup);
        this.game.add.sprite(70, 403, 'Game', 'StartAndFinishLine_1', this.endGroup);
        this.game.add.sprite(40, 403, 'Game', 'StartAndFinishLine_2', this.endGroup);
        this.horse = new Array(8);
        this.horsePath = new Array(8);
        for (var i = 0; i < 8; i++) {
            this.horse[i] = this.game.add.sprite(0, 0, 'Horse_' + (i + 1), null, this.horseGroup);
            this.horse[i].anchor.set(1, 1);
            this.horse[i].animations.add('Run', [0, 1, 2, 3, 4, 5, 6, 7], 20, true);
        }
        this.horseFrame = this.game.add.sprite(1750, 200, 'Game', 'HorseFrame', this);
        this.horseReset();
        this.game.add.image(0, 0, 'GameTopFrame', null, this);
        this.game.add.image(0, 953, 'GameBottomFrame', null, this);
        this.nextGameCountdownBG = this.game.add.sprite(960, 70, 'Game', 'CountdownBG', this);
        this.nextGameCountdownBG.anchor.set(0.5);
        this.nextGameCountdownText = this.game.add.bitmapText(960, 65, 'CountdownNumber', "", 40, this);
        this.nextGameCountdownText.anchor.set(0.5);
        this.rankingDisplay = new Array(8);
        for (var i = 0; i < 8; i++) {
            this.rankingDisplay[i] = this.game.add.sprite(585 + i * 100, 23, 'Lobby', 'HistoricalNumber_' + (i + 1), this);
            this.rankingDisplay[i].visible = false;
        }
        this.racingCountdownSprite = this.game.add.sprite(960, 540, null, null, this);
        this.racingCountdownSprite.anchor.set(0.5);
        this.game.add.text(53, 990, "最低注-最高注", { font: "bold 26px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.lowestAndHighestBetText = this.game.add.bitmapText(140, 1046, 'BetNumber', "", 34, this);
        this.lowestAndHighestBetText.anchor.set(0.5);
        this.game.add.text(317, 990, "本場押注金額", { font: "bold 26px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.totalBetText = this.game.add.bitmapText(397, 1045, 'BetNumber', "", 34, this);
        this.totalBetText.anchor.set(0.5);
        this.game.add.text(605, 990, "您的押注", { font: "bold 26px Microsoft JhengHei", fill: "#ffffff" }, this);
    };
    GameManager.prototype.updateTotalBet = function (totalBet) {
        this.totalBetText.setText(totalBet + "");
    };
    GameManager.prototype.updateLowestAndHighestBet = function (lowestBet, highest) {
        this.lowestAndHighestBetText.setText(lowestBet + "-" + highest);
    };
    GameManager.prototype.startNextGameCountdown = function (countdownTime) {
        var _this = this;
        if (this.isGaming) {
            return;
        }
        this.game.time.events.remove(this.nextGameCountdownTimerEvent);
        for (var i = 0; i < 8; i++) {
            this.rankingDisplay[i].visible = false;
        }
        if (countdownTime < 4) {
            this.nextGameCountdownBG.visible = false;
            this.nextGameCountdownText.visible = false;
            this.racingPrepare(countdownTime);
        }
        else {
            this.nextGameCountdownBG.visible = true;
            this.nextGameCountdownText.visible = true;
            this.nextGameCountdownText.setText(countdownTime + "");
            this.nextGameCountdownTimerEvent = this.game.time.events.loop(1000, function () {
                countdownTime--;
                _this.nextGameCountdownText.setText(countdownTime + "");
                if (countdownTime < 4) {
                    _this.game.time.events.remove(_this.nextGameCountdownTimerEvent);
                    _this.nextGameCountdownBG.visible = false;
                    _this.nextGameCountdownText.visible = false;
                    _this.racingPrepare(countdownTime);
                }
            }, this);
        }
    };
    GameManager.prototype.racingPrepare = function (countdownTime) {
        this.isGaming = true;
        for (var i = 0; i < 8; i++) {
            this.rankingDisplay[i].position.set(585 + i * 100, 23);
            this.rankingDisplay[i].visible = true;
        }
        this.racingCountdown(countdownTime);
    };
    GameManager.prototype.racingCountdown = function (countdownTime) {
        var _this = this;
        this.racingCountdownSprite.loadTexture('Game', countdownTime > 0 ? 'RacingCountdown_' + countdownTime : "RacingCountdown_Flag");
        this.racingCountdownSprite.visible = true;
        this.racingCountdownSprite.alpha = 1;
        this.racingCountdownSprite.scale.set(1);
        this.game.add.tween(this.racingCountdownSprite).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 0, countdownTime);
        this.game.add.tween(this.racingCountdownSprite.scale).to({ x: 4, y: 4 }, 1000, Phaser.Easing.Linear.None, true, 0, countdownTime);
        this.soundManager.playSound(SOUND.GAME_COUNTDOWN);
        var countdownTimerEvent = this.game.time.events.loop(1000, function () {
            if (countdownTime == 0) {
                _this.soundManager.stopSound(SOUND.GAME_COUNTDOWN);
                _this.game.time.events.remove(countdownTimerEvent);
                _this.racingStart();
                _this.racingCountdownSprite.visible = false;
                _this.game.tweens.removeAll();
                return;
            }
            countdownTime--;
            _this.racingCountdownSprite.loadTexture('Game', countdownTime > 0 ? 'RacingCountdown_' + countdownTime : "RacingCountdown_Flag");
        }, this);
    };
    GameManager.prototype.racingStart = function () {
        var _this = this;
        this.soundManager.playSound(SOUND.GAME);
        for (var i = 0; i < 8; i++) {
            this.horse[i].play('Run');
        }
        this.horseRandomPath();
        this.showRankingTimerEvent = this.game.time.events.loop(1000, this.updateInstantRanking, this);
        var racingStartTimerEvent = this.game.time.events.loop(20, function () {
            if (_this.loopGroup.position.x < 0) {
                if (_this.horseFrame.position.x < 2000) {
                    _this.horseFrame.position.x += 20;
                }
                _this.startGroup.position.x += 20;
                _this.loopGroup.position.x += 20;
                _this.updateHorsePosition();
            }
            else {
                _this.game.time.events.remove(racingStartTimerEvent);
                _this.loopGroup.position.set(0);
                _this.horseRandomPath();
                _this.racingLoop();
            }
        }, this);
    };
    GameManager.prototype.racingLoop = function () {
        var _this = this;
        this.isReadyToShowResult = true;
        this.horseRandomPathTimerEvent = this.game.time.events.loop(2000, this.horseRandomPath, this);
        this.racingLoopTimerEvent = this.game.time.events.loop(20, function () {
            _this.topTrackLoop.tilePosition.x += 20;
            _this.bottomTrackLoop.tilePosition.x += 20;
            _this.updateHorsePosition();
        }, this);
    };
    GameManager.prototype.racingStop = function (result) {
        var _this = this;
        if (!this.isReadyToShowResult) {
            return;
        }
        this.isReadyToShowResult = false;
        this.game.time.events.remove(this.racingLoopTimerEvent);
        this.game.time.events.remove(this.horseRandomPathTimerEvent);
        this.horsePathDesignated(result);
        var racingStopTimerEvent = this.game.time.events.loop(20, function () {
            _this.topTrackLoop.tilePosition.x += 20;
            _this.bottomTrackLoop.tilePosition.x += 20;
            _this.updateHorsePosition();
            if (_this.horseInterpolation == 1) {
                _this.game.time.events.remove(racingStopTimerEvent);
                _this.connectToEndTrack();
            }
        }, this);
    };
    GameManager.prototype.connectToEndTrack = function () {
        var _this = this;
        var remainingTile = 1920 - this.topTrackLoop.tilePosition.x;
        var connectTimerEvent = this.game.time.events.loop(20, function () {
            _this.topTrackLoop.tilePosition.x += 20;
            _this.bottomTrackLoop.tilePosition.x += 20;
            remainingTile -= 20;
            if (remainingTile <= 0) {
                _this.game.time.events.remove(connectTimerEvent);
                _this.topTrackLoop.tilePosition.set(0, 0);
                _this.bottomTrackLoop.tilePosition.set(0, 0);
                _this.racingEnd();
            }
        }, this);
    };
    GameManager.prototype.racingEnd = function () {
        var _this = this;
        var racingEndTimerEvent = this.game.time.events.loop(20, function () {
            if (_this.endGroup.position.x < 0) {
                _this.loopGroup.position.x += 20;
                _this.endGroup.position.x += 20;
            }
            else {
                _this.game.time.events.remove(racingEndTimerEvent);
                _this.endGroup.position.set(0);
                _this.horseToFinishLine();
            }
        }, this);
    };
    GameManager.prototype.horseToFinishLine = function () {
        var _this = this;
        var horseToFinishLineTimerEvent = this.game.time.events.loop(20, function () {
            if (_this.horseGroup.position.x > -2000) {
                _this.horseGroup.position.x -= 20;
            }
            else {
                _this.soundManager.stopSound(SOUND.GAME);
                _this.game.time.events.remove(horseToFinishLineTimerEvent);
                _this.horseGroup.position.set(-2000, 0);
                _this.game.time.events.remove(_this.showRankingTimerEvent);
                _this.horseReset();
                _this.startGroup.position.set(0);
                _this.loopGroup.position.set(-1920, 0);
                _this.endGroup.position.set(-1880, 0);
                _this.endGameSignal.dispatch();
                _this.isGaming = false;
            }
        }, this);
    };
    GameManager.prototype.horseReset = function () {
        this.horseGroup.position.set(0);
        this.horseFrame.position.set(1750, 200);
        for (var i = 0; i < 8; i++) {
            this.horse[i].position.set(2000, 490 + i * 70);
            this.horse[i].animations.stop("Run", true);
        }
    };
    GameManager.prototype.horseRandomPath = function () {
        for (var i = 0; i < 8; i++) {
            this.horsePath[i] = new Array(2);
            this.horsePath[i][0] = Math.floor(this.horse[i].position.x);
            this.horsePath[i][1] = Math.floor(Math.random() * 1500) + 400;
        }
        this.horseInterpolation = 0;
    };
    GameManager.prototype.horsePathDesignated = function (result) {
        var rankingPath = [500, 650, 800, 950, 1100, 1250, 1400, 1550];
        for (var i = 0; i < 8; i++) {
            var horseNumber = result[i] - 1;
            this.horsePath[horseNumber] = new Array(2);
            this.horsePath[horseNumber][0] = Math.floor(this.horse[horseNumber].position.x);
            this.horsePath[horseNumber][1] = rankingPath[i];
        }
        this.horseInterpolation = 0;
    };
    GameManager.prototype.updateHorsePosition = function () {
        this.horseInterpolation = this.horseInterpolation > 1 ? 1 : this.horseInterpolation + 0.01;
        for (var i = 0; i < 8; i++) {
            this.horse[i].position.set(Phaser.Math.linearInterpolation(this.horsePath[i], this.horseInterpolation), this.horse[i].position.y);
        }
    };
    GameManager.prototype.updateInstantRanking = function () {
        var horseInfo = new Array(8);
        for (var i = 0; i < 8; i++) {
            horseInfo[i] = {
                Numbering: i,
                PosX: this.horse[i].position.x
            };
        }
        horseInfo = horseInfo.sort(function (a, b) {
            return a.PosX > b.PosX ? 1 : -1;
        });
        for (var i = 0; i < 8; i++) {
            this.rankingDisplay[horseInfo[i].Numbering].position.set(585 + i * 100, 23);
        }
    };
    return GameManager;
}(Phaser.Group));
var LobbyManager = (function (_super) {
    __extends(LobbyManager, _super);
    function LobbyManager(game, soundManager) {
        var _this = _super.call(this, game) || this;
        _this.soundManager = soundManager;
        _this.initialize();
        return _this;
    }
    LobbyManager.prototype.initialize = function () {
        var _this = this;
        this.betSignal = new Phaser.Signal();
        this.clearUserTotalBetSignal = new Phaser.Signal();
        this.gameBetHandling = false;
        this.game.add.image(0, 0, 'MainBG', null, this);
        this.game.add.image(0, 0, 'LobbyTopFrame', null, this);
        this.game.add.image(0, 948, 'LobbyBottomFrame', null, this);
        this.game.add.image(0, 140, 'LobbyButtonBG', null, this);
        var rankingGroup = this.game.add.group(this);
        var bigSmallGroup = this.game.add.group(this);
        bigSmallGroup.visible = false;
        var firstSecondSumGroup = this.game.add.group(this);
        firstSecondSumGroup.visible = false;
        var firstSecondGroup = this.game.add.group(this);
        firstSecondGroup.visible = false;
        var firstSecondThirdGroup = this.game.add.group(this);
        firstSecondThirdGroup.visible = false;
        var selectRankingPage = new SpriteButton(this.game, 20, 150, 'Button', 'LobbyButton_Up_1', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_1',
            OutFrame: 'LobbyButton_Up_1',
            UpFrame: 'LobbyButton_Up_1',
            DownFrame: 'LobbyButton_Down_1'
        });
        this.add(selectRankingPage);
        var selectBigSmall = new SpriteButton(this.game, 401, 150, 'Button', 'LobbyButton_Up_2', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_2',
            OutFrame: 'LobbyButton_Up_2',
            UpFrame: 'LobbyButton_Up_2',
            DownFrame: 'LobbyButton_Down_2'
        });
        this.add(selectBigSmall);
        var selectFirstSecondSum = new SpriteButton(this.game, 782, 150, 'Button', 'LobbyButton_Up_3', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_3',
            OutFrame: 'LobbyButton_Up_3',
            UpFrame: 'LobbyButton_Up_3',
            DownFrame: 'LobbyButton_Down_3'
        });
        this.add(selectFirstSecondSum);
        var selectFirstSecond = new SpriteButton(this.game, 1163, 150, 'Button', 'LobbyButton_Up_4', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_4',
            OutFrame: 'LobbyButton_Up_4',
            UpFrame: 'LobbyButton_Up_4',
            DownFrame: 'LobbyButton_Down_4'
        });
        this.add(selectFirstSecond);
        var selectFirstSecondThird = new SpriteButton(this.game, 1544, 150, 'Button', 'LobbyButton_Up_5', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_5',
            OutFrame: 'LobbyButton_Up_5',
            UpFrame: 'LobbyButton_Up_5',
            DownFrame: 'LobbyButton_Down_5'
        });
        this.add(selectFirstSecondThird);
        var rankingTitleBG = new Array(8);
        var rankingTitle = new Array(8);
        var rankingName = ["冠軍", "亞軍", "季軍", "第四名", "第五名", "第六名", "第七名", "第八名"];
        var rankingSpriteButton = new Array(8);
        this.rankingRateText = new Array(8);
        this.rankingTempBet = new Array(8);
        this.rankingTempBetText = new Array(8);
        this.rankingConfirmBet = new Array(8);
        this.rankingConfirmBetText = new Array(8);
        this.rankingLastBet = new Array(8);
        var _loop_4 = function (rank) {
            rankingTitleBG[rank] = this_4.game.add.sprite(20, 245 + 88 * rank, 'Lobby', 'Ranking_' + (rank + 1), rankingGroup);
            rankingTitle[rank] = this_4.game.add.text(165, 285 + 88 * rank, rankingName[rank], { font: "bold 40px Microsoft JhengHei", fill: "#ffffff" }, rankingGroup);
            rankingTitle[rank].anchor.set(0.5);
            rankingSpriteButton[rank] = new Array(8);
            this_4.rankingRateText[rank] = new Array(8);
            this_4.rankingTempBet[rank] = new Array(8);
            this_4.rankingTempBetText[rank] = new Array(8);
            this_4.rankingConfirmBet[rank] = new Array(8);
            this_4.rankingConfirmBetText[rank] = new Array(8);
            this_4.rankingLastBet[rank] = new Array(8);
            var _loop_10 = function (horseNumber) {
                rankingSpriteButton[rank][horseNumber] = new SpriteButton(this_4.game, 290 + 202 * horseNumber, 246 + 88 * rank, 'Button', 'BetAndOdds_Up_' + (horseNumber + 1), {
                    Key: 'Button',
                    OverFrame: 'BetAndOdds_Up_' + (horseNumber + 1),
                    OutFrame: 'BetAndOdds_Up_' + (horseNumber + 1),
                    UpFrame: 'BetAndOdds_Up_' + (horseNumber + 1),
                    DownFrame: 'BetAndOdds_Down_' + (horseNumber + 1)
                });
                rankingSpriteButton[rank][horseNumber].OnDown.add(function () {
                    _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this_4);
                rankingSpriteButton[rank][horseNumber].OnUp.add(function () {
                    _this.startRankingBet(rank, horseNumber);
                }, this_4);
                rankingGroup.add(rankingSpriteButton[rank][horseNumber]);
                this_4.rankingRateText[rank][horseNumber] = this_4.game.add.text(472 + 202 * horseNumber, 288 + 88 * rank, "-", { font: "24px Microsoft JhengHei", fill: "#ffffff" }, rankingGroup);
                this_4.rankingRateText[rank][horseNumber].anchor.set(1, 0);
                this_4.rankingTempBet[rank][horseNumber] = 0;
                this_4.rankingTempBetText[rank][horseNumber] = this_4.game.add.text(347 + 202 * horseNumber, 285 + 88 * rank, "", { font: "24px Microsoft JhengHei", fill: "#00fc26" }, rankingGroup);
                this_4.rankingConfirmBet[rank][horseNumber] = 0;
                this_4.rankingConfirmBetText[rank][horseNumber] = this_4.game.add.bitmapText(352 + 202 * horseNumber, 253 + 88 * rank, 'BetNumber', "0", 30, rankingGroup);
                this_4.rankingLastBet[rank][horseNumber] = 0;
            };
            for (var horseNumber = 0; horseNumber < 8; horseNumber++) {
                _loop_10(horseNumber);
            }
        };
        var this_4 = this;
        for (var rank = 0; rank < 8; rank++) {
            _loop_4(rank);
        }
        var bigSmallTitleBG = new Array(8);
        var bigSmallTitle = new Array(8);
        var bigSmallSpriteButton = new Array(8);
        var bigSmallFrameName = ["SizeBig", "SizeSmall", "Single", "Double", "Dragon", "Tiger"];
        this.bigSmallRateText = new Array(8);
        this.bigSmallTempBet = new Array(8);
        this.bigSmallTempBetText = new Array(8);
        this.bigSmallConfirmBet = new Array(8);
        this.bigSmallConfirmBetText = new Array(8);
        this.bigSmallLastBet = new Array(8);
        var _loop_5 = function (i) {
            bigSmallTitleBG[i] = this_5.game.add.sprite(20, 245 + 88 * i, 'Lobby', 'Ranking_' + (i + 1), bigSmallGroup);
            bigSmallTitle[i] = this_5.game.add.text(165, 285 + 88 * i, rankingName[i], { font: "bold 40px Microsoft JhengHei", fill: "#ffffff" }, bigSmallGroup);
            bigSmallTitle[i].anchor.set(0.5);
            bigSmallSpriteButton[i] = new Array(6);
            this_5.bigSmallRateText[i] = new Array(6);
            this_5.bigSmallTempBet[i] = new Array(6);
            this_5.bigSmallTempBetText[i] = new Array(6);
            this_5.bigSmallConfirmBet[i] = new Array(6);
            this_5.bigSmallConfirmBetText[i] = new Array(6);
            this_5.bigSmallLastBet[i] = new Array(6);
            var _loop_11 = function (j) {
                if (i > 3 && j > 3) {
                    return "break";
                }
                bigSmallSpriteButton[i][j] = new SpriteButton(this_5.game, 290 + 273 * j, 246 + 88 * i, 'Button', bigSmallFrameName[j]);
                bigSmallSpriteButton[i][j].OnDown.add(function () {
                    _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this_5);
                bigSmallSpriteButton[i][j].OnUp.add(function () {
                    _this.startBigSmallBet(i, j);
                }, this_5);
                bigSmallGroup.add(bigSmallSpriteButton[i][j]);
                this_5.bigSmallRateText[i][j] = this_5.game.add.text(521 + 273 * j, 288 + 88 * i, "-", { font: "24px Microsoft JhengHei", fill: "#ffffff" }, bigSmallGroup);
                this_5.bigSmallRateText[i][j].anchor.set(1, 0);
                this_5.bigSmallTempBet[i][j] = 0;
                this_5.bigSmallTempBetText[i][j] = this_5.game.add.text(358 + 273 * j, 285 + 88 * i, "", { font: "24px Microsoft JhengHei", fill: "#00fc26" }, bigSmallGroup);
                this_5.bigSmallConfirmBet[i][j] = 0;
                this_5.bigSmallConfirmBetText[i][j] = this_5.game.add.bitmapText(360 + 273 * j, 254 + 88 * i, 'BetNumber', "0", 30, bigSmallGroup);
                this_5.bigSmallLastBet[i][j] = 0;
            };
            for (var j = 0; j < 6; j++) {
                var state_1 = _loop_11(j);
                if (state_1 === "break")
                    break;
            }
        };
        var this_5 = this;
        for (var i = 0; i < 8; i++) {
            _loop_5(i);
        }
        var firstSecondSumSpriteButton = new Array(17);
        var firstSecondSumFrameName = ["3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "SizeBig", "SizeSmall", "Single", "Double"];
        var spriteButtonPosX = [20, 320, 620, 920, 1220, 1520];
        var spriteButtonPosY = [246, 334, 422, 510];
        var payTextPosX = [285, 585, 885, 1185, 1485, 1785];
        var payTextPosY = [288, 376, 464, 552];
        var tempBetTextPosX = [82, 382, 682, 982, 1282, 1582];
        var tempBetTextPosY = [285, 373, 461, 549];
        var confirmBetTextPosX = [84, 384, 684, 984, 1284, 1584];
        var confirmBetTextPosY = [254, 342, 430, 518];
        var rowIndex = 0;
        this.firstSecondSumRateText = new Array(17);
        this.firstSecondSumTempBet = new Array(17);
        this.firstSecondSumTempBetText = new Array(17);
        this.firstSecondSumConfirmBet = new Array(17);
        this.firstSecondSumConfirmBetText = new Array(17);
        this.firstSecondSumLastBet = new Array(17);
        var _loop_6 = function (i) {
            firstSecondSumSpriteButton[i] = new SpriteButton(this_6.game, spriteButtonPosX[i % 6], spriteButtonPosY[rowIndex], 'Button', "Sum_" + firstSecondSumFrameName[i]);
            firstSecondSumSpriteButton[i].OnDown.add(function () {
                _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this_6);
            firstSecondSumSpriteButton[i].OnUp.add(function () {
                _this.startFirstSecondSumBet(i);
            }, this_6);
            firstSecondSumGroup.add(firstSecondSumSpriteButton[i]);
            this_6.firstSecondSumRateText[i] = this_6.game.add.text(payTextPosX[i % 6], payTextPosY[rowIndex], "-", { font: "24px Microsoft JhengHei", fill: "#ffffff" }, firstSecondSumGroup);
            this_6.firstSecondSumRateText[i].anchor.set(1, 0);
            this_6.firstSecondSumTempBet[i] = 0;
            this_6.firstSecondSumTempBetText[i] = this_6.game.add.text(tempBetTextPosX[i % 6], tempBetTextPosY[rowIndex], "", { font: "24px Microsoft JhengHei", fill: "#00fc26" }, firstSecondSumGroup);
            this_6.firstSecondSumConfirmBet[i] = 0;
            this_6.firstSecondSumConfirmBetText[i] = this_6.game.add.bitmapText(confirmBetTextPosX[i % 6], confirmBetTextPosY[rowIndex], 'BetNumber', "0", 30, firstSecondSumGroup);
            this_6.firstSecondSumLastBet[i] = 0;
            if (i % 6 == 5) {
                rowIndex++;
            }
        };
        var this_6 = this;
        for (var i = 0; i < 17; i++) {
            _loop_6(i);
        }
        var firstSecondTitleBG = new Array(2);
        var firstSecondTitle = new Array(2);
        this.game.add.image(20, 425, 'BettingBG_1', null, firstSecondGroup);
        this.firstSecondSpriteButton = new Array(2);
        this.firstSecondCurrentHorseNumber = new Array(2);
        this.firstSecondNewBettingBoardIndex = 0;
        this.firstSecondCurrentBettingBoardIndex = 0;
        this.firstSecondLastBettingBoardIndex = 0;
        this.firstSecondLastBet = 0;
        this.firstSecondLockBet = false;
        this.firstSecondBettingBoard = new Array(56);
        this.firstSecondScrollGroup = this.game.add.group(firstSecondGroup);
        var _loop_7 = function (i) {
            firstSecondTitleBG[i] = this_7.game.add.sprite(20, 245 + 88 * i, 'Lobby', 'Ranking_' + (i + 1), firstSecondGroup);
            firstSecondTitle[i] = this_7.game.add.text(165, 285 + 88 * i, rankingName[i], { font: "bold 40px Microsoft JhengHei", fill: "#ffffff" }, firstSecondGroup);
            firstSecondTitle[i].anchor.set(0.5);
            this_7.firstSecondSpriteButton[i] = new Array(8);
            var _loop_12 = function (j) {
                this_7.firstSecondSpriteButton[i][j] = new SpriteButton(this_7.game, 290 + 202 * j, 246 + 88 * i, 'Button', 'BetAndOdds_Up_' + (j + 1), {
                    Key: 'Button',
                    OverFrame: 'BetAndOdds_Up_' + (j + 1),
                    OutFrame: 'BetAndOdds_Up_' + (j + 1),
                    UpFrame: 'BetAndOdds_Up_' + (j + 1),
                    DownFrame: 'BetAndOdds_Down_' + (j + 1)
                });
                this_7.firstSecondSpriteButton[i][j].OnDown.add(function () {
                    _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this_7);
                this_7.firstSecondSpriteButton[i][j].OnUp.add(function () {
                    _this.changeFirstSecondSelected(i, j);
                }, this_7);
                firstSecondGroup.add(this_7.firstSecondSpriteButton[i][j]);
            };
            for (var j = 0; j < 8; j++) {
                _loop_12(j);
            }
        };
        var this_7 = this;
        for (var i = 0; i < 2; i++) {
            _loop_7(i);
        }
        var firstSecondMask = this.game.add.graphics(20, 440, firstSecondGroup);
        firstSecondMask.beginFill(0xffffff, 0);
        firstSecondMask.drawRect(0, 0, 1883, 442);
        firstSecondMask.endFill();
        firstSecondMask.inputEnabled = true;
        firstSecondMask.events.onInputDown.add(this.startFirstSecondScroll, this);
        firstSecondMask.events.onInputUp.add(this.stopFirstSecondScroll, this);
        firstSecondMask.events.onInputOut.add(this.stopFirstSecondScroll, this);
        this.firstSecondScrollGroup.mask = firstSecondMask;
        var firstSecondThirdTitleBG = new Array(3);
        var firstSecondThirdTitle = new Array(3);
        this.game.add.image(20, 510, 'BettingBG_2', null, firstSecondThirdGroup);
        this.firstSecondThirdSpriteButton = new Array(3);
        this.firstSecondThirdCurrentHorseNumber = new Array(3);
        this.firstSecondThirdNewBettingBoardIndex = 0;
        this.firstSecondThirdCurrentBettingBoardIndex = 0;
        this.firstSecondThirdLastBettingBoardIndex = 0;
        this.firstSecondThirdLastBet = 0;
        this.firstSecondThirdLockBet = false;
        this.firstSecondThirdBettingBoard = new Array(336);
        this.firstSecondThirdScrollGroup = this.game.add.group(firstSecondThirdGroup);
        var _loop_8 = function (i) {
            firstSecondThirdTitleBG[i] = this_8.game.add.sprite(20, 245 + 88 * i, 'Lobby', 'Ranking_' + (i + 1), firstSecondThirdGroup);
            firstSecondThirdTitle[i] = this_8.game.add.text(165, 285 + 88 * i, rankingName[i], { font: "bold 40px Microsoft JhengHei", fill: "#ffffff" }, firstSecondThirdGroup);
            firstSecondThirdTitle[i].anchor.set(0.5);
            this_8.firstSecondThirdSpriteButton[i] = new Array(8);
            var _loop_13 = function (j) {
                this_8.firstSecondThirdSpriteButton[i][j] = new SpriteButton(this_8.game, 290 + 202 * j, 246 + 88 * i, 'Button', 'BetAndOdds_Up_' + (j + 1), {
                    Key: 'Button',
                    OverFrame: 'BetAndOdds_Up_' + (j + 1),
                    OutFrame: 'BetAndOdds_Up_' + (j + 1),
                    UpFrame: 'BetAndOdds_Up_' + (j + 1),
                    DownFrame: 'BetAndOdds_Down_' + (j + 1)
                });
                this_8.firstSecondThirdSpriteButton[i][j].OnDown.add(function () {
                    _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this_8);
                this_8.firstSecondThirdSpriteButton[i][j].OnUp.add(function () {
                    _this.changeFirstSecondThirdSelected(i, j);
                }, this_8);
                firstSecondThirdGroup.add(this_8.firstSecondThirdSpriteButton[i][j]);
            };
            for (var j = 0; j < 8; j++) {
                _loop_13(j);
            }
        };
        var this_8 = this;
        for (var i = 0; i < 3; i++) {
            _loop_8(i);
        }
        var firstSecondThirdMask = this.game.add.graphics(20, 525, firstSecondThirdGroup);
        firstSecondThirdMask.beginFill(0xffffff, 0);
        firstSecondThirdMask.drawRect(0, 0, 1883, 349);
        firstSecondThirdMask.endFill();
        firstSecondThirdMask.inputEnabled = true;
        firstSecondThirdMask.events.onInputDown.add(this.startFirstSecondThirdScroll, this);
        firstSecondThirdMask.events.onInputUp.add(this.stopFirstSecondThirdScroll, this);
        firstSecondThirdMask.events.onInputOut.add(this.stopFirstSecondThirdScroll, this);
        this.firstSecondThirdScrollGroup.mask = firstSecondThirdMask;
        this.game.add.text(630, 63, "本場總押注", { font: "bold 36px Microsoft JhengHei", fill: "#000f54" }, this);
        this.totalBetText = this.game.add.bitmapText(1023, 88, 'BetNumber', "", 46, this);
        this.totalBetText.anchor.set(0.5);
        this.game.add.text(95, 990, "您的押注", { font: "bold 26px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.nextGameCountdownText = this.game.add.bitmapText(1316, 75, 'CountdownNumber', "", 30, this);
        this.nextGameCountdownText.anchor.set(0.5);
        this.chipSpriteButton = new Array(6);
        this.chipSize = [10, 50, 100, 500, 1000, 5000];
        var _loop_9 = function (i) {
            this_9.chipSpriteButton[i] = new SpriteButton(this_9.game, 285 + 138.5 * i, 950, 'Button', 'Chip_' + this_9.chipSize[i]);
            this_9.chipSpriteButton[i].OnDown.add(function () {
                _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this_9);
            this_9.chipSpriteButton[i].OnUp.add(function () {
                _this.changeChipSeleted(i);
            }, this_9);
            this_9.add(this_9.chipSpriteButton[i]);
        };
        var this_9 = this;
        for (var i = 0; i < 6; i++) {
            _loop_9(i);
        }
        this.confirmBet = new SpriteButton(this.game, 1135, 975, 'Button', 'ConfirmBet');
        this.add(this.confirmBet);
        this.keepBet = new SpriteButton(this.game, 1330, 975, 'Button', 'KeepBet');
        this.add(this.keepBet);
        this.resetBet = new SpriteButton(this.game, 1525, 975, 'Button', 'ResetBet');
        this.add(this.resetBet);
        this.currentSelectedPage = selectRankingPage;
        this.currentSelectedPage.SelectButton();
        this.currentShowGroup = rankingGroup;
        this.currentChip = 0;
        for (var i = 0; i < 6; i++) {
            if (i == this.currentChip) {
                this.chipSpriteButton[i].SelectButton();
            }
            else {
                this.chipSpriteButton[i].UnselectedButton();
            }
            this.chipSpriteButton[i].isSingleSeleted = true;
        }
        this.confirmBet.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.confirmBet.OnUp.add(this.confirmRankingBet, this);
        this.keepBet.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.keepBet.OnUp.add(this.keepRankingBet, this);
        this.resetBet.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.resetBet.OnUp.add(this.resetRankingBet, this);
        selectRankingPage.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectRankingPage.OnUp.add(function () {
            _this.changeBetPage(selectRankingPage, rankingGroup, BET_PAGE.RANKING);
        }, this);
        selectBigSmall.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectBigSmall.OnUp.add(function () {
            _this.changeBetPage(selectBigSmall, bigSmallGroup, BET_PAGE.BIG_SMALL_SINGLE_DOUBLE);
        }, this);
        selectFirstSecondSum.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectFirstSecondSum.OnUp.add(function () {
            _this.changeBetPage(selectFirstSecondSum, firstSecondSumGroup, BET_PAGE.FIRST_SECOND_SUM);
        }, this);
        selectFirstSecond.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectFirstSecond.OnUp.add(function () {
            _this.changeBetPage(selectFirstSecond, firstSecondGroup, BET_PAGE.FIRST_SECOND);
        }, this);
        selectFirstSecondThird.OnDown.add(function () {
            _this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectFirstSecondThird.OnUp.add(function () {
            _this.changeBetPage(selectFirstSecondThird, firstSecondThirdGroup, BET_PAGE.FIRST_SECOND_THIRD);
        }, this);
    };
    LobbyManager.prototype.startNextGameCountdown = function (countdownTime) {
        var _this = this;
        this.game.time.events.remove(this.nextGameCountdownTimerEvent);
        this.nextGameCountdownText.setText(countdownTime + "");
        if (countdownTime > 0) {
            this.nextGameCountdownTimerEvent = this.game.time.events.loop(1000, function () {
                countdownTime--;
                _this.nextGameCountdownText.setText(countdownTime + "");
                if (countdownTime == 0) {
                    _this.game.time.events.remove(_this.nextGameCountdownTimerEvent);
                    _this.clearAllPageBet();
                }
            }, this);
        }
    };
    LobbyManager.prototype.updateTotalBet = function (totalBet) {
        this.totalBetText.setText(totalBet + "");
    };
    LobbyManager.prototype.updateRankingRate = function (rate) {
        this.rankingRate = rate;
        for (var rank = 0; rank < 8; rank++) {
            for (var horseNumber = 0; horseNumber < 8; horseNumber++) {
                this.rankingRateText[rank][horseNumber].setText(rate[rank][horseNumber] + "");
            }
        }
    };
    LobbyManager.prototype.startRankingBet = function (rank, horseNumber) {
        if (this.gameBetHandling) {
            return;
        }
        this.rankingTempBet[rank][horseNumber] += this.chipSize[this.currentChip];
        this.rankingTempBetText[rank][horseNumber].setText("+" + this.rankingTempBet[rank][horseNumber]);
    };
    LobbyManager.prototype.confirmRankingBet = function () {
        var userBet = 0;
        var betArray = new Array();
        for (var rank = 0; rank < 8; rank++) {
            for (var horseNumber = 0; horseNumber < 8; horseNumber++) {
                if (this.rankingTempBet[rank][horseNumber] > 0) {
                    userBet += this.rankingTempBet[rank][horseNumber];
                    betArray.push({
                        str_type: (rank + 1) + "_" + (horseNumber + 1),
                        num_val: this.rankingTempBet[rank][horseNumber],
                        num_rate: this.rankingRate[rank][horseNumber]
                    });
                }
            }
        }
        this.betSignal.dispatch(BET_PAGE.RANKING, userBet, betArray);
    };
    LobbyManager.prototype.rankingBetResult = function (betStatus) {
        if (betStatus) {
            for (var rank = 0; rank < RANKING.TOTAL; rank++) {
                for (var horseNumber = 0; horseNumber < 8; horseNumber++) {
                    this.rankingLastBet[rank][horseNumber] = this.rankingTempBet[rank][horseNumber];
                    this.rankingConfirmBet[rank][horseNumber] += this.rankingTempBet[rank][horseNumber];
                    this.rankingConfirmBetText[rank][horseNumber].setText(this.rankingConfirmBet[rank][horseNumber] + "");
                }
            }
        }
        this.resetRankingBet();
    };
    LobbyManager.prototype.keepRankingBet = function () {
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                this.rankingTempBet[i][j] = this.rankingLastBet[i][j];
                this.rankingTempBetText[i][j].setText(this.rankingLastBet[i][j] > 0 ? "+" + this.rankingLastBet[i][j] : "");
            }
        }
    };
    LobbyManager.prototype.resetRankingBet = function () {
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                this.rankingTempBet[i][j] = 0;
                this.rankingTempBetText[i][j].setText("");
            }
        }
    };
    LobbyManager.prototype.clearRankingBet = function () {
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                this.rankingTempBet[i][j] = 0;
                this.rankingTempBetText[i][j].setText("");
                this.rankingConfirmBet[i][j] = 0;
                this.rankingConfirmBetText[i][j].setText("0");
            }
        }
    };
    LobbyManager.prototype.updateBigSmallRate = function (rate) {
        this.bigSmallRate = rate;
        for (var rank = 0; rank < RANKING.TOTAL; rank++) {
            for (var optionIndex = 0; optionIndex < 6; optionIndex++) {
                if (rank > 3 && optionIndex > 3) {
                    break;
                }
                this.bigSmallRateText[rank][optionIndex].setText(rate[rank][optionIndex] + "");
            }
        }
    };
    LobbyManager.prototype.startBigSmallBet = function (rank, horseNumber) {
        if (this.gameBetHandling) {
            return;
        }
        this.bigSmallTempBet[rank][horseNumber] += this.chipSize[this.currentChip];
        this.bigSmallTempBetText[rank][horseNumber].setText("+" + this.bigSmallTempBet[rank][horseNumber]);
    };
    LobbyManager.prototype.confirmBigSmallBet = function () {
        var options = ["b", "s", "o", "e", "d", "t"];
        var userBet = 0;
        var betArray = new Array();
        for (var rank = 0; rank < 8; rank++) {
            for (var optionIndex = 0; optionIndex < 6; optionIndex++) {
                if (rank > 3 && optionIndex > 3) {
                    break;
                }
                if (this.bigSmallTempBet[rank][optionIndex] > 0) {
                    userBet += this.bigSmallTempBet[rank][optionIndex];
                    betArray.push({
                        str_type: (rank + 1) + "_" + options[optionIndex],
                        num_val: this.bigSmallTempBet[rank][optionIndex],
                        num_rate: this.bigSmallRate[rank][optionIndex]
                    });
                }
            }
        }
        this.betSignal.dispatch(BET_PAGE.BIG_SMALL_SINGLE_DOUBLE, userBet, betArray);
    };
    LobbyManager.prototype.bigSmallBetResult = function (betStatus) {
        if (betStatus) {
            for (var rank = 0; rank < 8; rank++) {
                for (var optionIndex = 0; optionIndex < 6; optionIndex++) {
                    if (rank > 3 && optionIndex > 3) {
                        break;
                    }
                    this.bigSmallLastBet[rank][optionIndex] = this.bigSmallTempBet[rank][optionIndex];
                    this.bigSmallConfirmBet[rank][optionIndex] += this.bigSmallTempBet[rank][optionIndex];
                    this.bigSmallConfirmBetText[rank][optionIndex].setText(this.bigSmallConfirmBet[rank][optionIndex] + "");
                }
            }
        }
        this.resetBigSmallBet();
    };
    LobbyManager.prototype.keepBigSmallBet = function () {
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 6; j++) {
                if (i > 3 && j > 3) {
                    break;
                }
                this.bigSmallTempBet[i][j] = this.bigSmallLastBet[i][j];
                this.bigSmallTempBetText[i][j].setText(this.bigSmallLastBet[i][j] > 0 ? "+" + this.bigSmallLastBet[i][j] : "");
            }
        }
    };
    LobbyManager.prototype.resetBigSmallBet = function () {
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 6; j++) {
                if (i > 3 && j > 3) {
                    break;
                }
                this.bigSmallTempBet[i][j] = 0;
                this.bigSmallTempBetText[i][j].setText("");
            }
        }
    };
    LobbyManager.prototype.clearBigSmallBet = function () {
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 6; j++) {
                if (i > 3 && j > 3) {
                    break;
                }
                this.bigSmallTempBet[i][j] = 0;
                this.bigSmallTempBetText[i][j].setText("");
                this.bigSmallConfirmBet[i][j] = 0;
                this.bigSmallConfirmBetText[i][j].setText("0");
            }
        }
    };
    LobbyManager.prototype.updateFirstSecondSumRate = function (rate) {
        this.firstSecondSumRate = rate;
        for (var i = 0; i < 17; i++) {
            this.firstSecondSumRateText[i].setText(rate[i] + "");
        }
    };
    LobbyManager.prototype.startFirstSecondSumBet = function (selectIndex) {
        if (this.gameBetHandling) {
            return;
        }
        this.firstSecondSumTempBet[selectIndex] += this.chipSize[this.currentChip];
        this.firstSecondSumTempBetText[selectIndex].setText("+" + this.firstSecondSumTempBet[selectIndex]);
    };
    LobbyManager.prototype.confirmFirstSecondSumBet = function () {
        var options = ["3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "b", "s", "o", "e"];
        var userBet = 0;
        var betArray = new Array();
        for (var i = 0; i < 17; i++) {
            if (this.firstSecondSumTempBet[i] > 0) {
                userBet += this.firstSecondSumTempBet[i];
                betArray.push({
                    str_type: "1p2_" + options[i],
                    num_val: this.firstSecondSumTempBet[i],
                    num_rate: this.firstSecondSumRate[i]
                });
            }
        }
        this.betSignal.dispatch(BET_PAGE.FIRST_SECOND_SUM, userBet, betArray);
    };
    LobbyManager.prototype.firstSecondSumResult = function (betStatus) {
        if (betStatus) {
            for (var i = 0; i < 17; i++) {
                this.firstSecondSumLastBet[i] = this.firstSecondSumTempBet[i];
                this.firstSecondSumConfirmBet[i] += this.firstSecondSumTempBet[i];
                this.firstSecondSumConfirmBetText[i].setText(this.firstSecondSumConfirmBet[i] + "");
            }
        }
        this.resetFirstSecondSumBet();
    };
    LobbyManager.prototype.keepFirstSecondSumBet = function () {
        for (var i = 0; i < 17; i++) {
            this.firstSecondSumTempBet[i] = this.firstSecondSumLastBet[i];
            this.firstSecondSumTempBetText[i].setText(this.firstSecondSumLastBet[i] > 0 ? "+" + this.firstSecondSumLastBet[i] : "");
        }
    };
    LobbyManager.prototype.resetFirstSecondSumBet = function () {
        for (var i = 0; i < 17; i++) {
            this.firstSecondSumTempBet[i] = 0;
            this.firstSecondSumTempBetText[i].setText("");
        }
    };
    LobbyManager.prototype.clearFirstSecondSumBet = function () {
        for (var i = 0; i < 17; i++) {
            this.firstSecondSumTempBet[i] = 0;
            this.firstSecondSumTempBetText[i].setText("");
            this.firstSecondSumConfirmBet[i] = 0;
            this.firstSecondSumConfirmBetText[i].setText("0");
        }
    };
    LobbyManager.prototype.updateFirstSecondRate = function (rate) {
        this.firstSecondRate = rate;
    };
    LobbyManager.prototype.changeFirstSecondSelected = function (rank, horseNumber) {
        if (this.firstSecondLockBet) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請完成或取消當前下注" }));
            return;
        }
        if (this.firstSecondCurrentHorseNumber[rank] != undefined) {
            this.firstSecondSpriteButton[rank][this.firstSecondCurrentHorseNumber[rank]].ResetButton();
        }
        this.firstSecondSpriteButton[rank][horseNumber].SelectButton();
        this.firstSecondCurrentHorseNumber[rank] = horseNumber;
    };
    LobbyManager.prototype.betFirstSecond = function (targetChip) {
        if (this.gameBetHandling) {
            return;
        }
        for (var i = 0; i < 2; i++) {
            if (this.firstSecondCurrentHorseNumber[i] == undefined) {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇冠軍、亞軍的馬號" }));
                return;
            }
        }
        if (this.firstSecondCurrentHorseNumber[0] == this.firstSecondCurrentHorseNumber[1]) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇不同的馬號" }));
            return;
        }
        if (this.firstSecondLockBet && this.firstSecondCurrentBettingBoardIndex != this.firstSecondLastBettingBoardIndex) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請完成或取消當前下注" }));
            return;
        }
        this.currentChip = targetChip;
        this.firstSecondLockBet = true;
        var alreadyExist = false;
        for (var i = 0; i < this.firstSecondNewBettingBoardIndex; i++) {
            if (this.firstSecondBettingBoard[i].checkHorseNumberIsMatch(this.firstSecondCurrentHorseNumber)) {
                alreadyExist = true;
                this.firstSecondCurrentBettingBoardIndex = i;
                break;
            }
        }
        if (alreadyExist) {
            this.startFirstSecondBet();
        }
        else {
            this.newFirstSecondBettingBoard();
        }
    };
    LobbyManager.prototype.newFirstSecondBettingBoard = function () {
        var rowIndex = Math.floor(this.firstSecondNewBettingBoardIndex / 4);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex] = new FirstSecondBettingBoard(this.game, this.firstSecondScrollGroup);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].position.set(35 + 477 * (this.firstSecondNewBettingBoardIndex % 4), 440 + 90 * rowIndex);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].updateHorseNumber(this.firstSecondCurrentHorseNumber);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].updateRate(this.firstSecondRate[this.firstSecondCurrentHorseNumber[0]][this.firstSecondCurrentHorseNumber[1]]);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
        this.firstSecondCurrentBettingBoardIndex = this.firstSecondNewBettingBoardIndex;
        this.firstSecondNewBettingBoardIndex++;
        this.firstSecondScrollLimit = rowIndex >= 5 ? -(rowIndex - 4) * 90 : 0;
        this.firstSecondScrollGroup.position.set(0, this.firstSecondScrollLimit);
    };
    LobbyManager.prototype.startFirstSecondBet = function () {
        this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
    };
    LobbyManager.prototype.confirmFirstSecondBet = function () {
        for (var i = 0; i < 2; i++) {
            if (this.firstSecondCurrentHorseNumber[i] == undefined) {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇冠軍、亞軍的馬號" }));
                return;
            }
        }
        if (this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex] == undefined || this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].parent == undefined) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇押注" }));
            return;
        }
        if (this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastBet() == 0) {
            return;
        }
        var userBet = this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastBet();
        var betArray = [{
                str_type: "1p2_" + (this.firstSecondCurrentHorseNumber[0] + 1) + "n" + (this.firstSecondCurrentHorseNumber[1] + 1),
                num_val: userBet,
                num_rate: this.firstSecondRate[this.firstSecondCurrentHorseNumber[0]][this.firstSecondCurrentHorseNumber[1]]
            }];
        this.betSignal.dispatch(BET_PAGE.FIRST_SECOND, userBet, betArray);
    };
    LobbyManager.prototype.firstSecondResult = function (betStatus) {
        if (betStatus) {
            this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].updateConfirmBet();
            this.firstSecondLastBet = this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastBet();
            this.firstSecondLastHorseNumber = this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastHorseNumber();
            this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].clearTempBet();
            this.firstSecondLastBettingBoardIndex = this.firstSecondCurrentBettingBoardIndex;
            this.firstSecondLockBet = false;
        }
        else {
            this.resetFirstSecondBet();
        }
    };
    LobbyManager.prototype.keepFirstSecondBet = function () {
        if (this.firstSecondLastBet == 0) {
            return;
        }
        if (this.firstSecondLockBet) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請完成或取消當前下注" }));
            return;
        }
        this.firstSecondLockBet = true;
        if (this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex] == undefined || this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].parent == undefined) {
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex] = new FirstSecondBettingBoard(this.game, this.firstSecondScrollGroup);
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].position.set(35, 440);
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].updateHorseNumber(this.firstSecondLastHorseNumber);
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].keepTempBet(this.firstSecondLastBet);
            this.firstSecondNewBettingBoardIndex++;
        }
        else {
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].keepTempBet(this.firstSecondLastBet);
        }
        this.firstSecondCurrentBettingBoardIndex = this.firstSecondLastBettingBoardIndex;
    };
    LobbyManager.prototype.resetFirstSecondBet = function () {
        if (this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex] != undefined) {
            if (this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getConfirmBet() > 0) {
                this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].clearTempBet();
            }
            else {
                this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].destroy();
                this.firstSecondNewBettingBoardIndex = this.firstSecondCurrentBettingBoardIndex;
                var rowIndex = Math.floor((this.firstSecondCurrentBettingBoardIndex - 1) / 4);
                this.firstSecondScrollLimit = rowIndex >= 5 ? -(rowIndex - 4) * 90 : 0;
                this.firstSecondScrollGroup.position.set(0, this.firstSecondScrollLimit);
            }
            this.firstSecondLockBet = false;
        }
    };
    LobbyManager.prototype.clearFirstSecondBet = function () {
        this.firstSecondCurrentBettingBoardIndex = 0;
        this.firstSecondNewBettingBoardIndex = 0;
        this.firstSecondScrollGroup.children.length = 0;
        this.firstSecondScrollGroup.position.set(0);
        this.firstSecondScrollLimit = 0;
        this.firstSecondBettingBoard = new Array(56);
        this.firstSecondLockBet = false;
    };
    LobbyManager.prototype.startFirstSecondScroll = function () {
        var _this = this;
        if (this.firstSecondScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.firstSecondScrollGroup.y;
        this.firstSecondScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.firstSecondScrollLimit) {
                scrollPosY = _this.firstSecondScrollLimit;
            }
            _this.firstSecondScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    LobbyManager.prototype.stopFirstSecondScroll = function () {
        this.game.time.events.remove(this.firstSecondScrollTimerEvent);
    };
    LobbyManager.prototype.updateFirstSecondThirdRate = function (rate) {
        this.firstSecondThirdRate = rate;
    };
    LobbyManager.prototype.changeFirstSecondThirdSelected = function (rank, horseNumber) {
        if (this.firstSecondThirdLockBet) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請完成或取消當前下注" }));
            return;
        }
        if (this.firstSecondThirdCurrentHorseNumber[rank] != undefined) {
            this.firstSecondThirdSpriteButton[rank][this.firstSecondThirdCurrentHorseNumber[rank]].ResetButton();
        }
        this.firstSecondThirdSpriteButton[rank][horseNumber].SelectButton();
        this.firstSecondThirdCurrentHorseNumber[rank] = horseNumber;
    };
    LobbyManager.prototype.betFirstSecondThird = function (targetChip) {
        if (this.gameBetHandling) {
            return;
        }
        for (var i = 0; i < 3; i++) {
            if (this.firstSecondThirdCurrentHorseNumber[i] == undefined) {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇冠軍、亞軍、季軍的馬號" }));
                return;
            }
        }
        if (this.firstSecondThirdCurrentHorseNumber[0] == this.firstSecondThirdCurrentHorseNumber[1]
            || this.firstSecondThirdCurrentHorseNumber[0] == this.firstSecondThirdCurrentHorseNumber[2]
            || this.firstSecondThirdCurrentHorseNumber[1] == this.firstSecondThirdCurrentHorseNumber[2]) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇不同的馬號" }));
            return;
        }
        if (this.firstSecondThirdLockBet && this.firstSecondThirdCurrentBettingBoardIndex != this.firstSecondThirdLastBettingBoardIndex) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請完成或取消當前下注" }));
            return;
        }
        this.currentChip = targetChip;
        this.firstSecondThirdLockBet = true;
        var alreadyExist = false;
        for (var i = 0; i < this.firstSecondThirdNewBettingBoardIndex; i++) {
            if (this.firstSecondThirdBettingBoard[i].checkHorseNumberIsMatch(this.firstSecondThirdCurrentHorseNumber)) {
                alreadyExist = true;
                this.firstSecondThirdCurrentBettingBoardIndex = i;
                break;
            }
        }
        if (alreadyExist) {
            this.startFirstSecondThirdBet();
        }
        else {
            this.newFirstSecondThirdBettingBoard();
        }
    };
    LobbyManager.prototype.newFirstSecondThirdBettingBoard = function () {
        var rowIndex = Math.floor(this.firstSecondThirdNewBettingBoardIndex / 3);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex] = new FirstSecondThirdBettingBoard(this.game, this.firstSecondThirdScrollGroup);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].updateHorseNumber(this.firstSecondThirdCurrentHorseNumber);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].updateRate(this.firstSecondThirdRate[this.firstSecondThirdCurrentHorseNumber[0]][this.firstSecondThirdCurrentHorseNumber[1]][this.firstSecondThirdCurrentHorseNumber[2]]);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
        this.firstSecondThirdCurrentBettingBoardIndex = this.firstSecondThirdNewBettingBoardIndex;
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].position.set(35 + 638 * (this.firstSecondThirdNewBettingBoardIndex % 3), 525 + 90 * rowIndex);
        this.firstSecondThirdNewBettingBoardIndex++;
        this.firstSecondThirdScrollLimit = rowIndex >= 4 ? -(rowIndex - 3) * 90 : 0;
        this.firstSecondThirdScrollGroup.position.set(0, this.firstSecondThirdScrollLimit);
    };
    LobbyManager.prototype.startFirstSecondThirdBet = function () {
        this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
    };
    LobbyManager.prototype.confirmFirstSecondThirdBet = function () {
        for (var i = 0; i < 3; i++) {
            if (this.firstSecondThirdCurrentHorseNumber[i] == undefined) {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇冠軍、亞軍、季軍的馬號" }));
                return;
            }
        }
        if (this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex] == undefined || this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].parent == undefined) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請選擇押注" }));
            return;
        }
        if (this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastBet() == 0) {
            return;
        }
        var userBet = this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastBet();
        var betArray = [{
                str_type: "1p2p3_" + (this.firstSecondThirdCurrentHorseNumber[0] + 1) + "n" + (this.firstSecondThirdCurrentHorseNumber[1] + 1) + "n" + (this.firstSecondThirdCurrentHorseNumber[2] + 1),
                num_val: userBet,
                num_rate: this.firstSecondThirdRate[this.firstSecondThirdCurrentHorseNumber[0]][this.firstSecondThirdCurrentHorseNumber[1]][this.firstSecondThirdCurrentHorseNumber[2]]
            }];
        this.betSignal.dispatch(BET_PAGE.FIRST_SECOND_THIRD, userBet, betArray);
    };
    LobbyManager.prototype.firstSecondThirdResult = function (betStatus) {
        if (betStatus) {
            this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].updateConfirmBet();
            this.firstSecondThirdLastBet = this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastBet();
            this.firstSecondThirdLastHorseNumber = this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastHorseNumber();
            this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].clearTempBet();
            this.firstSecondThirdLastBettingBoardIndex = this.firstSecondThirdCurrentBettingBoardIndex;
            this.firstSecondThirdLockBet = false;
        }
        else {
            this.resetFirstSecondThirdBet();
        }
    };
    LobbyManager.prototype.keepFirstSecondThirdBet = function () {
        if (this.firstSecondThirdLastBet == 0) {
            return;
        }
        if (this.firstSecondThirdLockBet) {
            window.dispatchEvent(new CustomEvent('Warning', { 'detail': "請完成或取消當前下注" }));
            return;
        }
        this.firstSecondThirdLockBet = true;
        if (this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex] == undefined || this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].parent == undefined) {
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex] = new FirstSecondThirdBettingBoard(this.game, this.firstSecondThirdScrollGroup);
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].position.set(35, 525);
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].updateHorseNumber(this.firstSecondThirdLastHorseNumber);
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].keepTempBet(this.firstSecondThirdLastBet);
            this.firstSecondThirdNewBettingBoardIndex++;
        }
        else {
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].keepTempBet(this.firstSecondThirdLastBet);
        }
        this.firstSecondThirdCurrentBettingBoardIndex = this.firstSecondThirdLastBettingBoardIndex;
    };
    LobbyManager.prototype.resetFirstSecondThirdBet = function () {
        if (this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex] != undefined) {
            if (this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getConfirmBet() > 0) {
                this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].clearTempBet();
            }
            else {
                this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].destroy();
                this.firstSecondThirdNewBettingBoardIndex = this.firstSecondThirdCurrentBettingBoardIndex;
                var rowIndex = Math.floor((this.firstSecondThirdCurrentBettingBoardIndex - 1) / 3);
                this.firstSecondThirdScrollLimit = rowIndex >= 4 ? -(rowIndex - 3) * 90 : 0;
                this.firstSecondThirdScrollGroup.position.set(0, this.firstSecondThirdScrollLimit);
            }
            this.firstSecondThirdLockBet = false;
        }
    };
    LobbyManager.prototype.clearFirstSecondThirdBet = function () {
        this.firstSecondThirdCurrentBettingBoardIndex = 0;
        this.firstSecondThirdNewBettingBoardIndex = 0;
        this.firstSecondThirdScrollGroup.children.length = 0;
        this.firstSecondThirdScrollGroup.position.set(0);
        this.firstSecondThirdScrollLimit = 0;
        this.firstSecondThirdBettingBoard = new Array(336);
        this.firstSecondThirdLockBet = false;
    };
    LobbyManager.prototype.startFirstSecondThirdScroll = function () {
        var _this = this;
        if (this.firstSecondThirdScrollLimit == 0) {
            return;
        }
        var firstTouchPosY = this.game.input.y;
        var scrollPosY = this.firstSecondThirdScrollGroup.y;
        this.firstSecondThirdScrollTimerEvent = this.game.time.events.loop(50, function () {
            scrollPosY -= firstTouchPosY - _this.game.input.y;
            if (scrollPosY > 0) {
                scrollPosY = 0;
            }
            if (scrollPosY < _this.firstSecondThirdScrollLimit) {
                scrollPosY = _this.firstSecondThirdScrollLimit;
            }
            _this.firstSecondThirdScrollGroup.position.set(0, scrollPosY);
        }, this);
    };
    LobbyManager.prototype.stopFirstSecondThirdScroll = function () {
        this.game.time.events.remove(this.firstSecondThirdScrollTimerEvent);
    };
    LobbyManager.prototype.clearAllPageBet = function () {
        this.gameBetHandling = false;
        this.clearUserTotalBetSignal.dispatch();
        this.clearRankingBet();
        this.clearBigSmallBet();
        this.clearFirstSecondSumBet();
        this.clearFirstSecondBet();
        this.clearFirstSecondThirdBet();
    };
    LobbyManager.prototype.changeBetPage = function (targetSpriteButton, targetGroup, targetBetPage) {
        var _this = this;
        this.currentSelectedPage.ResetButton();
        this.currentSelectedPage = targetSpriteButton;
        this.currentSelectedPage.SelectButton();
        this.currentShowGroup.visible = false;
        this.currentShowGroup = targetGroup;
        this.currentShowGroup.visible = true;
        this.confirmBet.OnUp.removeAll();
        this.keepBet.OnUp.removeAll();
        this.resetBet.OnUp.removeAll();
        switch (targetBetPage) {
            case BET_PAGE.RANKING:
                this.confirmBet.OnUp.add(this.confirmRankingBet, this);
                this.keepBet.OnUp.add(this.keepRankingBet, this);
                this.resetBet.OnUp.add(this.resetRankingBet, this);
                break;
            case BET_PAGE.BIG_SMALL_SINGLE_DOUBLE:
                this.confirmBet.OnUp.add(this.confirmBigSmallBet, this);
                this.keepBet.OnUp.add(this.keepBigSmallBet, this);
                this.resetBet.OnUp.add(this.resetBigSmallBet, this);
                break;
            case BET_PAGE.FIRST_SECOND_SUM:
                this.confirmBet.OnUp.add(this.confirmFirstSecondSumBet, this);
                this.keepBet.OnUp.add(this.keepFirstSecondSumBet, this);
                this.resetBet.OnUp.add(this.resetFirstSecondSumBet, this);
                break;
            case BET_PAGE.FIRST_SECOND:
                this.confirmBet.OnUp.add(this.confirmFirstSecondBet, this);
                this.keepBet.OnUp.add(this.keepFirstSecondBet, this);
                this.resetBet.OnUp.add(this.resetFirstSecondBet, this);
                break;
            case BET_PAGE.FIRST_SECOND_THIRD:
                this.confirmBet.OnUp.add(this.confirmFirstSecondThirdBet, this);
                this.keepBet.OnUp.add(this.keepFirstSecondThirdBet, this);
                this.resetBet.OnUp.add(this.resetFirstSecondThirdBet, this);
                break;
        }
        var _loop_14 = function (i) {
            this_10.chipSpriteButton[i].OnUp.removeAll();
            if (targetBetPage < 3) {
                this_10.chipSpriteButton[i].OnUp.add(function () {
                    _this.changeChipSeleted(i);
                }, this_10);
            }
            else if (targetBetPage < 4) {
                this_10.chipSpriteButton[i].OnUp.add(function () {
                    _this.betFirstSecond(i);
                }, this_10);
            }
            else {
                this_10.chipSpriteButton[i].OnUp.add(function () {
                    _this.betFirstSecondThird(i);
                }, this_10);
            }
        };
        var this_10 = this;
        for (var i = 0; i < 6; i++) {
            _loop_14(i);
        }
        if (targetBetPage < 3) {
            for (var i = 0; i < 6; i++) {
                if (i == this.currentChip) {
                    this.chipSpriteButton[i].SelectButton();
                }
                else {
                    this.chipSpriteButton[i].UnselectedButton();
                }
                this.chipSpriteButton[i].isSingleSeleted = true;
            }
        }
        else {
            for (var i = 0; i < 6; i++) {
                this.chipSpriteButton[i].ResetButton();
                this.chipSpriteButton[i].isSingleSeleted = false;
            }
        }
    };
    LobbyManager.prototype.changeChipSeleted = function (targetChip) {
        this.chipSpriteButton[this.currentChip].UnselectedButton();
        this.currentChip = targetChip;
        this.chipSpriteButton[this.currentChip].SelectButton();
    };
    return LobbyManager;
}(Phaser.Group));
var LoginPage = (function (_super) {
    __extends(LoginPage, _super);
    function LoginPage(game) {
        var _this = _super.call(this, game) || this;
        _this.initialize();
        return _this;
    }
    LoginPage.prototype.initialize = function () {
        var _this = this;
        this.loginSignal = new Phaser.Signal();
        this.game.add.image(0, 0, 'MainBG', null, this);
        this.loginPanel = this.game.add.group(this);
        this.loginPanel.visible = false;
        this.game.add.image(960, 540, 'LoginPanel', null, this.loginPanel).anchor.set(0.5);
        var userAccountInput = this.game.add.inputField(725, 443, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 640,
            cursorColor: '#ffffff',
            textAlign: 'left',
            type: PhaserInput.InputType.text
        }, this.loginPanel);
        var userPasswordInput = this.game.add.inputField(725, 625, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 640,
            cursorColor: '#ffffff',
            textAlign: 'left',
            type: PhaserInput.InputType.password
        }, this.loginPanel);
        var loginButton = new SpriteButton(this.game, 960, 820, 'Button', 'LoginButton_Up', {
            Key: 'Button',
            OverFrame: 'LoginButton_Over',
            OutFrame: 'LoginButton_Up',
            UpFrame: 'LoginButton_Up',
            DownFrame: 'LoginButton_Down'
        });
        loginButton.anchor.set(0.5);
        loginButton.OnUp.add(function () {
            if (userAccountInput.value == "" || userPasswordInput.value == "") {
                window.dispatchEvent(new CustomEvent('Warning', { 'detail': "帳號及密碼不能為空" }));
                return;
            }
            _this.loginSignal.dispatch(userAccountInput.value, userPasswordInput.value);
            userAccountInput.resetText();
            userPasswordInput.resetText();
            _this.loginPanel.visible = false;
        }, this.loginPanel);
        this.loginPanel.add(loginButton);
    };
    LoginPage.prototype.logInAgain = function () {
        this.loginPanel.visible = true;
    };
    return LoginPage;
}(Phaser.Group));
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        return _super.call(this) || this;
    }
    Main.prototype.preload = function () {
        this.game.add.image(0, 0, 'MainBG');
        var progressBG1 = this.game.add.sprite(960, 925, 'Preloader', 'LoadingProgressBG_1');
        progressBG1.anchor.set(0.5);
        var progressBG2 = this.game.add.sprite(960, 930, 'Preloader', 'LoadingProgressBG_2');
        progressBG2.anchor.set(0.5);
        var progressTitle = this.game.add.sprite(960, 1000, 'Preloader', 'LoadingTitle');
        progressTitle.anchor.set(0.5);
        this.progressBar = this.game.add.sprite(172, 930, 'Preloader', 'LoadingProgressBar');
        this.progressBar.anchor.set(0, 0.5);
        this.progressBar.scale.set(0, 1);
        this.load.onFileComplete.add(this.allFileComplete, this);
        this.load.onLoadComplete.add(this.allLoadComplete, this);
        this.load.pack('main', 'assets/pack.json');
    };
    Main.prototype.allFileComplete = function (progress) {
        this.progressBar.scale.set(progress * 0.01195, 1);
    };
    Main.prototype.allLoadComplete = function () {
        this.load.onFileComplete.remove(this.allFileComplete, this);
        this.load.onLoadComplete.remove(this.allLoadComplete, this);
        this.initialize();
    };
    Main.prototype.initialize = function () {
        var _this = this;
        this.game.plugins.add(PhaserInput.Plugin);
        this.currentKey = localStorage.getItem('gameCurrentKey');
        this.passwordNeedChange = false;
        this.loginPage = new LoginPage(this.game);
        this.loginPage.loginSignal.add(function (account, password) {
            var postData = {
                str_acc: account,
                str_pwd: password
            };
            _this.server.postData(DATA_URL.MEMBER_LOGIN, JSON.stringify(postData));
        }, this);
        this.soundManager = new SoundManager(this.game);
        this.lobbyManager = new LobbyManager(this.game, this.soundManager);
        this.lobbyManager.betSignal.add(function (betPage, userBet, betArray) {
            _this.lobbyManager.gameBetHandling = true;
            _this.handlingBetPage = betPage;
            _this.handlingUserBet = userBet;
            if (_this.commonWindow.checkUserBetIsSuccessed(userBet)) {
                var postData = {
                    oddsPrompt: 0,
                    str_cur_page: betPage,
                    ary_bet: betArray
                };
                _this.server.postData(DATA_URL.GAME_BET + _this.currentKey, JSON.stringify(postData));
            }
            else {
                _this.receiveServerError(504);
                _this.gameBetResult(false);
            }
        }, this);
        this.lobbyManager.clearUserTotalBetSignal.add(function () {
            localStorage.setItem(_this.userAccount + 'userTotalBet', "0");
            _this.commonWindow.updateUserTotalBet("0");
        }, this);
        this.lobbyManager.visible = false;
        this.gameManager = new GameManager(this.game, this.soundManager);
        this.gameManager.endGameSignal.add(this.endGame, this);
        this.gameManager.visible = false;
        this.commonWindow = new CommonWindow(this.game, this.soundManager);
        this.commonWindow.switchGameSceneSignal.add(this.switchGameScene, this);
        this.commonWindow.logOutSignal.add(function () {
            _this.server.getData(DATA_URL.MEMBER_LOGOUT);
        }, this);
        this.commonWindow.requestDataSignal.add(function (url, postData) {
            if (url == DATA_URL.PASSWORD_CHANGE) {
                _this.passwordNeedChange = true;
            }
            _this.server.postData(url + _this.gameId, postData);
        }, this);
        this.commonWindow.visible = false;
        this.server = new Server(this);
        this.server.getData(DATA_URL.CHECK_LOGIN);
    };
    Main.prototype.switchGameScene = function (isShowGame) {
        this.gameManager.visible = isShowGame;
        this.lobbyManager.visible = !isShowGame;
        this.commonWindow.gameGroup.visible = isShowGame;
        this.commonWindow.lobbyGroup.visible = !isShowGame;
    };
    Main.prototype.startGame = function (data) {
        var countdownTime = data.objCur.intSecLeft;
        this.lobbyManager.startNextGameCountdown(countdownTime);
        this.gameManager.startNextGameCountdown(countdownTime);
        if (this.currentKey != data.objCur.strKey) {
            this.currentKey = data.objCur.strKey;
            localStorage.setItem('gameCurrentKey', this.currentKey);
            localStorage.setItem(this.userAccount + 'userTotalBet', "0");
            this.commonWindow.updateUserTotalBet("0");
        }
        var resultLength = data.objLast.aryNum.length;
        if (resultLength > 0) {
            var result = new Array(resultLength);
            for (var i = 0; i < resultLength; i++) {
                result[i] = data.objLast.aryNum[i].split("")[1];
            }
            this.gameManager.racingStop(result);
        }
    };
    Main.prototype.endGame = function () {
        this.soundManager.playSound(SOUND.GAME_END);
    };
    Main.prototype.receiveServerData = function (url, packet) {
        var _this = this;
        if (packet.code != 100) {
            console.error();
            return;
        }
        else {
            if (this.passwordNeedChange) {
                this.passwordNeedChange = this.commonWindow.panelMask.visible = this.commonWindow.passwordChangeGroup.visible = false;
                window.dispatchEvent(new CustomEvent('Success', { 'detail': "密碼變更成功！請重新登入" }));
                this.goToLoginPage();
            }
        }
        switch (url) {
            case DATA_URL.MEMBER_LOGOUT:
                this.goToLoginPage();
                break;
            case DATA_URL.MEMBER_LOGIN:
            case DATA_URL.CHECK_LOGIN:
                this.userAccount = packet.data.str_acc;
                var userTotalBet = localStorage.getItem(this.userAccount + 'userTotalBet') != null ? localStorage.getItem(this.userAccount + 'userTotalBet') : "0";
                this.commonWindow.updateUserTotalBet(userTotalBet);
                this.goToGame(parseFloat(packet.data.objBase.num_balance));
                this.server.getData(DATA_URL.GAME_LIST);
                break;
            case DATA_URL.GAME_LIST:
                this.gameId = packet.data[0].id;
                this.server.getData(DATA_URL.GAME_RESULT + this.gameId);
                this.server.getData(DATA_URL.GAME_INFO + this.gameId);
                this.game.time.events.loop(10000, function () {
                    _this.server.getData(DATA_URL.GAME_RESULT + _this.gameId);
                    _this.server.getData(DATA_URL.GAME_INFO + _this.gameId);
                }, this);
                this.game.time.events.loop(5000, function () {
                    _this.server.getData(DATA_URL.BET_DATA + _this.currentKey);
                }, this);
                break;
            case DATA_URL.GAME_RESULT + this.gameId:
                this.startGame(packet.data);
                break;
            case DATA_URL.GAME_INFO + this.gameId:
                this.updateGameRate(packet.data.objRate);
                break;
            case DATA_URL.GAME_BET + this.currentKey:
                this.gameBetResult(true);
                this.commonWindow.updateUserBalance(packet.data.num_balance);
                this.server.getData(DATA_URL.BET_DATA + this.currentKey);
                break;
            case DATA_URL.DAY_BET_RECORD + this.gameId:
                if (packet.data.length == 0) {
                    window.dispatchEvent(new CustomEvent('Warning', { 'detail': "查詢無資料" }));
                }
                this.commonWindow.showBetRecord(packet.data);
                break;
            case DATA_URL.OPEN_HISTORY + this.gameId:
                var dataLength = packet.data.length > 20 ? 20 : packet.data.length;
                var historicalLotteryData = new Array(dataLength);
                for (var i = 0; i < dataLength; i++) {
                    historicalLotteryData[i] = {
                        periods: packet.data[i].strIssue,
                        time: packet.data[i].timeOpen,
                        result: packet.data[i].aryNum,
                        firstSecondSum: packet.data[i].num1p2,
                        dragonTiger: packet.data[i].aryDT
                    };
                }
                this.commonWindow.openHistoricalLotteryForm(historicalLotteryData);
                break;
            case DATA_URL.BET_DATA + this.currentKey:
                this.lobbyManager.updateTotalBet(Number(packet.data.numBetTol));
                this.gameManager.updateTotalBet(Number(packet.data.numBetTol));
                this.gameManager.updateLowestAndHighestBet(Number(packet.data.numBetMin), Number(packet.data.numBetMax));
                break;
            case DATA_URL.GET_USER_INFO:
                this.commonWindow.updateUserBalance(Number(packet.data.num_balance));
                break;
        }
    };
    Main.prototype.receiveServerError = function (errorCode) {
        var errorMessage = "";
        switch (errorCode) {
            case 200:
                errorMessage = "無此會員";
                break;
            case 201:
                errorMessage = "原密碼錯誤";
                this.passwordNeedChange = false;
                this.commonWindow.passwordChangeGroup.visible = true;
                break;
            case 202:
                errorMessage = "無此代理商";
                break;
            case 301:
                errorMessage = "帳號格式錯誤";
                break;
            case 401:
                errorMessage = "帳號密碼輸入錯誤";
                this.goToLoginPage();
                break;
            case 500:
                errorMessage = "一般錯誤";
                break;
            case 501:
                errorMessage = "下注期數錯誤";
                break;
            case 502:
                errorMessage = "下注期數已關閉";
                break;
            case 503:
                errorMessage = "無效的下注金額";
                break;
            case 504:
                errorMessage = "餘額不足";
                break;
            case 505:
                errorMessage = "超過單注限額";
                break;
            case 506:
                errorMessage = "超過單局限額";
                break;
            case 507:
                errorMessage = "其他下注錯誤";
                break;
            case 508:
                errorMessage = "遊戲設定異常";
                break;
        }
        window.dispatchEvent(new CustomEvent('Error', { 'detail': errorMessage }));
        if (errorCode > 500) {
            this.gameBetResult(false);
        }
    };
    Main.prototype.gameBetResult = function (betStatus) {
        if (betStatus) {
            var userTotalBet = localStorage.getItem(this.userAccount + 'userTotalBet') != null ? Number(localStorage.getItem(this.userAccount + 'userTotalBet')) : 0;
            userTotalBet += this.handlingUserBet;
            localStorage.setItem(this.userAccount + 'userTotalBet', userTotalBet + "");
            this.commonWindow.updateUserTotalBet(userTotalBet + "");
        }
        switch (this.handlingBetPage) {
            case BET_PAGE.RANKING:
                this.lobbyManager.rankingBetResult(betStatus);
                break;
            case BET_PAGE.BIG_SMALL_SINGLE_DOUBLE:
                this.lobbyManager.bigSmallBetResult(betStatus);
                break;
            case BET_PAGE.FIRST_SECOND_SUM:
                this.lobbyManager.firstSecondSumResult(betStatus);
                break;
            case BET_PAGE.FIRST_SECOND:
                this.lobbyManager.firstSecondResult(betStatus);
                break;
            case BET_PAGE.FIRST_SECOND_THIRD:
                this.lobbyManager.firstSecondThirdResult(betStatus);
                break;
        }
        this.lobbyManager.gameBetHandling = false;
    };
    Main.prototype.goToLoginPage = function () {
        this.game.time.events.remove(this.updateBalanceTimerEvent);
        this.game.time.removeAll();
        this.lobbyManager.visible = false;
        this.gameManager.visible = false;
        this.commonWindow.visible = false;
        this.loginPage.visible = true;
        this.loginPage.logInAgain();
    };
    Main.prototype.goToGame = function (balance) {
        var _this = this;
        this.commonWindow.updateUserBalance(balance);
        this.commonWindow.visible = true;
        this.switchGameScene(true);
        this.updateBalanceTimerEvent = this.game.time.events.loop(10000, function () {
            _this.server.getData(DATA_URL.GET_USER_INFO);
        }, this);
    };
    Main.prototype.updateGameRate = function (gameRate) {
        var rankingRate = new Array(8);
        var bigSmallRate = new Array(8);
        var firstSecondSumRate = new Array(17);
        var firstSecondRate = new Array(8);
        var firstSecondThirdRate = new Array(8);
        for (var i = 0; i < 8; i++) {
            rankingRate[i] = new Array(8);
            bigSmallRate[i] = new Array(6);
            firstSecondRate[i] = new Array(8);
            firstSecondThirdRate[i] = new Array(8);
            for (var j = 0; j < 8; j++) {
                firstSecondThirdRate[i][j] = new Array(8);
            }
        }
        Object.keys(gameRate).forEach(function (key) {
            var firstByte = key.split("_")[0];
            var secondByte = key.split("_")[1];
            switch (firstByte) {
                case "1p2":
                    if (secondByte.split("").length == 3) {
                        firstByte = secondByte.split("n")[0];
                        secondByte = secondByte.split("n")[1];
                        firstSecondRate[parseInt(firstByte) - 1][parseInt(secondByte) - 1] = gameRate[key];
                    }
                    else {
                        switch (secondByte) {
                            case "b":
                                firstSecondSumRate[13] = gameRate[key];
                                break;
                            case "s":
                                firstSecondSumRate[14] = gameRate[key];
                                break;
                            case "o":
                                firstSecondSumRate[15] = gameRate[key];
                                break;
                            case "e":
                                firstSecondSumRate[16] = gameRate[key];
                                break;
                            default:
                                firstSecondSumRate[parseInt(secondByte) - 3] = gameRate[key];
                                break;
                        }
                    }
                    break;
                case "1p2p3":
                    var thirdByte = secondByte.split("n")[2];
                    firstByte = secondByte.split("n")[0];
                    secondByte = secondByte.split("n")[1];
                    firstSecondThirdRate[parseInt(firstByte) - 1][parseInt(secondByte) - 1][parseInt(thirdByte) - 1] = gameRate[key];
                    break;
                default:
                    switch (secondByte) {
                        case "b":
                            bigSmallRate[parseInt(firstByte) - 1][0] = gameRate[key];
                            break;
                        case "s":
                            bigSmallRate[parseInt(firstByte) - 1][1] = gameRate[key];
                            break;
                        case "o":
                            bigSmallRate[parseInt(firstByte) - 1][2] = gameRate[key];
                            break;
                        case "e":
                            bigSmallRate[parseInt(firstByte) - 1][3] = gameRate[key];
                            break;
                        case "d":
                            bigSmallRate[parseInt(firstByte) - 1][4] = gameRate[key];
                            break;
                        case "t":
                            bigSmallRate[parseInt(firstByte) - 1][5] = gameRate[key];
                            break;
                        default:
                            rankingRate[parseInt(firstByte) - 1][parseInt(secondByte) - 1] = gameRate[key];
                            break;
                    }
                    break;
            }
        });
        this.lobbyManager.updateRankingRate(rankingRate);
        this.lobbyManager.updateBigSmallRate(bigSmallRate);
        this.lobbyManager.updateFirstSecondSumRate(firstSecondSumRate);
        this.lobbyManager.updateFirstSecondRate(firstSecondRate);
        this.lobbyManager.updateFirstSecondThirdRate(firstSecondThirdRate);
    };
    return Main;
}(Phaser.State));
var Preloader = (function (_super) {
    __extends(Preloader, _super);
    function Preloader() {
        return _super.call(this) || this;
    }
    Preloader.prototype.preload = function () {
        this.load.onLoadComplete.add(this.preloadComplete, this);
        this.load.pack('preloader', 'assets/pack.json');
    };
    Preloader.prototype.preloadComplete = function () {
        this.load.onLoadComplete.remove(this.preloadComplete, this);
        this.game.state.add("Main", Main);
        this.game.state.start("Main", true, false);
    };
    return Preloader;
}(Phaser.State));
var Server = (function () {
    function Server(main) {
        this.main = main;
    }
    Server.prototype.postData = function (url, data) {
        var _this = this;
        var server = new XMLHttpRequest;
        server.onreadystatechange = function () {
            if (server.readyState === XMLHttpRequest.DONE) {
                if (server.status === 200) {
                    _this.main.receiveServerData(url, JSON.parse(server.response));
                }
                else {
                    _this.main.receiveServerError(server.status);
                }
            }
        };
        server.open("POST", "http://172.105.114.236/lotteryhorse/api/" + url, true);
        server.withCredentials = true;
        server.setRequestHeader("Content-type", "application/json");
        server.send(data);
    };
    Server.prototype.getData = function (url) {
        var _this = this;
        var server = new XMLHttpRequest;
        server.onreadystatechange = function () {
            if (server.readyState === XMLHttpRequest.DONE) {
                if (server.status === 200) {
                    _this.main.receiveServerData(url, JSON.parse(server.response));
                }
                else if (server.status == 400) {
                    _this.main.goToLoginPage();
                }
            }
        };
        server.open("GET", "http://172.105.114.236/lotteryhorse/api/" + url, true);
        server.withCredentials = true;
        server.send();
    };
    return Server;
}());
var SoundManager = (function () {
    function SoundManager(game) {
        this.game = game;
        this.initialize();
    }
    SoundManager.prototype.initialize = function () {
        var soundData = this.game.cache.getJSON('SoundKey').Sound;
        var soundDataLength = soundData.length;
        this.sound = new Array(soundDataLength);
        for (var i = 0; i < soundDataLength; i++) {
            this.sound[i] = this.game.add.audio(soundData[i].Key, 1, soundData[i].Loop);
        }
        var soundEffectData = this.game.cache.getJSON('SoundKey').SoundEffect;
        var soundEffectDataLength = soundEffectData.length;
        this.soundEffect = new Array(soundEffectDataLength);
        for (var i = 0; i < soundEffectDataLength; i++) {
            this.soundEffect[i] = this.game.add.audio(soundEffectData[i].Key, 1, soundEffectData[i].Loop);
        }
    };
    SoundManager.prototype.playSound = function (soundIndex) {
        this.sound[soundIndex].play();
    };
    SoundManager.prototype.stopSound = function (soundIndex) {
        this.sound[soundIndex].stop();
    };
    SoundManager.prototype.changeSoundVolume = function (newVolume) {
        var soundLength = this.sound.length;
        for (var i = 0; i < soundLength; i++) {
            this.sound[i].volume = newVolume;
        }
    };
    SoundManager.prototype.playSoundEffect = function (soundEffectIndex) {
        this.soundEffect[soundEffectIndex].play();
    };
    SoundManager.prototype.stopSoundEffect = function (soundEffectIndex) {
        this.soundEffect[soundEffectIndex].stop();
    };
    SoundManager.prototype.changeSoundEffectVolume = function (newVolume) {
        var soundEffectLength = this.soundEffect.length;
        for (var i = 0; i < soundEffectLength; i++) {
            this.soundEffect[i].volume = newVolume;
        }
    };
    return SoundManager;
}());
var SpriteButton = (function (_super) {
    __extends(SpriteButton, _super);
    function SpriteButton(game, x, y, key, frame, SpriteButtonStyle) {
        var _this = _super.call(this, game, x, y, key, frame) || this;
        _this.OnOver = new Phaser.Signal();
        _this.OnOut = new Phaser.Signal();
        _this.OnDown = new Phaser.Signal();
        _this.OnUp = new Phaser.Signal();
        _this.OnLongClick = new Phaser.Signal();
        _this.isLongClicking = false;
        _this.EnableLongClick = false;
        _this.isSingleSeleted = false;
        _this.inputEnabled = true;
        _this.input.useHandCursor = true;
        _this.SpriteButtonStyle = SpriteButtonStyle;
        _this.events.onInputOver.add(_this.onInputOver, _this);
        _this.events.onInputOut.add(_this.onInputOut, _this);
        _this.events.onInputDown.add(_this.onInputDown, _this);
        _this.events.onInputUp.add(_this.onInputUp, _this);
        return _this;
    }
    SpriteButton.prototype.onInputOver = function () {
        if (this.isSingleSeleted) {
            return;
        }
        if (this.SpriteButtonStyle != undefined) {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.OverFrame);
        }
        else {
            this.tint = 0xbababa;
        }
        this.OnOver.dispatch(this);
    };
    SpriteButton.prototype.onInputOut = function () {
        if (this.isSingleSeleted) {
            return;
        }
        if (this.SpriteButtonStyle != undefined) {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.OutFrame);
        }
        else {
            this.tint = 0xffffff;
        }
        this.OnOut.dispatch(this);
    };
    SpriteButton.prototype.onInputDown = function () {
        if (this.EnableLongClick) {
            this.LongClickTimer = this.game.time.events.add(1000, this.StartLongClick, this);
        }
        if (this.SpriteButtonStyle != undefined) {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.DownFrame);
        }
        else {
            this.tint = 0x666666;
        }
        this.OnDown.dispatch(this);
    };
    SpriteButton.prototype.onInputUp = function () {
        if (this.SpriteButtonStyle != undefined) {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.UpFrame);
        }
        else {
            this.tint = 0xffffff;
        }
        this.game.time.events.remove(this.LongClickTimer);
        if (this.isLongClicking) {
            this.isLongClicking = false;
        }
        else {
            this.OnUp.dispatch(this);
        }
    };
    SpriteButton.prototype.StartLongClick = function () {
        this.game.time.events.remove(this.LongClickTimer);
        this.isLongClicking = true;
        this.LongClickTimer = this.game.time.events.loop(60, this.LongClick, this);
    };
    SpriteButton.prototype.LongClick = function () {
        this.OnLongClick.dispatch(this);
    };
    SpriteButton.prototype.SelectButton = function () {
        if (this.SpriteButtonStyle == undefined) {
            this.tint = 0xffffff;
        }
        else {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.DownFrame);
        }
        this.inputEnabled = false;
        this.input.useHandCursor = false;
    };
    SpriteButton.prototype.UnselectedButton = function () {
        if (this.SpriteButtonStyle == undefined) {
            this.tint = 0x949494;
        }
        else {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.DownFrame);
        }
        this.inputEnabled = true;
        this.input.useHandCursor = true;
    };
    SpriteButton.prototype.ResetButton = function () {
        this.game.time.events.remove(this.LongClickTimer);
        if (this.SpriteButtonStyle == undefined) {
            this.tint = 0xffffff;
        }
        else {
            this.loadTexture(this.SpriteButtonStyle.Key, this.SpriteButtonStyle.UpFrame);
        }
        this.inputEnabled = true;
        this.input.useHandCursor = true;
    };
    SpriteButton.prototype.ButtonEnable = function (value) {
        this.game.time.events.remove(this.LongClickTimer);
        if (value) {
            this.input.useHandCursor = this.inputEnabled = true;
            this.tint = 0xffffff;
        }
        else {
            this.ResetButton();
            this.inputEnabled = false;
            this.tint = 0x666666;
        }
    };
    return SpriteButton;
}(Phaser.Sprite));
