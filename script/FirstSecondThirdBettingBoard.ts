class FirstSecondThirdBettingBoard extends Phaser.Group{

    private tempBet: number;
    private tempBetText: Phaser.Text;
    private confirmBet: number;
    private confirmBetText: Phaser.BitmapText;
    private horseNumberIcon: Phaser.Sprite[];
    private horseNumber: number[];
    private rateText: Phaser.Text;

    constructor(game: Phaser.Game, group: Phaser.Group){
        super(game);
        group.add(this);
        this.initialize();
    }

    private initialize(){
        this.game.add.sprite(0, 0, 'Lobby', 'SingleBettingBG_2', this);
        this.game.add.text(15, 17, "冠軍", {font: "bold 32px Microsoft JhengHei", fill: "#ffffff"}, this);
        this.game.add.text(150, 17, "亞軍", {font: "bold 32px Microsoft JhengHei", fill: "#ffffff"}, this);
        this.game.add.text(285, 17, "季軍", {font: "bold 32px Microsoft JhengHei", fill: "#ffffff"}, this);

        this.rateText = this.game.add.text(575, 37, "-", {font: "bold 24px Microsoft JhengHei", fill: "#ffffff"}, this);
        this.rateText.anchor.set(1, 0);

        this.tempBet = 0;
        this.tempBetText = this.game.add.text(395, 37, "", {font: "24px Microsoft JhengHei", fill: "#00fc26"}, this);

        this.confirmBet = 0;
        this.confirmBetText = this.game.add.bitmapText(477, 21, 'BetNumber', "0", 30, this);
        this.confirmBetText.anchor.set(0.5);

        this.horseNumberIcon = new Array(3);
        for(let i: number = 0; i < 3; i++){
            this.horseNumberIcon[i] = this.game.add.sprite(90 + 135 * i, 14, 'Lobby', 'BettingNumber_1', this);
        }

        this.horseNumber = new Array(3);
    }

    public showTempBet(bet: number){
        this.tempBet += bet;
        this.tempBetText.setText("+" + this.tempBet);
    }

    public keepTempBet(bet: number){
        this.tempBet = bet;
        this.tempBetText.setText("+" + this.tempBet);
    }

    public clearTempBet(){
        this.tempBet = 0;
        this.tempBetText.setText("");
    }

    public updateConfirmBet(){
        this.confirmBet += this.tempBet;
        this.confirmBetText.setText(this.confirmBet + "");
    }

    public clearConfirmBet(){
        this.confirmBet = 0;
        this.confirmBetText.setText(this.confirmBet + "");
    }

    public updateHorseNumber(horseNumber: number[]){
        for(let i: number = 0; i < 3; i++){
            this.horseNumberIcon[i].loadTexture('Lobby', 'BettingNumber_' + (horseNumber[i] + 1));
            this.horseNumber[i] = horseNumber[i];
        }
    }

    public updateRate(rate: number){
        this.rateText.setText(rate + "");
    }

    public checkHorseNumberIsMatch(horseNumber: number[]){
        let isMatch: boolean = true;
        for(let i: number = 0; i < 3; i++){
            if(this.horseNumber[i] != horseNumber[i]){
                isMatch = false;
                break;
            }
        }
        return isMatch;
    }

    public getLastBet(){
        return this.tempBet;
    }

    public getLastHorseNumber(){
        return this.horseNumber;
    }

    public getConfirmBet(){
        return this.confirmBet;
    }

    public getRate(){
        return parseFloat(this.rateText.text);
    }
}