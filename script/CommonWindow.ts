class CommonWindow extends Phaser.Group{

    public switchGameSceneSignal: Phaser.Signal;
    public logOutSignal: Phaser.Signal;
    public requestDataSignal: Phaser.Signal;
    public lobbyGroup: Phaser.Group;
    public gameGroup: Phaser.Group;
    public passwordChangeGroup: Phaser.Group;
    public panelMask: Phaser.Graphics;
    private soundManager: SoundManager;
    private userBalance: number;
    private userBalanceText: Phaser.BitmapText;
    private userNameText: Phaser.Text;
    private userIcon: SpriteButton;
    private userMessageIcon: Phaser.Sprite;
    private userMessageNameText: Phaser.Text;
    private userMessageBalanceText: Phaser.BitmapText;
    private userTotalBetLobbyText: Phaser.BitmapText;
    private userTotalBetGameText: Phaser.BitmapText;
    private betRecordYearOptionBG: Phaser.Sprite[];
    private betRecordYearOptionButton: SpriteButton[];
    private betRecordYearOptionText: Phaser.Text[];
    private betRecordTotalBetText: Phaser.Text;
    private betRecordTotalWinText: Phaser.Text;
    private betRecordFormScrollGroup: Phaser.Group;
    private betRecordFormScrollTimerEvent: Phaser.TimerEvent;
    private betRecordFormScrollLimit: number;
    private historicalLotteryFormScrollGroup: Phaser.Group;
    private historicalLotteryFormScrollTimerEvent: Phaser.TimerEvent;
    private historicalLotteryFormScrollLimit: number;
    private historicalLotteryGroup: Phaser.Group;
    private currentSelectedStatPage: SpriteButton;
    private currentShowGroup: Phaser.Group;
    private missingNumberArrow: Phaser.Sprite[];
    private missingNumberRankingHorseNumber: Phaser.Sprite[];
    private missingNumberRankingData: number[][][];
    private missingNumberRankingDataText: Phaser.Text[][];
    private missingNumberSingleDoubleData: number[][][];
    private missingNumberSingleDoubleDataText: Phaser.Text[][];
    private missingNumberBigSmallData: number[][][];
    private missingNumberBigSmallDataText: Phaser.Text[][];
    private missingNumberDragonTigerData: number[][];
    private missingNumberDragonTigerDataText: Phaser.Text[][];
    private missingNumberFirstSecondSumSingleDoubleData: number[][];
    private missingNumberFirstSecondSumSingleDoubleDataText: Phaser.Text[][];
    private missingNumberFirstSecondSumBigSmallData: number[][];
    private missingNumberFirstSecondSumBigSmallDataText: Phaser.Text[][];
    private todayNumberData: number[][];
    private todayNumberDataText: Phaser.Text[][];
    private dailyDragonBigLongDragonData: DailyLongDragonData[][];
    private dailyDragonSmallLongDragonData: DailyLongDragonData[][];
    private dailyDragonSingleLongDragonData: DailyLongDragonData[][];
    private dailyDragonDoubleLongDragonData: DailyLongDragonData[][];
    private dailyDragonDragonLongDragonData: DailyLongDragonData[][];
    private dailyDragonTigerLongDragonData: DailyLongDragonData[][];
    private dailyDragonFormDateText: Phaser.Text[];
    private dailyDragonFormScrollGroup: Phaser.Group[][];
    private dailyDragonFormScrollTimerEvent: Phaser.TimerEvent;
    private dailyDragonFormScrollLimit: number[][];
    private twoSideFormScrollGroup: Phaser.Group;
    private twoSideFormData: BigSmallSingleDoubleData[][];
    private twoSideFormScrollTimerEvent: Phaser.TimerEvent;
    private twoSideFormScrollLimit: number;
    private singleDoubleFormScrollGroup: Phaser.Group;
    private singleDoubleFormData: BigSmallSingleDoubleData[][];
    private singleDoubleFormScrollTimerEvent: Phaser.TimerEvent;
    private singleDoubleFormScrollLimit: number;
    private firstSecondSumFormScrollGroup: Phaser.Group;
    private firstSecondSumFormScrollTimerEvent: Phaser.TimerEvent;
    private firstSecondSumFormScrollLimit: number;

    constructor(game: Phaser.Game, soundManager: SoundManager){
        super(game);
        this.soundManager = soundManager;
        this.initialize();
    }

    private initialize(){
        this.lobbyGroup = this.game.add.group(this);
        this.lobbyGroup.visible = false;
        this.gameGroup = this.game.add.group(this);
        this.switchGameSceneSignal = new Phaser.Signal();
        this.requestDataSignal = new Phaser.Signal();

        this.userBalance = 0;
        this.userBalanceText = this.game.add.bitmapText(330, 87, 'BetNumber', this.userBalance + "", 34, this);
        this.userBalanceText.anchor.set(0.5);
        this.userNameText = this.game.add.text(150, 15, "", {font: "30px Microsoft JhengHei", fill: "#ffffff"}, this);
        this.userTotalBetGameText = this.game.add.bitmapText(660, 1045, 'BetNumber', "", 34, this.gameGroup);
        this.userTotalBetGameText.anchor.set(0.5);
        this.userTotalBetLobbyText = this.game.add.bitmapText(147, 1045, 'BetNumber', "", 34, this.lobbyGroup);
        this.userTotalBetLobbyText.anchor.set(0.5);
        this.userIcon = new SpriteButton(this.game, 28, 15, 'Lobby', 'UserIcon');
        this.add(this.userIcon);
        
        let gameRuleButton: SpriteButton = new SpriteButton(this.game, 1480, 10, 'Button', 'HelpButton');
        this.add(gameRuleButton);

        let mailButton: SpriteButton = new SpriteButton(this.game, 1635, 10, 'Button', 'MailButton');
        this.add(mailButton);

        let gameSettingButton: SpriteButton = new SpriteButton(this.game, 1790, 10, 'Button', 'ListButton');
        this.add(gameSettingButton);

        // let historicalLotteryButton: SpriteButton = new SpriteButton(this.game, 1080, 983, 'Button', 'HistoricalLotteryButton');
        let historicalLotteryButton: SpriteButton = new SpriteButton(this.game, 1360, 983, 'Button', 'HistoricalLotteryButton');
        this.gameGroup.add(historicalLotteryButton);

        // let betRecordButton: SpriteButton = new SpriteButton(this.game, 1360, 983, 'Button', 'BetRecordButton');
        let betRecordButton: SpriteButton = new SpriteButton(this.game, 1640, 983, 'Button', 'BetRecordButton');
        this.gameGroup.add(betRecordButton);

        // let statisticalDataButton: SpriteButton = new SpriteButton(this.game, 1640, 983, 'Button', 'StatisticalDataButton');
        // this.gameGroup.add(statisticalDataButton);

        let openBetPage: SpriteButton = new SpriteButton(this.game, 40, 150, 'Button', 'BetPageButton');
        openBetPage.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        openBetPage.OnUp.add(()=>{
            this.switchGameSceneSignal.dispatch(false);
        }, this);
        this.gameGroup.add(openBetPage);

        let returnGame: SpriteButton = new SpriteButton(this.game, 1720, 975, 'Button', 'ReturnGame');
        returnGame.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        returnGame.OnUp.add(()=>{
            this.switchGameSceneSignal.dispatch(true);
        }, this);
        this.lobbyGroup.add(returnGame);

        this.panelMask = this.game.add.graphics(0, 0, this);
        this.panelMask.beginFill(0x000000, 0.5);
        this.panelMask.drawRect(0, 0, 1920, 1080);
        this.panelMask.endFill();
        this.panelMask.inputEnabled = true;
        this.panelMask.visible = false;

        let userMessageGroup: Phaser.Group = this.game.add.group(this);
        userMessageGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, userMessageGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'UserMessageTitle', userMessageGroup).anchor.set(0.5);
        let userMessageClose: SpriteButton = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        userMessageClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        userMessageClose.OnUp.add(()=>{
            userMessageGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        userMessageGroup.add(userMessageClose);

        this.userIcon.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.userIcon.OnUp.add(()=>{
            userMessageGroup.visible = true;
            this.panelMask.visible = true;
        }, this);
        this.game.add.sprite(960, 650, 'Lobby', 'UserMessageLine_2', userMessageGroup).anchor.set(0.5);
        this.game.add.sprite(670, 400, 'Lobby', 'UserMessageIconFrame', userMessageGroup);
        this.game.add.sprite(870, 515, 'Lobby', 'UserMessageCoinFrame', userMessageGroup);
        this.game.add.sprite(870, 515, 'Lobby', 'Coin_Icon', userMessageGroup);
        this.userMessageIcon = this.game.add.sprite(676, 410, 'Lobby', 'UserIcon', userMessageGroup);
        this.userMessageIcon.scale.set(1.6);
        this.userMessageNameText = this.game.add.text(940, 410, "", {font: "48px Microsoft JhengHei", fill: "#ffffff"}, userMessageGroup);
        this.userMessageBalanceText = this.game.add.bitmapText(1070, 547, 'BetNumber', this.userBalance + "", 34, userMessageGroup);
        this.userMessageBalanceText.anchor.set(0.5);
        this.logOutSignal = new Phaser.Signal;
        let userMessageSwitchUserButton: SpriteButton = new SpriteButton(this.game, 760, 760, 'Button', 'SwitchUser');
        userMessageSwitchUserButton.anchor.set(0.5);
        userMessageGroup.add(userMessageSwitchUserButton);
        userMessageSwitchUserButton.OnUp.add(()=>{
            userMessageGroup.visible = false;
            this.panelMask.visible = false;
            this.logOutSignal.dispatch();
        }, this);
        let passwordChangeButton: SpriteButton = new SpriteButton(this.game, 1160, 760, 'Button', 'PasswordChangeButton');
        passwordChangeButton.anchor.set(0.5);
        userMessageGroup.add(passwordChangeButton);
        passwordChangeButton.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        passwordChangeButton.OnUp.add(()=>{
            userMessageGroup.visible = false;
            this.passwordChangeGroup.visible = true;
        }, this);
        this.passwordChangeGroup = this.game.add.group(this);
        this.passwordChangeGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, this.passwordChangeGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'PasswordChangeTitle', this.passwordChangeGroup).anchor.set(0.5);
        let passwordChangePanelClose: SpriteButton = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        passwordChangePanelClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        passwordChangePanelClose.OnUp.add(()=>{
            this.passwordChangeGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        this.passwordChangeGroup.add(passwordChangePanelClose);
        this.game.add.text(820, 360, "請輸入舊密碼:", {font: "50px Microsoft JhengHei", fill: "#ffffff"}, this.passwordChangeGroup).anchor.set(1, 0);
        this.game.add.sprite(850, 345, 'Button', 'DropDownButton_2', this.passwordChangeGroup);
        let oldPasswordInput: PhaserInput.InputField = this.game.add.inputField(865, 355, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 480,
            cursorColor: '#ffffff',
            textAlign: 'left',
            max: "12",
            type: PhaserInput.InputType.password
        }, this.passwordChangeGroup);
        this.game.add.text(820, 490, "請輸入新密碼:", {font: "50px Microsoft JhengHei", fill: "#ffffff"}, this.passwordChangeGroup).anchor.set(1, 0);
        this.game.add.sprite(850, 475, 'Button', 'DropDownButton_2', this.passwordChangeGroup);
        let newPasswordInput: PhaserInput.InputField = this.game.add.inputField(865, 485, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 480,
            cursorColor: '#ffffff',
            textAlign: 'left',
            max: "12",
            type: PhaserInput.InputType.password
        }, this.passwordChangeGroup);
        this.game.add.text(820, 620, "確認新密碼:", {font: "50px Microsoft JhengHei", fill: "#ffffff"}, this.passwordChangeGroup).anchor.set(1, 0);
        this.game.add.sprite(850, 605, 'Button', 'DropDownButton_2', this.passwordChangeGroup);
        let newPasswordConfirmInput: PhaserInput.InputField = this.game.add.inputField(865, 615, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 480,
            cursorColor: '#ffffff',
            textAlign: 'left',
            max: "12",
            type: PhaserInput.InputType.password
        }, this.passwordChangeGroup);
        let confirmpasswordChangeButton: SpriteButton = new SpriteButton(this.game, 960, 780, 'Button', 'ConfirmBet');
        confirmpasswordChangeButton.anchor.set(0.5);
        this.passwordChangeGroup.add(confirmpasswordChangeButton);
        confirmpasswordChangeButton.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        confirmpasswordChangeButton.OnUp.add(()=>{
            if(oldPasswordInput.value == "" || newPasswordInput.value == "" || newPasswordConfirmInput.value == ""){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "密碼修改選項不能為空"}));
                return;
            }else if(newPasswordInput.value.length < 5 || newPasswordConfirmInput.value.length < 5){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "密碼長度不得小於5"}));
            }else if(newPasswordInput.value != newPasswordConfirmInput.value){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "兩次新密碼輸入不一樣，請重新輸入"}));
                newPasswordInput.resetText();
                newPasswordConfirmInput.resetText();
                return;
            }
            let postData: any = {
                strPwd: oldPasswordInput.value,
                strPwdN: newPasswordInput.value
            };
            this.requestDataSignal.dispatch(DATA_URL.PASSWORD_CHANGE, JSON.stringify(postData));
            oldPasswordInput.resetText();
            newPasswordInput.resetText();
            newPasswordConfirmInput.resetText();
        }, this);

        let gameRuleGroup: Phaser.Group = this.game.add.group(this);
        gameRuleGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_1', null, gameRuleGroup).anchor.set(0.5);
        this.game.add.sprite(960, 90, 'Lobby', 'GameRuleTitle', gameRuleGroup).anchor.set(0.5);
        let gameRuleClose: SpriteButton = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        gameRuleClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameRuleClose.OnUp.add(()=>{
            gameRuleGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        gameRuleGroup.add(gameRuleClose);

        gameRuleButton.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameRuleButton.OnUp.add(()=>{
            gameRuleGroup.visible = true;
            this.panelMask.visible = true;
        }, this);

        let mailGroup: Phaser.Group = this.game.add.group(this);
        mailGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, mailGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'mailTitle', mailGroup).anchor.set(0.5);
        this.game.add.text(960, 400, "Line ID", {font:"bold 60px Arial", fill: "#ffffff"}, mailGroup).anchor.set(0.5);
        let mailClose: SpriteButton = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        mailClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        mailClose.OnUp.add(()=>{
            mailGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        mailGroup.add(mailClose);

        mailButton.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        mailButton.OnUp.add(()=>{
            mailGroup.visible = true;
            this.panelMask.visible = true;
        }, this);

        let gameSettingGroup: Phaser.Group = this.game.add.group(this);
        gameSettingGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_2', null, gameSettingGroup).anchor.set(0.5);
        this.game.add.sprite(960, 243, 'Lobby', 'GameSettingTitle', gameSettingGroup).anchor.set(0.5);
        let gameSettingClose: SpriteButton = new SpriteButton(this.game, 1393, 203, 'Button', 'PanelClose');
        gameSettingClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameSettingClose.OnUp.add(()=>{
            gameSettingGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        gameSettingGroup.add(gameSettingClose);
        gameSettingButton.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        gameSettingButton.OnUp.add(()=>{
            gameSettingGroup.visible = true;
            this.panelMask.visible = true;
        }, this);

        this.game.add.text(550, 410, "音樂", {font: "38px Microsoft JhengHei", fill: "#ffffff"}, gameSettingGroup);
        this.game.add.sprite(700, 412, 'Lobby', 'GameSettingBar_Empty', gameSettingGroup);
        let musicFullBG: Phaser.Sprite = this.game.add.sprite(700, 412, 'Lobby', 'GameSettingBar_Full', gameSettingGroup);
        let musicMask: Phaser.Graphics = this.game.add.graphics(700, 400, gameSettingGroup);
        musicMask.beginFill(0xffffff, 0);
        musicMask.drawRect(0, 0, 634, 70);
        musicMask.endFill();
        musicFullBG.mask = musicMask;
        let musicPoint: Phaser.Sprite = this.game.add.sprite(1315, 433, 'Lobby', 'GameSettingPoint', gameSettingGroup);
        musicPoint.anchor.set(0.5);
        let musicTimerEvent: Phaser.TimerEvent;
        let musicTouch: Phaser.Graphics = this.game.add.graphics(700, 412, gameSettingGroup);
        musicTouch.beginFill(0x000000, 0);
        musicTouch.drawRect(0, 0, 634, 45);
        musicTouch.endFill();
        musicTouch.inputEnabled = true;
        musicTouch.events.onInputDown.add(()=>{
            musicTimerEvent = this.game.time.events.loop(20, ()=>{
                let inputX: number = this.game.input.x;
                if(inputX > 1315){
                    inputX = 1315;
                }
                if(inputX < 715){
                    inputX = 715;
                }
                let newVolume: number = (inputX - 715) / 600;
                musicMask.scale.set(newVolume, 1);
                musicPoint.position.set(inputX, 433);
                this.soundManager.changeSoundVolume(newVolume);
            }, this);
        }, this);
        musicTouch.events.onInputUp.add(()=>{
            this.game.time.events.remove(musicTimerEvent);
        }, this);

        this.game.add.text(550, 550, "音效", {font: "38px Microsoft JhengHei", fill: "#ffffff"}, gameSettingGroup);
        this.game.add.sprite(700, 552, 'Lobby', 'GameSettingBar_Empty', gameSettingGroup);
        let soundEffectFullBG: Phaser.Sprite = this.game.add.sprite(700, 552, 'Lobby', 'GameSettingBar_Full', gameSettingGroup);
        let soundEffectMask: Phaser.Graphics = this.game.add.graphics(700, 552, gameSettingGroup);
        soundEffectMask.beginFill(0xffffff, 0);
        soundEffectMask.drawRect(0, 0, 634, 45);
        soundEffectMask.endFill();
        soundEffectFullBG.mask = soundEffectMask;
        let soundEffectPoint: Phaser.Sprite = this.game.add.sprite(1315, 573, 'Lobby', 'GameSettingPoint', gameSettingGroup);
        soundEffectPoint.anchor.set(0.5);
        let soundEffectTimerEvent: Phaser.TimerEvent;
        let soundEffectTouch: Phaser.Graphics = this.game.add.graphics(700, 540, gameSettingGroup);
        soundEffectTouch.beginFill(0x000000, 0);
        soundEffectTouch.drawRect(0, 0, 634, 70);
        soundEffectTouch.endFill();
        soundEffectTouch.inputEnabled = true;
        soundEffectTouch.events.onInputDown.add(()=>{
            soundEffectTimerEvent = this.game.time.events.loop(20, ()=>{
                let inputX: number = this.game.input.x;
                if(inputX > 1315){
                    inputX = 1315;
                }
                if(inputX < 715){
                    inputX = 715;
                }
                let newVolume: number = (inputX - 715) / 600;
                soundEffectMask.scale.set(newVolume, 1);
                soundEffectPoint.position.set(inputX, 573);
                this.soundManager.changeSoundEffectVolume(newVolume);
            }, this);
        }, this);
        soundEffectTouch.events.onInputUp.add(()=>{
            this.game.time.events.remove(soundEffectTimerEvent);
        }, this);
        
        let specialEffectSwitch: boolean = true;
        this.game.add.text(550, 690, "特效", {font: "38px Microsoft JhengHei", fill: "#ffffff"}, gameSettingGroup);
        let specialEffectOption: Phaser.Sprite = this.game.add.sprite(700, 677, 'Lobby', 'GameSettingOption_On', gameSettingGroup);
        let specialEffectPoint: Phaser.Sprite = this.game.add.sprite(830, 665, 'Lobby', 'GameSettingPoint', gameSettingGroup);
        let specialEffectText: Phaser.Text = this.game.add.text(755, 692, "ON", {font: "bold 38px Microsoft JhengHei", fill: "#ffffff"}, gameSettingGroup);
        let specialEffectTouch: Phaser.Graphics = this.game.add.graphics(700, 677, gameSettingGroup);
        specialEffectTouch.beginFill(0xffffff, 0);
        specialEffectTouch.drawRect(0, 0, 202, 77);
        specialEffectTouch.endFill();
        specialEffectTouch.inputEnabled = true;
        specialEffectTouch.events.onInputUp.add(()=>{
            specialEffectSwitch = !specialEffectSwitch;
            specialEffectOption.loadTexture('Lobby', specialEffectSwitch ? 'GameSettingOption_On' : 'GameSettingOption_Off');
            specialEffectPoint.position.set(specialEffectSwitch ? 830 : 670 , 665);
            specialEffectText.position.set(specialEffectSwitch ? 755 : 785 , 692);
            specialEffectText.setText(specialEffectSwitch ? "ON" : "OFF");
        }, this);

        this.historicalLotteryGroup = this.game.add.group(this);
        this.historicalLotteryGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_1', null, this.historicalLotteryGroup).anchor.set(0.5);
        this.game.add.sprite(960, 90, 'Lobby', 'HistoricalLotteryTitle', this.historicalLotteryGroup).anchor.set(0.5);
        let historicalLotteryClose: SpriteButton = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        historicalLotteryClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        historicalLotteryClose.OnUp.add(()=>{
            this.historicalLotteryGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        this.historicalLotteryGroup.add(historicalLotteryClose);

        historicalLotteryButton.OnDown.add(()=>{
            let date: Date = new Date();
            let month: number = date.getMonth() + 1;
            let monthString: string = month < 10 ? "0" + month : "" + month;
            let day: number = date.getDate();
            let dayString: string = day < 10 ? "0" + day : "" + day;
            let postData: any = {
                dateOpen: date.getFullYear() + "-" + monthString + "-" + dayString
            };
            this.requestDataSignal.dispatch(DATA_URL.OPEN_HISTORY, JSON.stringify(postData));
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);

        this.game.add.sprite(960, 220, 'Lobby', 'HistoricalLotteryFrame_Top', this.historicalLotteryGroup).anchor.set(0.5);
        this.game.add.text(330, 195, "時間", {font: "42px Microsoft JhengHei", fill: "#f2e200"}, this.historicalLotteryGroup);
        this.game.add.text(820, 195, "開獎號碼", {font: "42px Microsoft JhengHei", fill: "#f2e200"}, this.historicalLotteryGroup);
        this.game.add.text(1235, 195, "冠亞軍和", {font: "42px Microsoft JhengHei", fill: "#f2e200"}, this.historicalLotteryGroup);
        this.game.add.text(1600, 195, "龍虎", {font: "42px Microsoft JhengHei", fill: "#f2e200"}, this.historicalLotteryGroup);
        this.historicalLotteryFormScrollGroup = this.game.add.group(this.historicalLotteryGroup);
        let historicalLotteryScrollMask: Phaser.Graphics = this.game.add.graphics(86, 259, this.historicalLotteryGroup);
        historicalLotteryScrollMask.beginFill(0xffffff, 0);
        historicalLotteryScrollMask.drawRect(0, 0, 1748, 742);
        historicalLotteryScrollMask.endFill();
        historicalLotteryScrollMask.inputEnabled = true;
        historicalLotteryScrollMask.events.onInputDown.add(this.startHistoricalLotteryFormScroll, this);
        historicalLotteryScrollMask.events.onInputUp.add(this.stopHistoricalLotteryFormScroll, this);
        historicalLotteryScrollMask.events.onInputOut.add(this.stopHistoricalLotteryFormScroll, this);
        this.historicalLotteryFormScrollGroup.mask = historicalLotteryScrollMask;
        
        let betRecordGroup: Phaser.Group = this.game.add.group(this);
        betRecordGroup.visible = false;
        this.game.add.image(960, 540, 'PanelBG_1', null, betRecordGroup).anchor.set(0.5);
        this.game.add.sprite(960, 90, 'Lobby', 'BettingRecordTitle', betRecordGroup).anchor.set(0.5);
        this.betRecordFormScrollGroup = this.game.add.group(betRecordGroup);
        let betRecordFormGroup: Phaser.Group = this.game.add.group(betRecordGroup);
        let betRecordYearOptionGroup: Phaser.Group = this.game.add.group(betRecordGroup);
        betRecordYearOptionGroup.visible = false;
        let betRecordMonthOptionGroup: Phaser.Group = this.game.add.group(betRecordGroup);
        betRecordMonthOptionGroup.visible = false;
        let betRecordDayOptionGroup: Phaser.Group = this.game.add.group(betRecordGroup);
        betRecordDayOptionGroup.visible = false;
        let betRecordClose: SpriteButton = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        betRecordClose.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordClose.OnUp.add(()=>{
            betRecordYearOptionGroup.visible = false;
            betRecordYearArrow.angle = 0;
            betRecordMonthOptionGroup.visible = false;
            betRecordMonthArrow.angle = 0;
            betRecordDayOptionGroup.visible = false;
            betRecordDayArrow.angle = 0;
            betRecordGroup.visible = false;
            this.panelMask.visible = false;
        }, this);
        betRecordGroup.add(betRecordClose);

        betRecordButton.OnDown.add(()=>{
            //填入預設值，開啟投注紀錄時預設可查詢今年及去年的資料，並預設查詢今日資料
            this.updateBetRecordYearOption([date.getFullYear() - 1, date.getFullYear()]);
            let postData: any = {
                strDate: betRecordCurrentYear.text + "-" + betRecordCurrentMonth.text + "-" + betRecordCurrentDay.text
            };
            this.requestDataSignal.dispatch(DATA_URL.DAY_BET_RECORD, JSON.stringify(postData));
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordButton.OnUp.add(()=>{
            betRecordGroup.visible = true;
            this.panelMask.visible = true;
        }, this);

        let date: Date = new Date();
        let betRecordYearArrow: Phaser.Sprite = this.game.add.sprite(470, 225, 'Button', 'DropDownArrow', betRecordGroup);
        betRecordYearArrow.anchor.set(0.5);
        let betRecordYearButton: SpriteButton = new SpriteButton(this.game, 265, 188, 'Button', 'DropDownButton_1');
        betRecordYearButton.OnDown.add(()=>{
            betRecordMonthOptionGroup.visible = false;
            betRecordDayOptionGroup.visible = false;
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordYearButton.OnUp.add(()=>{
            betRecordYearOptionGroup.visible = !betRecordYearOptionGroup.visible;
            betRecordYearArrow.angle = betRecordYearOptionGroup.visible ? 180 : 0;
        }, this);
        betRecordGroup.add(betRecordYearButton);
        this.game.add.text(87, 202, "西元年", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        this.game.add.text(220, 199, ":", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        let betRecordCurrentYear: Phaser.Text = this.game.add.text(320, 202, date.getFullYear() + "", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        this.betRecordYearOptionBG = new Array(10);
        this.betRecordYearOptionButton = new Array(10);
        this.betRecordYearOptionText = new Array(10);
        for(let i: number = 0;i < 10;i++){
            this.betRecordYearOptionBG[i] = this.game.add.sprite(265, 271 + 66 * i, 'Button', 'DropDownBG_1', betRecordYearOptionGroup);
            this.betRecordYearOptionBG[i].visible = false;
            this.betRecordYearOptionButton[i] = new SpriteButton(this.game, 265, 271 + 66 * i, 'Button', 'DropDownBG_1', {
                Key: 'Button',
                OverFrame: 'DropDownSelected_1',
                OutFrame: 'DropDownBG_1',
                UpFrame: 'DropDownBG_1',
                DownFrame: 'DropDownSelected_1'
            });
            this.betRecordYearOptionButton[i].OnDown.add(()=>{
                betRecordCurrentMonth.setText("");
                betRecordCurrentDay.setText("");
                this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this);
            this.betRecordYearOptionButton[i].OnUp.add(()=>{
                betRecordYearOptionGroup.visible = false;
                betRecordYearArrow.angle = 0;
                betRecordCurrentYear.setText(this.betRecordYearOptionText[i].text);
            }, this);
            this.betRecordYearOptionButton[i].visible = false;
            betRecordYearOptionGroup.add(this.betRecordYearOptionButton[i]);

            this.betRecordYearOptionText[i] = this.game.add.text(385, 307 + 66 * i, "", {font: "38px Microsoft JhengHei", fill: "#ffffff"}, betRecordYearOptionGroup);
            this.betRecordYearOptionText[i].anchor.set(0.5);
            this.betRecordYearOptionText[i].visible = false;
        }

        let betRecordMonthArrow: Phaser.Sprite = this.game.add.sprite(870, 225, 'Button', 'DropDownArrow', betRecordGroup);
        betRecordMonthArrow.anchor.set(0.5);
        let betRecordMonthButton: SpriteButton = new SpriteButton(this.game, 665, 188, 'Button', 'DropDownButton_1');
        betRecordMonthButton.OnDown.add(()=>{
            betRecordYearOptionGroup.visible = false;
            betRecordDayOptionGroup.visible = false;
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordMonthButton.OnUp.add(()=>{
            betRecordMonthOptionGroup.visible = !betRecordMonthOptionGroup.visible;
            betRecordMonthArrow.angle = betRecordMonthOptionGroup.visible ? 180 : 0;
        }, this);
        betRecordGroup.add(betRecordMonthButton);
        this.game.add.text(570, 202, "月", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        this.game.add.text(620, 199, ":", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        let betRecordCurrentMonth: Phaser.Text = this.game.add.text(750, 202, ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1), {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        let betRecordMonthOptionBG: Phaser.Sprite[] = new Array(12);
        let betRecordMonthOptionButton: SpriteButton[] = new Array(12);
        let betRecordMonthOptionText: Phaser.Text[] = new Array(12);
        for(let i: number = 0;i < 12;i++){
            betRecordMonthOptionBG[i] = this.game.add.sprite(665, 271 + 66 * i, 'Button', 'DropDownBG_1', betRecordMonthOptionGroup);
            betRecordMonthOptionButton[i] = new SpriteButton(this.game, 665, 271 + 66 * i, 'Button', 'DropDownBG_1', {
                Key: 'Button',
                OverFrame: 'DropDownSelected_1',
                OutFrame: 'DropDownBG_1',
                UpFrame: 'DropDownBG_1',
                DownFrame: 'DropDownSelected_1'
            });
            betRecordMonthOptionButton[i].OnDown.add(()=>{
                betRecordCurrentDay.setText("");
                this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this);
            betRecordMonthOptionButton[i].OnUp.add(()=>{
                betRecordMonthOptionGroup.visible = false;
                betRecordMonthArrow.angle = 0;
                if(betRecordCurrentYear.text == ""){
                    window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇西元年"}));
                    return;
                }
                let dayLength: number = new Date(parseInt(betRecordCurrentYear.text), i + 1, 0).getDate();
                for(let day: number = 0;day < 31;day++){
                    betRecordDayOptionBG[day].visible = day < dayLength ? true : false;
                    betRecordDayOptionButton[day].visible = day < dayLength ? true : false;
                    betRecordDayOptionText[day].visible = day < dayLength ? true : false;
                }
                betRecordCurrentMonth.setText(betRecordMonthOptionText[i].text);
            }, this);
            betRecordMonthOptionGroup.add(betRecordMonthOptionButton[i]);

            betRecordMonthOptionText[i] = this.game.add.text(785, 307 + 66 * i, (i < 9 ? "0" : "") + (i + 1), {font: "38px Microsoft JhengHei", fill: "#ffffff"}, betRecordMonthOptionGroup);
            betRecordMonthOptionText[i].anchor.set(0.5);
        }

        let betRecordDayArrow: Phaser.Sprite = this.game.add.sprite(1260, 225, 'Button', 'DropDownArrow', betRecordGroup);
        betRecordDayArrow.anchor.set(0.5);
        let betRecordDayButton: SpriteButton = new SpriteButton(this.game, 1055, 188, 'Button', 'DropDownButton_1');
        betRecordDayButton.OnDown.add(()=>{
            betRecordYearOptionGroup.visible = false;
            betRecordMonthOptionGroup.visible = false;
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        betRecordDayButton.OnUp.add(()=>{
            betRecordDayOptionGroup.visible = !betRecordDayOptionGroup.visible;
            betRecordDayArrow.angle = betRecordDayOptionGroup.visible ? 180 : 0;
        }, this);
        betRecordGroup.add(betRecordDayButton);
        this.game.add.text(960, 202, "日", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        this.game.add.text(1010, 199, ":", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        let betRecordCurrentDay: Phaser.Text = this.game.add.text(1140, 202, (date.getDate() < 10 ? "0" : "") + date.getDate(), {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordGroup);
        let betRecordDayOptionBG: Phaser.Sprite[] = new Array(31);
        let betRecordDayOptionButton: SpriteButton[] = new Array(31);
        let betRecordDayOptionText: Phaser.Text[] = new Array(31);
        for(let i: number = 0;i < 31;i++){
            betRecordDayOptionBG[i] = this.game.add.sprite(1055 + 238 * (Math.floor(i / 11)), 271 + 66 * (i % 11), 'Button', 'DropDownBG_1', betRecordDayOptionGroup);
            betRecordDayOptionButton[i] = new SpriteButton(this.game, 1055 + 238 * (Math.floor(i / 11)), 271 + 66 * (i % 11), 'Button', 'DropDownBG_1', {
                Key: 'Button',
                OverFrame: 'DropDownSelected_1',
                OutFrame: 'DropDownBG_1',
                UpFrame: 'DropDownBG_1',
                DownFrame: 'DropDownSelected_1'
            });
            betRecordDayOptionButton[i].OnDown.add(()=>{
                this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this);
            betRecordDayOptionButton[i].OnUp.add(()=>{
                betRecordDayOptionGroup.visible = false;
                betRecordDayArrow.angle = 0;
                betRecordCurrentDay.setText(betRecordDayOptionText[i].text);
            }, this);
            betRecordDayOptionGroup.add(betRecordDayOptionButton[i]);

            betRecordDayOptionText[i] = this.game.add.text(1175 + 238 * (Math.floor(i / 11)), 307 + 66 * (i % 11), (i < 9 ? "0" : "") + (i + 1), {font: "38px Microsoft JhengHei", fill: "#ffffff"}, betRecordDayOptionGroup);
            betRecordDayOptionText[i].anchor.set(0.5);
        }

        let betRecordSearchButton: Phaser.Sprite = this.game.add.sprite(1350, 175, 'Button', 'ConfirmBet', betRecordGroup);
        betRecordSearchButton.inputEnabled = true;
        betRecordSearchButton.events.onInputDown.add(()=>{
            if(betRecordCurrentYear.text == "" || betRecordCurrentMonth.text == "" || betRecordCurrentDay.text == ""){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇查詢的年／月／日"}));
                return;
            }

            //帶入查詢的日期資料
            let postData: any = {
                strDate: betRecordCurrentYear.text + "-" + betRecordCurrentMonth.text + "-" + betRecordCurrentDay.text
            };
            this.requestDataSignal.dispatch(DATA_URL.DAY_BET_RECORD, JSON.stringify(postData));

            betRecordSearchButton.tint = 0x666666;
        }, this);
        betRecordSearchButton.events.onInputUp.add(()=>{
            betRecordSearchButton.tint = 0xffffff;
        }, this);

        let betRecordTitleString: string[] = ["玩法", "組合", "賠率", "投注金額", "派彩金額"];
        this.game.add.sprite(960, 340, 'Lobby', 'BettingRecordFrame_Top', betRecordFormGroup).anchor.set(0.5);
        this.game.add.sprite(960, 972, 'Lobby', 'BettingRecordFrame_Bottom', betRecordFormGroup).anchor.set(0.5);
        for(let i: number = 0;i < 5;i++){
            this.game.add.text(260 + 348 * i, 345, betRecordTitleString[i], {font: "42px Microsoft JhengHei", fill: "#f2e200"}, betRecordFormGroup).anchor.set(0.5);
        }
        this.game.add.text(260, 975, "合計", {font: "42px Microsoft JhengHei", fill: "#f2e200"}, betRecordFormGroup).anchor.set(0.5);
        this.betRecordTotalBetText = this.game.add.text(1304, 975, "", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordFormGroup);
        this.betRecordTotalBetText.anchor.set(0.5);
        this.betRecordTotalWinText = this.game.add.text(1652, 975, "", {font: "42px Microsoft JhengHei", fill: "#ffffff"}, betRecordFormGroup);
        this.betRecordTotalWinText.anchor.set(0.5);
        let betRecordScrollMask: Phaser.Graphics = this.game.add.graphics(86, 377, betRecordFormGroup);
        betRecordScrollMask.beginFill(0xffffff, 0);
        betRecordScrollMask.drawRect(0, 0, 1747, 558);
        betRecordScrollMask.endFill();
        betRecordScrollMask.inputEnabled = true;
        betRecordScrollMask.events.onInputDown.add(this.startBetRecordFormScroll, this);
        betRecordScrollMask.events.onInputUp.add(this.stopBetRecordFormScroll, this);
        betRecordScrollMask.events.onInputOut.add(this.stopBetRecordFormScroll, this);
        this.betRecordFormScrollGroup.mask = betRecordScrollMask;

        // let statisticalDataGroup: Phaser.Group = this.game.add.group(this);
        // statisticalDataGroup.visible = false;
        // this.game.add.image(960, 540, 'PanelBG_1', null, statisticalDataGroup).anchor.set(0.5);
        // this.game.add.sprite(960, 270, 'Lobby', 'UserMessageLine_1', statisticalDataGroup).anchor.set(0.5);
        // this.game.add.sprite(960, 90, 'Lobby', 'StatisticsDataTitle', statisticalDataGroup).anchor.set(0.5);
        // let statisticalDataClose: SpriteButton = new SpriteButton(this.game, 1773, 48, 'Button', 'PanelClose');
        // statisticalDataClose.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // statisticalDataClose.OnUp.add(()=>{
        //     missingNumberRankingOptionGroup.visible = false;
        //     missingNumberRankingArrow.angle = 0;
        //     missingNumberSortOptionGroup.visible = false;
        //     missingNumberSortArrow.angle = 0;
        //     statisticalDataGroup.visible = false;
        //     this.panelMask.visible = false;
        // }, this);
        // statisticalDataGroup.add(statisticalDataClose);

        // statisticalDataButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // statisticalDataButton.OnUp.add(()=>{
        //     if(this.missingNumberRankingData == undefined || this.missingNumberRankingData.length == 0){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "無資料"}));
        //         return;
        //     }
        //     statisticalDataGroup.visible = true;
        //     this.panelMask.visible = true;
        // }, this);

        // let selectMissingNumber: SpriteButton = new SpriteButton(this.game, 85, 175, 'Button', 'StatDataButton_Up_1', {
        //     Key: 'Button',
        //     OverFrame: 'StatDataButton_Up_1',
        //     OutFrame: 'StatDataButton_Up_1',
        //     UpFrame: 'StatDataButton_Up_1',
        //     DownFrame: 'StatDataButton_Down_1'
        // });
        // statisticalDataGroup.add(selectMissingNumber);

        // let selectTodayNumber: SpriteButton = new SpriteButton(this.game, 381, 175, 'Button', 'StatDataButton_Up_2', {
        //     Key: 'Button',
        //     OverFrame: 'StatDataButton_Up_2',
        //     OutFrame: 'StatDataButton_Up_2',
        //     UpFrame: 'StatDataButton_Up_2',
        //     DownFrame: 'StatDataButton_Down_2'
        // });
        // statisticalDataGroup.add(selectTodayNumber);

        // let selectDailyDragon: SpriteButton = new SpriteButton(this.game, 677, 175, 'Button', 'StatDataButton_Up_3', {
        //     Key: 'Button',
        //     OverFrame: 'StatDataButton_Up_3',
        //     OutFrame: 'StatDataButton_Up_3',
        //     UpFrame: 'StatDataButton_Up_3',
        //     DownFrame: 'StatDataButton_Down_3'
        // });
        // statisticalDataGroup.add(selectDailyDragon);

        // let selectTwoSide: SpriteButton = new SpriteButton(this.game, 973, 175, 'Button', 'StatDataButton_Up_4', {
        //     Key: 'Button',
        //     OverFrame: 'StatDataButton_Up_4',
        //     OutFrame: 'StatDataButton_Up_4',
        //     UpFrame: 'StatDataButton_Up_4',
        //     DownFrame: 'StatDataButton_Down_4'
        // });
        // statisticalDataGroup.add(selectTwoSide);

        // let selectSingleDouble: SpriteButton = new SpriteButton(this.game, 1269, 175, 'Button', 'StatDataButton_Up_5', {
        //     Key: 'Button',
        //     OverFrame: 'StatDataButton_Up_5',
        //     OutFrame: 'StatDataButton_Up_5',
        //     UpFrame: 'StatDataButton_Up_5',
        //     DownFrame: 'StatDataButton_Down_5'
        // });
        // statisticalDataGroup.add(selectSingleDouble);

        // let selectFirstSecondSum: SpriteButton = new SpriteButton(this.game, 1565, 175, 'Button', 'StatDataButton_Up_6', {
        //     Key: 'Button',
        //     OverFrame: 'StatDataButton_Up_6',
        //     OutFrame: 'StatDataButton_Up_6',
        //     UpFrame: 'StatDataButton_Up_6',
        //     DownFrame: 'StatDataButton_Down_6'
        // });
        // statisticalDataGroup.add(selectFirstSecondSum);

        // let missingNumberGroup: Phaser.Group = this.game.add.group(statisticalDataGroup);
        // let missingNumberFormTitleGroup: Phaser.Group = this.game.add.group(missingNumberGroup);
        // let missingNumberFormGroup: Phaser.Group[] = new Array(6);
        // for(let i: number = 0;i < 6;i++){
        //     missingNumberFormGroup[i] = this.game.add.group(missingNumberGroup);
        //     missingNumberFormGroup[i].visible = i == MISSING_NUMBER_PAGE.RANKING ? true : false;
        // }
        // let missingNumberRankingOptionGroup: Phaser.Group = this.game.add.group(missingNumberGroup);
        // missingNumberRankingOptionGroup.visible = false;
        // let missingNumberSortOptionGroup: Phaser.Group = this.game.add.group(missingNumberGroup);
        // missingNumberSortOptionGroup.visible = false;
        
        // let todayNumberGroup: Phaser.Group = this.game.add.group(statisticalDataGroup);
        // todayNumberGroup.visible = false;
        // let todayNumberSecondOptionGroup: Phaser.Group = this.game.add.group(todayNumberGroup);
        // todayNumberSecondOptionGroup.visible = false;
        // let todayNumberThirdOptionGroup: Phaser.Group = this.game.add.group(todayNumberGroup);
        // todayNumberThirdOptionGroup.visible = false;
        // let todayNumberFormScrollGroup: Phaser.Group = this.game.add.group(todayNumberGroup);
        
        // let dailyDragonGroup: Phaser.Group = this.game.add.group(statisticalDataGroup);
        // dailyDragonGroup.visible = false;
        // let dailyDragonFormLeftGroup: Phaser.Group = this.game.add.group(dailyDragonGroup);
        // this.dailyDragonFormScrollGroup = new Array(6);
        // for(let sortIndex: number = 0;sortIndex < 6;sortIndex++){
        //     let rankLength: number = sortIndex < 4 ? 8 : 4;
        //     this.dailyDragonFormScrollGroup[sortIndex] = new Array(rankLength);
        //     for(let rankIndex: number = 0;rankIndex < rankLength;rankIndex++){
        //         this.dailyDragonFormScrollGroup[sortIndex][rankIndex] = this.game.add.group(dailyDragonGroup);
        //         this.dailyDragonFormScrollGroup[sortIndex][rankIndex].visible = false;
        //     }
        // }
        // let dailyDragonSortOptionGroup: Phaser.Group = this.game.add.group(dailyDragonGroup);
        // dailyDragonSortOptionGroup.visible = false;
        // let dailyDragonRankingOptionGroup: Phaser.Group = this.game.add.group(dailyDragonGroup);
        // dailyDragonRankingOptionGroup.visible = false;

        // let twoSideGroup: Phaser.Group = this.game.add.group(statisticalDataGroup);
        // twoSideGroup.visible = false;
        // let twoSideFormTitleGroup: Phaser.Group = this.game.add.group(twoSideGroup);
        // this.twoSideFormScrollGroup = this.game.add.group(twoSideGroup);
        // let twoSideRankingOptionGroup: Phaser.Group = this.game.add.group(twoSideGroup);
        // twoSideRankingOptionGroup.visible = false;

        // let singleDoubleGroup: Phaser.Group = this.game.add.group(statisticalDataGroup);
        // singleDoubleGroup.visible = false;
        // let singleDoubleFormTitleGroup: Phaser.Group = this.game.add.group(singleDoubleGroup);
        // this.singleDoubleFormScrollGroup = this.game.add.group(singleDoubleGroup);
        // let singleDoubleRankingOptionGroup: Phaser.Group = this.game.add.group(singleDoubleGroup);
        // singleDoubleRankingOptionGroup.visible = false;

        // let firstSecondSumGroup: Phaser.Group = this.game.add.group(statisticalDataGroup);
        // firstSecondSumGroup.visible = false;
        // this.firstSecondSumFormScrollGroup = this.game.add.group(firstSecondSumGroup);

        // let rankingString: string[] = ["冠軍", "亞軍", "季軍", "第四名", "第五名", "第六名", "第七名", "第八名"];
        // let missingNumberRankingArrow: Phaser.Sprite = this.game.add.sprite(287, 330, 'Button', 'DropDownArrow', missingNumberGroup);
        // missingNumberRankingArrow.anchor.set(0.5);
        // let missingNumberRankingDropDownButton: SpriteButton = new SpriteButton(this.game, 87, 290, 'Button', 'DropDownButton_1');
        // missingNumberRankingDropDownButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // })
        // missingNumberRankingDropDownButton.OnUp.add(()=>{
        //     missingNumberSortOptionGroup.visible = false;
        //     missingNumberSortArrow.angle = 0;
        //     missingNumberRankingOptionGroup.visible = !missingNumberRankingOptionGroup.visible;
        //     missingNumberRankingArrow.angle = missingNumberRankingOptionGroup.visible ? 180 : 0;
        // }, this);
        // missingNumberGroup.add(missingNumberRankingDropDownButton);
        // let missingNumberCurrentRanikingOption: Phaser.Text = this.game.add.text(192, 335, rankingString[0], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, missingNumberGroup);
        // missingNumberCurrentRanikingOption.anchor.set(0.5);
        // let missingNumberRankingOptionButton: SpriteButton[] = new Array(8);
        // let missingNumberCurrentRankingIndex: number = RANKING.FIRST;
        // for(let i: number = 0;i < 8;i++){
        //     this.game.add.sprite(85, 371 + 66 * i, 'Button', 'DropDownBG_1', missingNumberRankingOptionGroup);
        //     missingNumberRankingOptionButton[i] = new SpriteButton(this.game, 87, 371 + 66 * i, 'Button', 'DropDownBG_1', {
        //         Key: 'Button',
        //         OverFrame: 'DropDownSelected_1',
        //         OutFrame: 'DropDownBG_1',
        //         UpFrame: 'DropDownBG_1',
        //         DownFrame: 'DropDownSelected_1'
        //     });
        //     missingNumberRankingOptionButton[i].OnDown.add(()=>{
        //         this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        //     }, this);
        //     missingNumberRankingOptionButton[i].OnUp.add(()=>{
        //         missingNumberRankingOptionGroup.visible = false;
        //         missingNumberRankingArrow.angle = 0;
        //         missingNumberCurrentRanikingOption.setText(rankingString[i]);
        //         missingNumberCurrentRankingIndex = i;
        //         for(let valueIndex: number = 0;valueIndex < 7;valueIndex++){
        //             this.missingNumberArrow[valueIndex].events.onInputUp.removeAll();
        //             this.missingNumberArrow[valueIndex].events.onInputUp.add(()=>{
        //                 switch(missingNumberCurrentSortIndex){
        //                     case MISSING_NUMBER_PAGE.RANKING:
        //                         this.changeMissingNumberRankingSort(missingNumberCurrentRankingIndex, valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.SINGLE_DOUBLE:
        //                         this.changeMissingNumberSingleDoubleSort(missingNumberCurrentRankingIndex, valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.BIG_SMALL:
        //                         this.changeMissingNumberBigSmallSort(missingNumberCurrentRankingIndex, valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.DRAGON_TIGER:
        //                         this.changeMissingNumberDragonTigerSort(valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE:
        //                         this.changeMissingNumberFirstSecondSumSingleDoubleSort(valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL:
        //                         this.changeMissingNumberFirstSecondSumBigSmallSort(valueIndex);
        //                         break;
        //                 }
        //             }, this);
        //         }
        //         switch(missingNumberCurrentSortIndex){
        //             case MISSING_NUMBER_PAGE.RANKING:
        //                 this.changeMissingNumberRankingSort(missingNumberCurrentRankingIndex, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.SINGLE_DOUBLE:
        //                 this.changeMissingNumberSingleDoubleSort(missingNumberCurrentRankingIndex, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.BIG_SMALL:
        //                 this.changeMissingNumberBigSmallSort(missingNumberCurrentRankingIndex, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.DRAGON_TIGER:
        //                 this.changeMissingNumberDragonTigerSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE:
        //                 this.changeMissingNumberFirstSecondSumSingleDoubleSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL:
        //                 this.changeMissingNumberFirstSecondSumBigSmallSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //         }
        //     }, this);
        //     missingNumberRankingOptionGroup.add(missingNumberRankingOptionButton[i]);
        //     this.game.add.text(207, 408 + 66 * i, rankingString[i], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, missingNumberRankingOptionGroup).anchor.set(0.5);
        // }

        // let missingNumberSortString: string[] = ["名次", "單雙", "大小", "龍虎", "冠亞和單雙", "冠亞和大小"];
        // let missingNumberSortArrow: Phaser.Sprite = this.game.add.sprite(605, 330, 'Button', 'DropDownArrow', missingNumberGroup);
        // missingNumberSortArrow.anchor.set(0.5);
        // let missingNumberSortDropDownButton: SpriteButton = new SpriteButton(this.game, 385, 290, 'Button', 'DropDownButton_1');
        // missingNumberSortDropDownButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // missingNumberSortDropDownButton.OnUp.add(()=>{
        //     missingNumberRankingOptionGroup.visible = false;
        //     missingNumberRankingArrow.angle = 0;
        //     missingNumberSortOptionGroup.visible = !missingNumberSortOptionGroup.visible;
        //     missingNumberSortArrow.angle = missingNumberSortOptionGroup.visible ? 180 : 0;
        // }, this);
        // missingNumberGroup.add(missingNumberSortDropDownButton);
        // let missingNumberCurrentSortOption: Phaser.Text = this.game.add.text(492, 335, missingNumberSortString[0], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, missingNumberGroup);
        // missingNumberCurrentSortOption.anchor.set(0.5);
        // let missingNumberSortOptionButton: SpriteButton[] = new Array(6);
        // let missingNumberCurrentSortIndex: number = MISSING_NUMBER_PAGE.RANKING;
        // for(let sortIndex: number = 0;sortIndex < 6;sortIndex++){
        //     this.game.add.sprite(385, 371 + 66 * sortIndex, 'Button', 'DropDownBG_1', missingNumberSortOptionGroup);
        //     missingNumberSortOptionButton[sortIndex] = new SpriteButton(this.game, 385, 371 + 66 * sortIndex, 'Button', 'DropDownBG_1', {
        //         Key: 'Button',
        //         OverFrame: 'DropDownSelected_1',
        //         OutFrame: 'DropDownBG_1',
        //         UpFrame: 'DropDownBG_1',
        //         DownFrame: 'DropDownSelected_1'
        //     });
        //     missingNumberSortOptionButton[sortIndex].OnDown.add(()=>{
        //         this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        //     }, this);
        //     missingNumberSortOptionButton[sortIndex].OnUp.add(()=>{
        //         missingNumberFormGroup[missingNumberCurrentSortIndex].visible = false;
        //         missingNumberCurrentSortIndex = sortIndex;
        //         missingNumberFormGroup[missingNumberCurrentSortIndex].visible = true;
        //         missingNumberSortOptionGroup.visible = false;
        //         missingNumberSortArrow.angle = 0;
        //         missingNumberCurrentSortOption.setText(missingNumberSortString[sortIndex]);
        //         missingNumberCurrentRanikingOption.setText(sortIndex > 2 ? "" : rankingString[RANKING.FIRST]);
        //         missingNumberRankingDropDownButton.inputEnabled = sortIndex > 2 ? false : true;
        //         missingNumberCurrentRankingIndex = RANKING.FIRST;
                
        //         for(let valueIndex: number = 0;valueIndex < 7;valueIndex++){
        //             this.missingNumberArrow[valueIndex].events.onInputUp.removeAll();
        //             this.missingNumberArrow[valueIndex].events.onInputUp.add(()=>{
        //                 switch(sortIndex){
        //                     case MISSING_NUMBER_PAGE.RANKING:
        //                         this.changeMissingNumberRankingSort(missingNumberCurrentRankingIndex, valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.SINGLE_DOUBLE:
        //                         this.changeMissingNumberSingleDoubleSort(missingNumberCurrentRankingIndex, valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.BIG_SMALL:
        //                         this.changeMissingNumberBigSmallSort(missingNumberCurrentRankingIndex, valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.DRAGON_TIGER:
        //                         this.changeMissingNumberDragonTigerSort(valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE:
        //                         this.changeMissingNumberFirstSecondSumSingleDoubleSort(valueIndex);
        //                         break;
        //                     case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL:
        //                         this.changeMissingNumberFirstSecondSumBigSmallSort(valueIndex);
        //                         break;
        //                 }
        //             }, this);
        //         }
        //         switch(sortIndex){
        //             case MISSING_NUMBER_PAGE.RANKING:
        //                 this.changeMissingNumberRankingSort(missingNumberCurrentRankingIndex, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.SINGLE_DOUBLE:
        //                 this.changeMissingNumberSingleDoubleSort(missingNumberCurrentRankingIndex, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.BIG_SMALL:
        //                 this.changeMissingNumberBigSmallSort(missingNumberCurrentRankingIndex, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.DRAGON_TIGER:
        //                 this.changeMissingNumberDragonTigerSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE:
        //                 this.changeMissingNumberFirstSecondSumSingleDoubleSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //             case MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL:
        //                 this.changeMissingNumberFirstSecondSumBigSmallSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
        //                 break;
        //         }
        //     }, this);
        //     missingNumberSortOptionGroup.add(missingNumberSortOptionButton[sortIndex]);
        //     this.game.add.text(505, 408 + 66 * sortIndex, missingNumberSortString[sortIndex], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, missingNumberSortOptionGroup).anchor.set(0.5);
        // }

        // this.game.add.sprite(960, 425, 'Lobby', 'MissingNumberFrame_Top', missingNumberFormTitleGroup).anchor.set(0.5);
        // let missingNumberString: string[] = ["號碼", "遺漏輸贏", "今日出現", "當前遺漏", "今日遺漏", "一週遺漏", "一月遺漏", "歷史遺漏"];
        // this.missingNumberArrow = new Array(7);
        // this.missingNumberRankingHorseNumber = new Array(8);
        // this.missingNumberRankingDataText = new Array(8);
        // this.missingNumberSingleDoubleDataText = new Array(2);
        // this.missingNumberBigSmallDataText = new Array(2);
        // this.missingNumberDragonTigerDataText = new Array(8);
        // this.missingNumberFirstSecondSumSingleDoubleDataText = new Array(2);
        // this.missingNumberFirstSecondSumBigSmallDataText = new Array(2);
        // for(let i: number = 0;i < 8;i++){
        //     this.game.add.text(i == 0 ? 165 : 122 + 217 * i, 407, missingNumberString[i], {font: "32px Microsoft JhengHei", fill: "#f2e200"}, missingNumberFormTitleGroup);
        //     if(i < 7){
        //         this.missingNumberArrow[i] = this.game.add.sprite(485 + 217 * i, 428, 'Button', 'DropDownArrow', missingNumberFormTitleGroup);
        //         this.missingNumberArrow[i].anchor.set(0.5);
        //         this.missingNumberArrow[i].inputEnabled = true;
        //         this.missingNumberArrow[i].input.useHandCursor = true;
        //         this.missingNumberArrow[i].events.onInputUp.add(()=>{
        //             this.changeMissingNumberRankingSort(RANKING.FIRST, i);
        //         }, this);

        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Middle', missingNumberFormGroup[MISSING_NUMBER_PAGE.RANKING]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Middle', missingNumberFormGroup[MISSING_NUMBER_PAGE.DRAGON_TIGER]).anchor.set(0.5);
        //     }else{
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Bottom', missingNumberFormGroup[MISSING_NUMBER_PAGE.RANKING]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Bottom', missingNumberFormGroup[MISSING_NUMBER_PAGE.DRAGON_TIGER]).anchor.set(0.5);
        //     }
        //     this.missingNumberRankingHorseNumber[i] = this.game.add.sprite(170, 472 + 71.5 * i, 'Lobby', 'TodayNumber_0' + (i+1), missingNumberFormGroup[MISSING_NUMBER_PAGE.RANKING]);
            
        //     if(i == 0){
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Middle', missingNumberFormGroup[MISSING_NUMBER_PAGE.SINGLE_DOUBLE]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Middle', missingNumberFormGroup[MISSING_NUMBER_PAGE.BIG_SMALL]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Middle', missingNumberFormGroup[MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Middle', missingNumberFormGroup[MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL]).anchor.set(0.5);
        //     }

        //     if(i == 1){
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Bottom', missingNumberFormGroup[MISSING_NUMBER_PAGE.SINGLE_DOUBLE]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Bottom', missingNumberFormGroup[MISSING_NUMBER_PAGE.BIG_SMALL]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Bottom', missingNumberFormGroup[MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE]).anchor.set(0.5);
        //         this.game.add.sprite(960, 497 + 72 * i, 'Lobby', 'MissingNumberFrame_Bottom', missingNumberFormGroup[MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL]).anchor.set(0.5);
        //     }

        //     this.missingNumberRankingDataText[i] = new Array(7);
        //     this.missingNumberSingleDoubleDataText[i] = new Array(8);
        //     this.missingNumberBigSmallDataText[i] = new Array(8);
        //     this.missingNumberDragonTigerDataText[i] = new Array(8);
        //     this.missingNumberFirstSecondSumSingleDoubleDataText[i] = new Array(8);
        //     this.missingNumberFirstSecondSumBigSmallDataText[i] = new Array(8);
        //     for(let j: number = 0;j < 8;j++){
        //         if(j < 7){
        //             this.missingNumberRankingDataText[i][j] = this.game.add.text(416 + 217.5 * j, 500 + 72 * i, "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, missingNumberFormGroup[MISSING_NUMBER_PAGE.RANKING]);
        //             this.missingNumberRankingDataText[i][j].anchor.set(0.5);
        //         }
        //         this.missingNumberDragonTigerDataText[i][j] = this.game.add.text(198.5 + 217.5 * j, 500 + 72 * i, "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, missingNumberFormGroup[MISSING_NUMBER_PAGE.DRAGON_TIGER]);
        //         this.missingNumberDragonTigerDataText[i][j].anchor.set(0.5);

        //         if(i < 2){
        //             this.missingNumberSingleDoubleDataText[i][j] = this.game.add.text(198.5 + 217.5 * j, 500 + 72 * i, "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, missingNumberFormGroup[MISSING_NUMBER_PAGE.SINGLE_DOUBLE]);
        //             this.missingNumberSingleDoubleDataText[i][j].anchor.set(0.5);
        //             this.missingNumberBigSmallDataText[i][j] = this.game.add.text(198.5 + 217.5 * j, 500 + 72 * i, "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, missingNumberFormGroup[MISSING_NUMBER_PAGE.BIG_SMALL]);
        //             this.missingNumberBigSmallDataText[i][j].anchor.set(0.5);
        //             this.missingNumberFirstSecondSumSingleDoubleDataText[i][j] = this.game.add.text(198.5 + 217.5 * j, 500 + 72 * i, "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, missingNumberFormGroup[MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_SINGLE_DOUBLE]);
        //             this.missingNumberFirstSecondSumSingleDoubleDataText[i][j].anchor.set(0.5);
        //             this.missingNumberFirstSecondSumBigSmallDataText[i][j] = this.game.add.text(198.5 + 217.5 * j, 500 + 72 * i, "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, missingNumberFormGroup[MISSING_NUMBER_PAGE.FIRST_SECOND_SUM_BIG_SMALL]);
        //             this.missingNumberFirstSecondSumBigSmallDataText[i][j].anchor.set(0.5);
        //         }
        //     }
        // }

        // this.game.add.text(85, 280, "使用說明:", {font: "32px Microsoft JhengHei", fill: "#f2e200"}, todayNumberGroup);
        // this.game.add.text(235, 280, "提供號碼當日的開出次數及近期未開次數，總開: 近50期統計。未開: 近500期統計。使用參數設置可自定義關注", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberGroup);
        // this.game.add.text(85, 317, "遺漏範圍，請根據個人需要手動設置，各參數的數值可設置範圍是: 2-500。", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberGroup);
        // this.game.add.text(85, 360, "參數設置:", {font: "32px Microsoft JhengHei", fill: "#f2e200"}, todayNumberGroup);
        // this.game.add.text(235, 360, "當前數字為             至             時為    色", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberGroup);
        // this.game.add.sprite(400, 360, 'Lobby', 'TodayNumberInputBG', todayNumberGroup);
        // this.game.add.sprite(535, 360, 'Lobby', 'TodayNumberInputBG', todayNumberGroup);
        // this.game.add.sprite(704, 360, 'Lobby', 'TodayNumberIcon', todayNumberGroup).tint = 0xff0019;
        // let firstMinInput: PhaserInput.InputField = this.game.add.inputField(400, 360, {
        //     font: '32px Microsoft JhengHei',
        //     fill: '#ffffff',
        //     fillAlpha: 0,
        //     width: 95,
        //     cursorColor: '#ffffff',
        //     min: '1',
        //     max: '500',
        //     placeHolder: '15',
        //     textAlign: 'center',
        //     type: PhaserInput.InputType.number
        // }, todayNumberGroup);
        // let firstMaxInput: PhaserInput.InputField = this.game.add.inputField(535, 360, {
        //     font: '32px Microsoft JhengHei',
        //     fill: '#ffffff',
        //     fillAlpha: 0,
        //     width: 95,
        //     cursorColor: '#ffffff',
        //     min: '1',
        //     max: '500',
        //     placeHolder: '100',
        //     textAlign: 'center',
        //     type: PhaserInput.InputType.number
        // }, todayNumberGroup);
        // let firstCalculatingSigns: SpriteButton = new SpriteButton(this.game, 765, 357, 'Button', 'TodayNumberPlus');
        // todayNumberGroup.add(firstCalculatingSigns);
        // firstCalculatingSigns.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // firstCalculatingSigns.OnUp.add(()=>{
        //     if(!todayNumberSecondOptionGroup.visible){
        //         todayNumberSecondOptionGroup.visible = true;
        //         firstCalculatingConfirm.visible = false;
        //         firstCalculatingSigns.loadTexture('Button', 'TodayNumberMinus');
        //         secondMinInput.resetText();
        //         secondMaxInput.resetText();
        //     }else{
        //         todayNumberSecondOptionGroup.visible = false;
        //         firstCalculatingConfirm.visible = true;
        //         firstCalculatingSigns.loadTexture('Button', 'TodayNumberPlus');
        //         if(todayNumberThirdOptionGroup.visible){
        //             secondCalculatingConfirm.visible = true;
        //             todayNumberThirdOptionGroup.visible = false;
        //             secondCalculatingSigns.loadTexture('Button', 'TodayNumberPlus');
        //         }
        //     }
        // }, this);
        // let firstCalculatingConfirm: SpriteButton = new SpriteButton(this.game, 825, 350, 'Button', 'TodayNumberOptionConfirm');
        // firstCalculatingConfirm.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // firstCalculatingConfirm.OnUp.add(()=>{
        //     if(Number(firstMinInput.value) < 2){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請填寫大於1的數值"}));
        //         return;
        //     }

        //     if(firstMinInput.value == "" || firstMaxInput.value == ""){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請填寫範圍"}));
        //         return;
        //     }

        //     if(Number(firstMinInput.value) >= Number(firstMaxInput.value)){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請依數值大小，由左至右填寫"}));
        //         return;
        //     }
        //     this.changeTodayNumberTextColor(Number(firstMinInput.value), Number(firstMaxInput.value));
        // }, this);
        // todayNumberGroup.add(firstCalculatingConfirm);
        // this.game.add.text(923, 360, "至             時為    色", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberSecondOptionGroup);
        // this.game.add.sprite(825, 360, 'Lobby', 'TodayNumberInputBG', todayNumberSecondOptionGroup);
        // this.game.add.sprite(960, 360, 'Lobby', 'TodayNumberInputBG', todayNumberSecondOptionGroup);
        // this.game.add.sprite(1128, 360, 'Lobby', 'TodayNumberIcon', todayNumberSecondOptionGroup).tint = 0x0062ff;
        // let secondMinInput: PhaserInput.InputField = this.game.add.inputField(825, 360, {
        //     font: '32px Microsoft JhengHei',
        //     fill: '#ffffff',
        //     fillAlpha: 0,
        //     width: 95,
        //     cursorColor: '#ffffff',
        //     min: '1',
        //     max: '500',
        //     placeHolder: '101',
        //     textAlign: 'center',
        //     type: PhaserInput.InputType.number
        // }, todayNumberSecondOptionGroup);
        // let secondMaxInput: PhaserInput.InputField = this.game.add.inputField(960, 360, {
        //     font: '32px Microsoft JhengHei',
        //     fill: '#ffffff',
        //     fillAlpha: 0,
        //     width: 95,
        //     cursorColor: '#ffffff',
        //     min: '1',
        //     max: '500',
        //     placeHolder: '150',
        //     textAlign: 'center',
        //     type: PhaserInput.InputType.number
        // }, todayNumberSecondOptionGroup);
        // let secondCalculatingSigns: SpriteButton = new SpriteButton(this.game, 1189, 357, 'Button', 'TodayNumberPlus');
        // todayNumberSecondOptionGroup.add(secondCalculatingSigns);
        // secondCalculatingSigns.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // secondCalculatingSigns.OnUp.add(()=>{
        //     if(!todayNumberThirdOptionGroup.visible){
        //         todayNumberThirdOptionGroup.visible = true;
        //         secondCalculatingConfirm.visible = false;
        //         secondCalculatingSigns.loadTexture('Button', 'TodayNumberMinus');
        //         thirdMinInput.resetText();
        //         thirdMaxInput.resetText();
        //     }else{
        //         todayNumberThirdOptionGroup.visible = false;
        //         secondCalculatingConfirm.visible = true;
        //         secondCalculatingSigns.loadTexture('Button', 'TodayNumberPlus');
        //     }
        // }, this);
        // let secondCalculatingConfirm: SpriteButton = new SpriteButton(this.game, 1249, 350, 'Button', 'TodayNumberOptionConfirm');
        // secondCalculatingConfirm.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // secondCalculatingConfirm.OnUp.add(()=>{
        //     if(Number(firstMinInput.value) < 2){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請填寫大於1的數值"}));
        //         return;
        //     }

        //     if(firstMinInput.value == "" || firstMaxInput.value == "" || secondMinInput.value == "" || secondMaxInput.value == ""){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請填寫範圍"}));
        //         return;
        //     }
        //     if((Number(firstMinInput.value) >= Number(firstMaxInput.value)) || (Number(secondMinInput.value) <= Number(firstMaxInput.value)) || (Number(secondMinInput.value) >= Number(secondMaxInput.value))){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請依數值大小，由左至右填寫"}));
        //         return;
        //     }
        //     this.changeTodayNumberTextColor(Number(firstMinInput.value), Number(firstMaxInput.value), Number(secondMinInput.value), Number(secondMaxInput.value));
        // }, this);
        // todayNumberSecondOptionGroup.add(secondCalculatingConfirm);
        // this.game.add.text(1347, 360, "至             時為    色", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberThirdOptionGroup);
        // this.game.add.sprite(1249, 360, 'Lobby', 'TodayNumberInputBG', todayNumberThirdOptionGroup);
        // this.game.add.sprite(1384, 360, 'Lobby', 'TodayNumberInputBG', todayNumberThirdOptionGroup);
        // this.game.add.sprite(1552, 360, 'Lobby', 'TodayNumberIcon', todayNumberThirdOptionGroup).tint = 0x00ff0d;
        // let thirdMinInput: PhaserInput.InputField = this.game.add.inputField(1249, 360, {
        //     font: '32px Microsoft JhengHei',
        //     fill: '#ffffff',
        //     fillAlpha: 0,
        //     width: 95,
        //     cursorColor: '#ffffff',
        //     min: '1',
        //     max: '500',
        //     placeHolder: '151',
        //     textAlign: 'center',
        //     type: PhaserInput.InputType.number
        // }, todayNumberThirdOptionGroup);
        // let thirdMaxInput: PhaserInput.InputField = this.game.add.inputField(1384, 360, {
        //     font: '32px Microsoft JhengHei',
        //     fill: '#ffffff',
        //     fillAlpha: 0,
        //     width: 95,
        //     cursorColor: '#ffffff',
        //     min: '1',
        //     max: '500',
        //     placeHolder: '200',
        //     textAlign: 'center',
        //     type: PhaserInput.InputType.number
        // }, todayNumberThirdOptionGroup);
        // let thirdCalculatingConfirm: SpriteButton = new SpriteButton(this.game, 1613, 350, 'Button', 'TodayNumberOptionConfirm');
        // thirdCalculatingConfirm.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // thirdCalculatingConfirm.OnUp.add(()=>{
        //     if(Number(firstMinInput.value) < 2){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請填寫大於1的數值"}));
        //         return;
        //     }

        //     if(firstMinInput.value == "" || firstMaxInput.value == "" || secondMinInput.value == "" || secondMaxInput.value == "" || thirdMinInput.value == "" || thirdMaxInput.value == ""){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請填寫範圍"}));
        //         return;
        //     }

        //     if((Number(firstMinInput.value) >= Number(firstMaxInput.value)) || (Number(secondMinInput.value) <= Number(firstMaxInput.value)) || (Number(secondMinInput.value) >= Number(secondMaxInput.value)) || (Number(thirdMinInput.value) <= Number(secondMaxInput.value)) || (Number(thirdMinInput.value) >= Number(thirdMaxInput.value))){
        //         window.dispatchEvent(new CustomEvent('Warning', {'detail': "請依數值大小，由左至右填寫"}));
        //         return;
        //     }
        //     this.changeTodayNumberTextColor(Number(firstMinInput.value), Number(firstMaxInput.value), Number(secondMinInput.value), Number(secondMaxInput.value), Number(thirdMinInput.value), Number(thirdMaxInput.value));
        // }, this);
        // todayNumberThirdOptionGroup.add(thirdCalculatingConfirm);
        // this.todayNumberDataText = new Array(16);
        // for(let i: number = 0;i < 16;i++){
        //     if(i < 8){
        //         this.game.add.image(960, 525 + 220 * i, 'TodayNumberFrame', null, todayNumberFormScrollGroup).anchor.set(0.5);
        //         this.game.add.text(157, 532 + 220 * i, rankingString[i], {font: "34px Microsoft JhengHei", fill: "#ffffff"}, todayNumberFormScrollGroup).anchor.set(0.5);
        //         this.game.add.text(275, 450 + 220 * i, "號碼", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberFormScrollGroup);
        //         this.game.add.text(275, 513 + 220 * i, "總開", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberFormScrollGroup);
        //         this.game.add.text(275, 570 + 220 * i, "未開", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberFormScrollGroup);
        //     }
        //     this.todayNumberDataText[i] = new Array(8);
        //     for(let j: number = 0;j < 8;j++){
        //         if(i < 8){
        //             this.game.add.sprite(457 + 179 * j, 442 + 220 * i, 'Lobby', 'TodayNumber_0' + (j + 1), todayNumberFormScrollGroup);
        //         }
        //         this.todayNumberDataText[i][j] = this.game.add.text(482 + 179 * j, 536 + (i % 2 == 0 ? 220 * (i / 2) : (220 * (i - 1) / 2) + 56), "", {font: "32px Microsoft JhengHei", fill: "#ffffff"}, todayNumberFormScrollGroup);
        //         this.todayNumberDataText[i][j].anchor.set(0.5);
        //     }
        // }
        // let todayNumberScrollMask: Phaser.Graphics = this.game.add.graphics(88, 432, todayNumberGroup);
        // todayNumberScrollMask.beginFill(0xffffff, 0);
        // todayNumberScrollMask.drawRect(0, 0, 1744, 608);
        // todayNumberScrollMask.endFill();
        // todayNumberScrollMask.inputEnabled = true;
        // todayNumberFormScrollGroup.mask = todayNumberScrollMask;
        // let todayNumberFirstTouchPosY: number;
        // let todayNumberScrollPosY: number;
        // let todayNumberScrollTimerEvent: Phaser.TimerEvent;
        // todayNumberScrollMask.events.onInputDown.add(()=>{
        //     todayNumberFirstTouchPosY = this.game.input.y;
        //     todayNumberScrollPosY = todayNumberFormScrollGroup.y;
        //     todayNumberScrollTimerEvent = this.game.time.events.loop(50, ()=>{
        //         todayNumberScrollPosY -= todayNumberFirstTouchPosY - this.game.input.y;
        //         if(todayNumberScrollPosY > 0){
        //             todayNumberScrollPosY = 0;
        //         }
        //         if(todayNumberScrollPosY < -1120){
        //             todayNumberScrollPosY = -1120;
        //         }
        //         todayNumberFormScrollGroup.position.set(0, todayNumberScrollPosY);
        //     }, this);
        // }, this);
        // todayNumberScrollMask.events.onInputUp.add(()=>{
        //     this.game.time.events.remove(todayNumberScrollTimerEvent);
        // }, this);

        // let dailyDragonSortString: string[] = ["大長龍", "小長龍", "單長龍", "雙長龍", "龍長龍", "虎長龍"];
        // let dailyDragonSortArrow: Phaser.Sprite = this.game.add.sprite(287, 350, 'Button', 'DropDownArrow', dailyDragonGroup);
        // dailyDragonSortArrow.anchor.set(0.5);
        // let dailyDragonSortDropDownButton: SpriteButton = new SpriteButton(this.game, 87, 310, 'Button', 'DropDownButton_1');
        // dailyDragonSortDropDownButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // dailyDragonSortDropDownButton.OnUp.add(()=>{
        //     dailyDragonRankingOptionGroup.visible = false;
        //     dailyDragonRankingArrow.angle = 0;
        //     dailyDragonSortOptionGroup.visible = !dailyDragonSortOptionGroup.visible;
        //     dailyDragonSortArrow.angle = dailyDragonSortOptionGroup.visible ? 180 : 0;
        // }, this);
        // dailyDragonGroup.add(dailyDragonSortDropDownButton);
        // let dailyDragonCurrentSortOption: Phaser.Text = this.game.add.text(192, 355, dailyDragonSortString[0], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonGroup);
        // dailyDragonCurrentSortOption.anchor.set(0.5);
        // let dailyDragonSortOptionButton: SpriteButton[] = new Array(6);
        // let dailyDragonCurrentSortIndex: number = DAILY_DRAGON_PAGE.BIG_LONG_DRAGON;
        // for(let i: number = 0;i < 6;i++){
        //     this.game.add.sprite(85, 391 + 66 * i, 'Button', 'DropDownBG_1', dailyDragonSortOptionGroup);
        //     dailyDragonSortOptionButton[i] = new SpriteButton(this.game, 87, 391 + 66 * i, 'Button', 'DropDownBG_1', {
        //         Key: 'Button',
        //         OverFrame: 'DropDownSelected_1',
        //         OutFrame: 'DropDownBG_1',
        //         UpFrame: 'DropDownBG_1',
        //         DownFrame: 'DropDownSelected_1'
        //     });
        //     dailyDragonSortOptionButton[i].OnDown.add(()=>{
        //         this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        //     }, this);
        //     dailyDragonSortOptionButton[i].OnUp.add(()=>{
        //         dailyDragonSortOptionGroup.visible = false;
        //         dailyDragonSortArrow.angle = 0;
        //         dailyDragonCurrentSortOption.setText(dailyDragonSortString[i]);
        //         dailyDragonCurrentRankingOption.setText(rankingString[RANKING.FIRST]);
        //         dailyDragonCurrentTitleText.setText(dailyDragonTitleString[i]);

        //         this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].visible = false;
        //         dailyDragonCurrentSortIndex = i;
        //         dailyDragonCurrentRankIndex = RANKING.FIRST;
        //         this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].visible = true;

        //         dailyDragonScrollMask.events.onInputDown.removeAll();
        //         dailyDragonScrollMask.events.onInputDown.add(()=>{
        //             this.startDailyDragonFormScroll(dailyDragonCurrentSortIndex, dailyDragonCurrentRankIndex);
        //         }, this);
        //         this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].position.set(0);

        //         for(let buttonIndex: number = 4;buttonIndex < 8;buttonIndex++){
        //             dailyDragonRankingOptionButton[buttonIndex].visible = i < 4 ? true : false;
        //             dailyDragonRankingOptionBG[buttonIndex].visible = i < 4 ? true : false;
        //             dailyDragonRankingOptionText[buttonIndex].visible = i < 4 ? true : false;
        //         }

        //         this.changeDailyDragonFormDateText(i, RANKING.FIRST);
        //     }, this);
        //     dailyDragonSortOptionGroup.add(dailyDragonSortOptionButton[i]);
        //     this.game.add.text(207, 428 + 66 * i, dailyDragonSortString[i], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonSortOptionGroup).anchor.set(0.5);
        // }

        // let dailyDragonRankingArrow: Phaser.Sprite = this.game.add.sprite(605, 350, 'Button', 'DropDownArrow', dailyDragonGroup);
        // dailyDragonRankingArrow.anchor.set(0.5);
        // let dailyDragonRankingDropDownButton: SpriteButton = new SpriteButton(this.game, 385, 310, 'Button', 'DropDownButton_1');
        // dailyDragonRankingDropDownButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // dailyDragonRankingDropDownButton.OnUp.add(()=>{
        //     dailyDragonSortOptionGroup.visible = false;
        //     dailyDragonSortArrow.angle = 0;
        //     dailyDragonRankingOptionGroup.visible = !dailyDragonRankingOptionGroup.visible;
        //     dailyDragonRankingArrow.angle = dailyDragonRankingOptionGroup.visible ? 180 : 0;
        // }, this);
        // dailyDragonGroup.add(dailyDragonRankingDropDownButton);
        // let dailyDragonCurrentRankingOption: Phaser.Text = this.game.add.text(492, 355, rankingString[0], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonGroup);
        // dailyDragonCurrentRankingOption.anchor.set(0.5);
        // let dailyDragonRankingOptionButton: SpriteButton[] = new Array(8);
        // let dailyDragonRankingOptionBG: Phaser.Sprite[] = new Array(8);
        // let dailyDragonRankingOptionText: Phaser.Text[] = new Array(8);
        // let dailyDragonCurrentRankIndex: number = RANKING.FIRST;
        // this.dailyDragonFormDateText = new Array(7);
        // for(let i: number = 0;i < 8;i++){
        //     dailyDragonRankingOptionBG[i] = this.game.add.sprite(385, 391 + 66 * i, 'Button', 'DropDownBG_1', dailyDragonRankingOptionGroup);
        //     dailyDragonRankingOptionButton[i] = new SpriteButton(this.game, 385, 391 + 66 * i, 'Button', 'DropDownBG_1', {
        //         Key: 'Button',
        //         OverFrame: 'DropDownSelected_1',
        //         OutFrame: 'DropDownBG_1',
        //         UpFrame: 'DropDownBG_1',
        //         DownFrame: 'DropDownSelected_1'
        //     });
        //     dailyDragonRankingOptionButton[i].OnDown.add(()=>{
        //         this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        //     }, this);
        //     dailyDragonRankingOptionButton[i].OnUp.add(()=>{
        //         dailyDragonRankingOptionGroup.visible = false;
        //         dailyDragonRankingArrow.angle = 0;
        //         dailyDragonCurrentRankingOption.setText(rankingString[i]);

        //         this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].visible = false;
        //         dailyDragonCurrentRankIndex = i;
        //         this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].visible = true;

        //         dailyDragonScrollMask.events.onInputDown.removeAll();
        //         dailyDragonScrollMask.events.onInputDown.add(()=>{
        //             this.startDailyDragonFormScroll(dailyDragonCurrentSortIndex, dailyDragonCurrentRankIndex);
        //         }, this);
        //         this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].position.set(0);

        //         this.changeDailyDragonFormDateText(dailyDragonCurrentSortIndex, i);
        //     }, this);
        //     dailyDragonRankingOptionGroup.add(dailyDragonRankingOptionButton[i]);
        //     dailyDragonRankingOptionText[i] = this.game.add.text(505, 428 + 66 * i, rankingString[i], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonRankingOptionGroup);
        //     dailyDragonRankingOptionText[i].anchor.set(0.5);

        //     if(i < 7){
        //         this.dailyDragonFormDateText[i] = this.game.add.text(210, 543 + 70 * i, "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonFormLeftGroup);
        //         this.dailyDragonFormDateText[i].anchor.set(0.5);
        //     }
        // }
        // let dailyDragonTitleString: string[] = ["大", "小", "單", "雙", "龍", "虎"];
        // let dailyDragonCurrentTitleText: Phaser.Text = this.game.add.text(874, 330, dailyDragonTitleString[dailyDragonCurrentSortIndex], {font: "36px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonGroup);
        // this.game.add.text(670, 330, "★ 近七日開 '     ' 長龍出現條數", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, dailyDragonGroup);
        // this.game.add.sprite(87, 435, 'Lobby', 'DailyDragonFrame_Left', dailyDragonFormLeftGroup);
        // this.game.add.text(210, 476, "日期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, dailyDragonFormLeftGroup).anchor.set(0.5);
        
        // let dailyDragonScrollMask: Phaser.Graphics = this.game.add.graphics(331, 432, dailyDragonFormLeftGroup);
        // dailyDragonScrollMask.beginFill(0xffffff, 0);
        // dailyDragonScrollMask.drawRect(0, 0, 1504, 568);
        // dailyDragonScrollMask.endFill();
        // dailyDragonScrollMask.inputEnabled = true;
        // this.dailyDragonFormScrollLimit = new Array(6);
        // for(let sortIndex: number = 0;sortIndex < 6;sortIndex++){
        //     let rankLength: number = sortIndex < 4 ? 8 : 4;
        //     this.dailyDragonFormScrollLimit[sortIndex] = new Array(rankLength);
        //     for(let rankIndex: number = 0;rankIndex < rankLength;rankIndex++){
        //         this.dailyDragonFormScrollLimit[sortIndex][rankIndex] = 0;
        //         this.dailyDragonFormScrollGroup[sortIndex][rankIndex].mask = dailyDragonScrollMask;
        //     }
        // }
        // dailyDragonScrollMask.events.onInputUp.add(this.stopDailyDragonFormScroll, this);
        // dailyDragonScrollMask.events.onInputOut.add(this.stopDailyDragonFormScroll, this);
        
        // let twoSideRankingArrow: Phaser.Sprite = this.game.add.sprite(287, 330, 'Button', 'DropDownArrow', twoSideGroup);
        // twoSideRankingArrow.anchor.set(0.5);
        // let twoSideRankingDropDownButton: SpriteButton = new SpriteButton(this.game, 87, 290, 'Button', 'DropDownButton_1');
        // twoSideRankingDropDownButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // twoSideRankingDropDownButton.OnUp.add(()=>{
        //     twoSideRankingOptionGroup.visible = !twoSideRankingOptionGroup.visible;
        //     twoSideRankingArrow.angle = twoSideRankingOptionGroup.visible ? 180 : 0;
        // }, this);
        // twoSideGroup.add(twoSideRankingDropDownButton);
        // let twoSideCurrentRanikingOption: Phaser.Text = this.game.add.text(192, 335, rankingString[0], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, twoSideGroup);
        // twoSideCurrentRanikingOption.anchor.set(0.5);
        // let twoSideRankingOptionButton: SpriteButton[] = new Array(8);
        // for(let i: number = 0;i < 8;i++){
        //     this.game.add.sprite(85, 371 + 66 * i, 'Button', 'DropDownBG_1', twoSideRankingOptionGroup);
        //     twoSideRankingOptionButton[i] = new SpriteButton(this.game, 87, 371 + 66 * i, 'Button', 'DropDownBG_1', {
        //         Key: 'Button',
        //         OverFrame: 'DropDownSelected_1',
        //         OutFrame: 'DropDownBG_1',
        //         UpFrame: 'DropDownBG_1',
        //         DownFrame: 'DropDownSelected_1'
        //     });
        //     twoSideRankingOptionButton[i].OnDown.add(()=>{
        //         this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        //     }, this);
        //     twoSideRankingOptionButton[i].OnUp.add(()=>{
        //         twoSideRankingOptionGroup.visible = false;
        //         twoSideRankingArrow.angle = 0;
        //         twoSideCurrentRanikingOption.setText(rankingString[i]);
        //         this.changeTwoSideForm(i);
        //     }, this);
        //     twoSideRankingOptionGroup.add(twoSideRankingOptionButton[i]);
        //     this.game.add.text(207, 408 + 66 * i, rankingString[i], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, twoSideRankingOptionGroup).anchor.set(0.5);
        // }
        // this.game.add.text(370, 310, "★ 以每10期為一組，統計最近20組中大小單雙的出現次數", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, twoSideGroup);
        // this.game.add.sprite(960, 425, 'Lobby', 'TwoSideFrame_Top', twoSideFormTitleGroup).anchor.set(0.5);
        // this.game.add.text(297, 430, "日期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, twoSideFormTitleGroup).anchor.set(0.5);
        // let bigSmallSingleDoubleTitleString: string[] = ["大", "小", "單", "雙"];
        // for(let i:number = 0;i< 4;i++){
        //     this.game.add.text(670 + 333 * i, 430, bigSmallSingleDoubleTitleString[i], {font: "36px Microsoft JhengHei", fill: "#f2e200"}, twoSideFormTitleGroup).anchor.set(0.5);
        // }
        // let twoSideScrollMask: Phaser.Graphics = this.game.add.graphics(86, 459, twoSideFormTitleGroup);
        // twoSideScrollMask.beginFill(0xffffff, 0);
        // twoSideScrollMask.drawRect(0, 0, 1748, 564);
        // twoSideScrollMask.endFill();
        // twoSideScrollMask.inputEnabled = true;
        // twoSideScrollMask.events.onInputDown.add(this.startTwoSideFormScroll, this);
        // twoSideScrollMask.events.onInputUp.add(this.stopTwoSideFormScroll, this);
        // twoSideScrollMask.events.onInputOut.add(this.stopTwoSideFormScroll, this);
        // this.twoSideFormScrollGroup.mask = twoSideScrollMask;

        // let singleDoubleRankingArrow: Phaser.Sprite = this.game.add.sprite(287, 330, 'Button', 'DropDownArrow', singleDoubleGroup);
        // singleDoubleRankingArrow.anchor.set(0.5);
        // let singleDoubleRankingDropDownButton: SpriteButton = new SpriteButton(this.game, 87, 290, 'Button', 'DropDownButton_1');
        // singleDoubleRankingDropDownButton.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // singleDoubleRankingDropDownButton.OnUp.add(()=>{
        //     singleDoubleRankingOptionGroup.visible = !singleDoubleRankingOptionGroup.visible;
        //     singleDoubleRankingArrow.angle = singleDoubleRankingOptionGroup.visible ? 180 : 0;
        // }, this);
        // singleDoubleGroup.add(singleDoubleRankingDropDownButton);
        // let singleDoubleCurrentRanikingOption: Phaser.Text = this.game.add.text(192, 335, rankingString[0], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, singleDoubleGroup);
        // singleDoubleCurrentRanikingOption.anchor.set(0.5);
        // let singleDoubleRankingOptionButton: SpriteButton[] = new Array(8);
        // for(let i: number = 0;i < 8;i++){
        //     this.game.add.sprite(85, 371 + 66 * i, 'Button', 'DropDownBG_1', singleDoubleRankingOptionGroup);
        //     singleDoubleRankingOptionButton[i] = new SpriteButton(this.game, 87, 371 + 66 * i, 'Button', 'DropDownBG_1', {
        //         Key: 'Button',
        //         OverFrame: 'DropDownSelected_1',
        //         OutFrame: 'DropDownBG_1',
        //         UpFrame: 'DropDownBG_1',
        //         DownFrame: 'DropDownSelected_1'
        //     });
        //     singleDoubleRankingOptionButton[i].OnDown.add(()=>{
        //         this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        //     }, this);
        //     singleDoubleRankingOptionButton[i].OnUp.add(()=>{
        //         singleDoubleRankingOptionGroup.visible = false;
        //         singleDoubleRankingArrow.angle = 0;
        //         singleDoubleCurrentRanikingOption.setText(rankingString[i]);
        //         this.changeSingleDoubleForm(i);
        //     }, this);
        //     singleDoubleRankingOptionGroup.add(singleDoubleRankingOptionButton[i]);
        //     this.game.add.text(207, 408 + 66 * i, rankingString[i], {font: "38px Microsoft JhengHei", fill: "#ffffff"}, singleDoubleRankingOptionGroup).anchor.set(0.5);
        // }
        // this.game.add.sprite(960, 425, 'Lobby', 'TwoSideFrame_Top', singleDoubleFormTitleGroup).anchor.set(0.5);
        // this.game.add.text(297, 430, "日期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, singleDoubleFormTitleGroup).anchor.set(0.5);
        // for(let i:number = 0;i< 4;i++){
        //     this.game.add.text(670 + 333 * i, 430, bigSmallSingleDoubleTitleString[i], {font: "36px Microsoft JhengHei", fill: "#f2e200"}, singleDoubleFormTitleGroup).anchor.set(0.5);
        // }
        // let singleDoubleScrollMask: Phaser.Graphics = this.game.add.graphics(86, 459, singleDoubleFormTitleGroup);
        // singleDoubleScrollMask.beginFill(0xffffff, 0);
        // singleDoubleScrollMask.drawRect(0, 0, 1748, 564);
        // singleDoubleScrollMask.endFill();
        // singleDoubleScrollMask.inputEnabled = true;
        // singleDoubleScrollMask.events.onInputDown.add(this.startSingleDoubleFormScroll, this);
        // singleDoubleScrollMask.events.onInputUp.add(this.stopSingleDoubleFormScroll, this);
        // singleDoubleScrollMask.events.onInputOut.add(this.stopSingleDoubleFormScroll, this);
        // this.singleDoubleFormScrollGroup.mask = singleDoubleScrollMask;

        // this.game.add.sprite(960, 335, 'Lobby', 'FirstSecondSumFrame_Top', firstSecondSumGroup).anchor.set(0.5);
        // let firstSecondSumTitleString: string[] = ["日期", "冠亞和大", "冠亞和小", "冠亞和單", "冠亞和雙"];
        // for(let i:number = 0;i< 5;i++){
        //     this.game.add.text(263 + 350 * i, 340, firstSecondSumTitleString[i], {font: "36px Microsoft JhengHei", fill: "#f2e200"}, firstSecondSumGroup).anchor.set(0.5);
        // }
        // this.firstSecondSumFormScrollGroup = this.game.add.group(firstSecondSumGroup);
        // let firstSecondSumScrollMask: Phaser.Graphics = this.game.add.graphics(86, 368, firstSecondSumGroup);
        // firstSecondSumScrollMask.beginFill(0xffffff, 0);
        // firstSecondSumScrollMask.drawRect(0, 0, 1748, 635);
        // firstSecondSumScrollMask.endFill();
        // firstSecondSumScrollMask.inputEnabled = true;
        // firstSecondSumScrollMask.events.onInputDown.add(this.startFirstSecondSumFormScroll, this);
        // firstSecondSumScrollMask.events.onInputUp.add(this.stopFirstSecondSumFormScroll, this);
        // firstSecondSumScrollMask.events.onInputOut.add(this.stopFirstSecondSumFormScroll, this);
        // this.firstSecondSumFormScrollGroup.mask = firstSecondSumScrollMask;

        // this.currentSelectedStatPage = selectMissingNumber;
        // this.currentSelectedStatPage.SelectButton();
        // this.currentShowGroup = missingNumberGroup;

        // selectMissingNumber.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // selectMissingNumber.OnUp.add(()=>{
        //     this.changeStatPage(selectMissingNumber, missingNumberGroup);

        //     for(let sortIndex: number = 0;sortIndex < 7;sortIndex++){
        //         this.missingNumberArrow[sortIndex].events.onInputUp.removeAll();
        //         this.missingNumberArrow[sortIndex].events.onInputUp.add(()=>{
        //             this.changeMissingNumberRankingSort(RANKING.FIRST, sortIndex);
        //         }, this);
        //     }
        //     this.changeMissingNumberRankingSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);

        //     missingNumberFormGroup[missingNumberCurrentSortIndex].visible = false;
        //     missingNumberCurrentSortIndex = MISSING_NUMBER_PAGE.RANKING;
        //     missingNumberFormGroup[missingNumberCurrentSortIndex].visible = true;

        //     missingNumberSortOptionGroup.visible = false;
        //     missingNumberSortArrow.angle = 0;
        //     missingNumberCurrentSortOption.setText(missingNumberSortString[missingNumberCurrentSortIndex]);

        //     missingNumberRankingDropDownButton.inputEnabled = true;
        //     missingNumberRankingOptionGroup.visible = false;
        //     missingNumberRankingArrow.angle = 0;
        //     missingNumberCurrentRankingIndex = RANKING.FIRST;
        //     missingNumberCurrentRanikingOption.setText(rankingString[missingNumberCurrentRankingIndex]);
        // }, this);

        // selectTodayNumber.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // selectTodayNumber.OnUp.add(()=>{
        //     this.changeStatPage(selectTodayNumber, todayNumberGroup);
        //     this.changeTodayNumberTextColor(0, 0);
            
        //     firstCalculatingConfirm.visible = true;
        //     firstCalculatingSigns.loadTexture('Button', 'TodayNumberPlus');
        //     firstMinInput.resetText();
        //     firstMaxInput.resetText();
        //     todayNumberSecondOptionGroup.visible = false;

        //     if(todayNumberThirdOptionGroup.visible){
        //         todayNumberThirdOptionGroup.visible = false;
        //         secondCalculatingConfirm.visible = true;
        //         secondCalculatingSigns.loadTexture('Button', 'TodayNumberPlus');
        //     }
        // }, this);

        // selectDailyDragon.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // selectDailyDragon.OnUp.add(()=>{
        //     this.changeStatPage(selectDailyDragon, dailyDragonGroup);

        //     dailyDragonSortOptionGroup.visible = false;
        //     dailyDragonSortArrow.angle = 0;
        //     dailyDragonCurrentSortOption.setText(dailyDragonSortString[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON]);
        //     dailyDragonCurrentTitleText.setText(dailyDragonTitleString[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON]);
            
        //     dailyDragonRankingOptionGroup.visible = false;
        //     dailyDragonRankingArrow.angle = 0;
        //     dailyDragonCurrentRankingOption.setText(rankingString[RANKING.FIRST]);

        //     this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].visible = false;
        //     dailyDragonCurrentSortIndex = DAILY_DRAGON_PAGE.BIG_LONG_DRAGON;
        //     dailyDragonCurrentRankIndex = RANKING.FIRST;
        //     this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].visible = true;
        //     this.changeDailyDragonFormDateText(dailyDragonCurrentSortIndex, dailyDragonCurrentRankIndex);

        //     for(let buttonIndex: number = 4;buttonIndex < 8;buttonIndex++){
        //         dailyDragonRankingOptionButton[buttonIndex].visible = true;
        //         dailyDragonRankingOptionBG[buttonIndex].visible = true;
        //         dailyDragonRankingOptionText[buttonIndex].visible = true;
        //     }

        //     dailyDragonScrollMask.events.onInputDown.removeAll();
        //     dailyDragonScrollMask.events.onInputDown.add(()=>{
        //         this.startDailyDragonFormScroll(DAILY_DRAGON_PAGE.BIG_LONG_DRAGON, RANKING.FIRST);
        //     }, this);
        //     this.dailyDragonFormScrollGroup[dailyDragonCurrentSortIndex][dailyDragonCurrentRankIndex].position.set(0);
        // }, this);

        // selectTwoSide.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // selectTwoSide.OnUp.add(()=>{
        //     this.changeStatPage(selectTwoSide, twoSideGroup);
        //     this.changeTwoSideForm(0);

        //     twoSideRankingOptionGroup.visible = false;
        //     twoSideRankingArrow.angle = 0;
        //     twoSideCurrentRanikingOption.setText(rankingString[0]);
        // }, this);

        // selectSingleDouble.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // selectSingleDouble.OnUp.add(()=>{
        //     this.changeStatPage(selectSingleDouble, singleDoubleGroup);
        //     this.changeSingleDoubleForm(0);

        //     singleDoubleRankingOptionGroup.visible = false;
        //     singleDoubleRankingArrow.angle = 0;
        //     singleDoubleCurrentRanikingOption.setText(rankingString[0]);
        // }, this);

        // selectFirstSecondSum.OnDown.add(()=>{
        //     this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        // }, this);
        // selectFirstSecondSum.OnUp.add(()=>{
        //     this.changeStatPage(selectFirstSecondSum, firstSecondSumGroup);
        // }, this);

        //克難用的全螢幕功能
        // var trigger: Phaser.Graphics = this.game.add.graphics(0, 0, this);
        // trigger.beginFill(0xffffff, 0);
        // trigger.drawRect(0, 0, 1920, 1080);
        // trigger.endFill();
        // trigger.inputEnabled = true;
        // trigger.events.onInputDown.add(()=>{
        //     window.dispatchEvent(new CustomEvent("StartFullScreen"));
        // }, this);
        // window.addEventListener("disableTrigger", ()=>{
        //     trigger.visible = false;
        // });
    }

    /**
     * 更新用戶的名字
     * @param name 名字
     */
    public updateUserName(name: string){
        this.userNameText.setText(name);
        this.userMessageNameText.setText(name);
    }

    /**
     * 更新用戶的可用餘額
     * @param balance 餘額
     */
    public updateUserBalance(balance: number){
        this.userBalance = balance;
        this.userBalanceText.setText(this.userBalance.toFixed(2));
        this.userMessageBalanceText.setText(this.userBalance.toFixed(2));
    }

    /**
     * 更新玩家當前場次的總押注
     * @param userTotalBet 玩家的總押注
     */
    public updateUserTotalBet(userTotalBet: string){
        this.userTotalBetGameText.setText(userTotalBet);
        this.userTotalBetLobbyText.setText(userTotalBet);
    }

    /**
     * 檢查用戶押注是否成功
     * @param bet 押注
     */
    public checkUserBetIsSuccessed(bet: number){
        return bet <= this.userBalance ? true : false;
    }

    /**
     * 更新投注紀錄可查詢的年份
     * @param years 年份
     */
    public updateBetRecordYearOption(years: number[]){
        let periodsLength: number = years.length;
        for(let i: number = 0;i < 10;i++){
            this.betRecordYearOptionBG[i].visible = i < periodsLength ? true : false;
            this.betRecordYearOptionButton[i].visible = i < periodsLength ? true : false;
            this.betRecordYearOptionText[i].visible = i < periodsLength ? true : false;
            this.betRecordYearOptionText[i].setText(i < periodsLength ? years[i] + "" : "");
        }
    }
    
    /**
     * 顯示查詢的押注紀錄
     * @param data 資料
     */
    public showBetRecord(data: any[]){
        let rowLength: number = data.length;
        let formLength: number = rowLength > 7 ? rowLength : 7;
        let totalBet: number = 0;
        let totalWin: number = 0;
        this.betRecordFormScrollGroup.children.length = 0;
        this.betRecordFormScrollGroup.position.set(0);
        this.betRecordFormScrollLimit = rowLength > 7 ? -(rowLength - 7) * 79 : 0;

        for(let rowIndex: number = 0;rowIndex < formLength;rowIndex++){
            this.game.add.sprite(960, 419 + 79 * rowIndex, 'Lobby', 'BettingRecordFrame_Middle', this.betRecordFormScrollGroup).anchor.set(0.5);
            if(rowIndex < rowLength){
                let playName: string = "";
                let combination: string = "";
                let firstByte: string = data[rowIndex].str_bet_type.split("_")[0];
                let secondByte: string = data[rowIndex].str_bet_type.split("_")[1];
                switch(firstByte){
                    case "1p2":
                        if(secondByte.split("").length == 3){
                            playName = "冠亞";
                            firstByte = secondByte.split("n")[0];
                            secondByte = secondByte.split("n")[1];
                            combination = "冠" + firstByte + ", 亞" + secondByte;
                        }else{
                            playName = "冠亞和";
                            switch(secondByte){
                                case "b":
                                    combination = "大";
                                    break;
                                case "s":
                                    combination = "小";
                                    break;
                                case "o":
                                    combination = "單";
                                    break;
                                case "e":
                                    combination = "雙";
                                    break;
                                default:
                                    combination = secondByte;
                                    break;
                            }
                        }
                        break;
                    case "1p2p3":
                        playName = "冠亞季";
                        let thirdByte: string = secondByte.split("n")[2];
                        firstByte = secondByte.split("n")[0];
                        secondByte = secondByte.split("n")[1];
                        combination = "冠" + firstByte + ", 亞" + secondByte + ", 季" + thirdByte;
                        break;
                    default:
                        if(isNaN(parseInt(secondByte))){
                            playName = "大小單雙";
                            combination = "第" + firstByte + "名";
                            switch(secondByte){
                                case "b":
                                    combination += "大";
                                    break;
                                case "s":
                                    combination += "小";
                                    break;
                                case "o":
                                    combination += "單";
                                    break;
                                case "e":
                                    combination += "雙";
                                    break;
                                case "d":
                                    combination += "龍";
                                    break;
                                case "t":
                                    combination += "虎";
                                    break;
                            }
                        }else{
                            playName = "名次";
                            combination = "第" + firstByte + "名" + secondByte + "號";
                        }
                        break;
                }
                this.game.add.text(260, 422 + 79 * rowIndex, playName, {font: "42px Microsoft JhengHei", fill: "#ffffff"}, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(608, 422 + 79 * rowIndex, combination, {font: "42px Microsoft JhengHei", fill: "#ffffff"}, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(956, 422 + 79 * rowIndex, data[rowIndex].num_rate, {font: "42px Microsoft JhengHei", fill: "#ffffff"}, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(1304, 422 + 79 * rowIndex, data[rowIndex].num_bet, {font: "42px Microsoft JhengHei", fill: "#ffffff"}, this.betRecordFormScrollGroup).anchor.set(0.5);
                this.game.add.text(1652, 422 + 79 * rowIndex, (data[rowIndex].num_result > 0 ? "+" : "") + data[rowIndex].num_result, {font: "42px Microsoft JhengHei", fill: data[rowIndex].num_result > 0 ? "#00ff0d" : "#ff0019"}, this.betRecordFormScrollGroup).anchor.set(0.5);
                
                totalBet += parseFloat(data[rowIndex].num_bet);
                totalWin += parseFloat(data[rowIndex].num_result);
            }
        }
        this.betRecordTotalBetText.setText(totalBet.toFixed(4));
        this.betRecordTotalWinText.setText(totalWin.toFixed(4));
    }

    /**
     * 開始投注紀錄頁的滾動功能，若項目未超過預設顯示長度則不滾動
     */
    private startBetRecordFormScroll(){
        if(this.betRecordFormScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.betRecordFormScrollGroup.y;
        this.betRecordFormScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.betRecordFormScrollLimit){
                scrollPosY = this.betRecordFormScrollLimit;
            }
            this.betRecordFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    /**
     * 停止投注紀錄頁的滾動功能
     */
    private stopBetRecordFormScroll(){
        this.game.time.events.remove(this.betRecordFormScrollTimerEvent);
    }

    /**
     * 開啟歷史開獎頁面
     * @param data 歷史開獎資料
     */
    public openHistoricalLotteryForm(data: HistoricalLottery[]){
        if(data.length == 0){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "今日尚未開獎"}));
            return;
        }
        this.historicalLotteryGroup.visible = true;
        this.panelMask.visible = true;

        let formLength: number = data.length;
        this.historicalLotteryFormScrollGroup.children.length = 0;
        this.historicalLotteryFormScrollGroup.position.set(0);
        this.historicalLotteryFormScrollLimit = formLength > 9 ? -(formLength - 9) * 82 : 0;

        for(let i: number = 0;i < formLength;i++){
            this.game.add.sprite(960, 302 + 82 * i, 'Lobby', i < formLength - 1 ? 'HistoricalLotteryFrame_Middle' : 'HistoricalLotteryFrame_Bottom', this.historicalLotteryFormScrollGroup).anchor.set(0.5);
            this.game.add.text(100, 283 + 82 * i, data[i].periods + "期", {font: "28px Microsoft JhengHei", fill: "#ffffff"}, this.historicalLotteryFormScrollGroup);
            this.game.add.text(628, 283 + 82 * i, data[i].time, {font: "28px Microsoft JhengHei", fill: "#ffffff"}, this.historicalLotteryFormScrollGroup).anchor.set(1, 0);
            for(let j: number = 0;j < 8;j++){
                this.game.add.sprite(650 + 65 * j, 275 + 82 * i, 'Lobby', 'TodayNumber_' + (data[i].result[j]), this.historicalLotteryFormScrollGroup);
                
                if(j < 3){
                    let firstSecondSumDataString: string;
                    switch(j){
                        case 0:
                            firstSecondSumDataString = data[i].firstSecondSum + "";
                            break;
                        case 1:
                            firstSecondSumDataString = data[i].firstSecondSum > 9 ? "大" : "小";
                            break;
                        case 2:
                            firstSecondSumDataString = data[i].firstSecondSum % 2 != 0 ? "單" : "雙";
                            break;
                    }
                    this.game.add.text(1220 + 93 * j, 304 + 82 * i, firstSecondSumDataString, {font: "34px Microsoft JhengHei", fill: "#ffffff"}, this.historicalLotteryFormScrollGroup).anchor.set(0.5);
                }

                if(j < 4){
                    this.game.add.text(1485 + 94 * j, 279 + 82 * i, data[i].dragonTiger[j] == "d" ? "龍" : "虎", {font: "34px Microsoft JhengHei", fill: "#ffffff"}, this.historicalLotteryFormScrollGroup);
                }
            }
        }
    }

    /**
     * 開始歷史開獎頁的滾動功能，若項目未超過預設顯示長度則不滾動
     */
    private startHistoricalLotteryFormScroll(){
        if(this.historicalLotteryFormScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.historicalLotteryFormScrollGroup.y;
        this.historicalLotteryFormScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.historicalLotteryFormScrollLimit){
                scrollPosY = this.historicalLotteryFormScrollLimit;
            }
            this.historicalLotteryFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    /**
     * 停止投注紀錄頁的滾動功能
     */
    private stopHistoricalLotteryFormScroll(){
        this.game.time.events.remove(this.historicalLotteryFormScrollTimerEvent);
    }

    public updateMissingNumberRankingData(data: number[][][]){
        this.missingNumberRankingData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            this.missingNumberRankingData[rankIndex] = new Array(8);
            for(let rowIndex: number = 0;rowIndex < 8;rowIndex++){
                this.missingNumberRankingData[rankIndex][rowIndex] = new Array(8);
                for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                    this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] = valueIndex < 7 ? data[rankIndex][rowIndex][valueIndex] : rowIndex + 1;
                }
            }
        }
        this.changeMissingNumberRankingSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    }

    private changeMissingNumberRankingSort(rankIndex: number, sortIndex: number){
        this.changeMissingNumberArrow(sortIndex);

        this.missingNumberRankingData[rankIndex].sort((a, b)=>{
            if(b[sortIndex] == a[sortIndex]){
                return a[7] - b[7];
            }else{
                return b[sortIndex] - a[sortIndex];
            }
        });

        for(let rowIndex: number = 0;rowIndex < 8;rowIndex++){
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                if(valueIndex == 0){
                    this.missingNumberRankingDataText[rowIndex][valueIndex].setText((this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberRankingData[rankIndex][rowIndex][valueIndex]));
                    this.missingNumberRankingDataText[rowIndex][valueIndex].tint = this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] > 0 ? 0x00ff0d : 0xff0019;
                }else if(valueIndex < 7){
                    this.missingNumberRankingDataText[rowIndex][valueIndex].setText(this.missingNumberRankingData[rankIndex][rowIndex][valueIndex] + "");
                }else{
                    this.missingNumberRankingHorseNumber[rowIndex].loadTexture('Lobby', 'TodayNumber_0' + this.missingNumberRankingData[rankIndex][rowIndex][valueIndex]);
                }
            }
        }
    }

    public updateMissingNumberSingleDoubleData(data: number[][][]){
        this.missingNumberSingleDoubleData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            this.missingNumberSingleDoubleData[rankIndex] = new Array(2);
            for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
                this.missingNumberSingleDoubleData[rankIndex][rowIndex] = new Array(8);
                for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                    this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex] = valueIndex < 7 ? data[rankIndex][rowIndex][valueIndex] : rowIndex;
                }
            }
        }
        this.changeMissingNumberSingleDoubleSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    }

    private changeMissingNumberSingleDoubleSort(rankIndex: number, sortIndex: number){
        this.changeMissingNumberArrow(sortIndex);

        this.missingNumberSingleDoubleData[rankIndex].sort((a, b)=>{
            if(b[sortIndex] == a[sortIndex]){
                return a[7] - b[7];
            }else{
                return b[sortIndex] - a[sortIndex];
            }
        });

        let singleDoubleString: string[] = ["單", "雙"];
        for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                if(valueIndex == 0){
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].setText(singleDoubleString[this.missingNumberSingleDoubleData[rankIndex][rowIndex][7]]);
                }else if(valueIndex == 1){
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].setText((this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1]));
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].tint = this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }else{
                    this.missingNumberSingleDoubleDataText[rowIndex][valueIndex].setText(this.missingNumberSingleDoubleData[rankIndex][rowIndex][valueIndex - 1] + "");
                }
            }
        }
    }

    public updateMissingNumberBigSmallData(data: number[][][]){
        this.missingNumberBigSmallData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            this.missingNumberBigSmallData[rankIndex] = new Array(2);
            for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
                this.missingNumberBigSmallData[rankIndex][rowIndex] = new Array(8);
                for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                    this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex] = valueIndex < 7 ? data[rankIndex][rowIndex][valueIndex] : rowIndex;
                }
            }
        }
        this.changeMissingNumberBigSmallSort(RANKING.FIRST, MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    }

    private changeMissingNumberBigSmallSort(rankIndex: number, sortIndex: number){
        this.changeMissingNumberArrow(sortIndex);

        this.missingNumberBigSmallData[rankIndex].sort((a, b)=>{
            if(b[sortIndex] == a[sortIndex]){
                return a[7] - b[7];
            }else{
                return b[sortIndex] - a[sortIndex];
            }
        });

        let bigSmallString: string[] = ["大", "小"];
        for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                if(valueIndex == 0){
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].setText(bigSmallString[this.missingNumberBigSmallData[rankIndex][rowIndex][7]]);
                }else if(valueIndex == 1){
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].setText((this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1]));
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].tint = this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }else{
                    this.missingNumberBigSmallDataText[rowIndex][valueIndex].setText(this.missingNumberBigSmallData[rankIndex][rowIndex][valueIndex - 1] + "");
                }
            }
        }
    }

    public updateMissingNumberDragonTigerData(data: number[][]){
        this.missingNumberDragonTigerData = new Array(8);
        for(let rowIndex: number = 0;rowIndex < 8;rowIndex++){
            this.missingNumberDragonTigerData[rowIndex] = new Array(8);
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                this.missingNumberDragonTigerData[rowIndex][valueIndex] = valueIndex < 7 ? data[rowIndex][valueIndex] : rowIndex;
            }
        }
        this.changeMissingNumberDragonTigerSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    }

    private changeMissingNumberDragonTigerSort(sortIndex: number){
        this.changeMissingNumberArrow(sortIndex);

        this.missingNumberDragonTigerData.sort((a, b)=>{
            if(b[sortIndex] == a[sortIndex]){
                return a[7] - b[7];
            }else{
                return b[sortIndex] - a[sortIndex];
            }
        });

        let dragonTigerString: string[] = ["冠軍龍", "冠軍虎", "亞軍龍", "亞軍虎", "季軍龍", "季軍虎", "第4名龍", "第4名虎"];
        for(let rowIndex: number = 0;rowIndex < 8;rowIndex++){
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                if(valueIndex == 0){
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].setText(dragonTigerString[this.missingNumberDragonTigerData[rowIndex][7]]);
                }else if(valueIndex == 1){
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].setText((this.missingNumberDragonTigerData[rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberDragonTigerData[rowIndex][valueIndex - 1]));
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].tint = this.missingNumberDragonTigerData[rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }else{
                    this.missingNumberDragonTigerDataText[rowIndex][valueIndex].setText(this.missingNumberDragonTigerData[rowIndex][valueIndex - 1] + "");
                }
            }
        }
    }

    public updateMissingNumberFirstSecondSumSingleDoubleData(data: number[][]){
        this.missingNumberFirstSecondSumSingleDoubleData = new Array(8);
        for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
            this.missingNumberFirstSecondSumSingleDoubleData[rowIndex] = new Array(2);
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex] = valueIndex < 7 ? data[rowIndex][valueIndex] : rowIndex;
            }
        }
        this.changeMissingNumberFirstSecondSumSingleDoubleSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    }

    private changeMissingNumberFirstSecondSumSingleDoubleSort(sortIndex: number){
        this.changeMissingNumberArrow(sortIndex);

        this.missingNumberFirstSecondSumSingleDoubleData.sort((a, b)=>{
            if(b[sortIndex] == a[sortIndex]){
                return a[7] - b[7];
            }else{
                return b[sortIndex] - a[sortIndex];
            }
        });

        let firstSecondSumSingleDoubleString: string[] = ["冠亞和單", "冠亞和雙"];
        for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                if(valueIndex == 0){
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].setText(firstSecondSumSingleDoubleString[this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][7]]);
                }else if(valueIndex == 1){
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].setText((this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1]));
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].tint = this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }else{
                    this.missingNumberFirstSecondSumSingleDoubleDataText[rowIndex][valueIndex].setText(this.missingNumberFirstSecondSumSingleDoubleData[rowIndex][valueIndex - 1] + "");
                }
            }
        }
    }

    public updateMissingNumberFirstSecondSumBigSmallData(data: number[][]){
        this.missingNumberFirstSecondSumBigSmallData = new Array(8);
        for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
            this.missingNumberFirstSecondSumBigSmallData[rowIndex] = new Array(2);
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex] = valueIndex < 7 ? data[rowIndex][valueIndex] : rowIndex;
            }
        }
        this.changeMissingNumberFirstSecondSumBigSmallSort(MISSING_NUMBER_SORT_OPTION.TODAY_APPEAR);
    }

    private changeMissingNumberFirstSecondSumBigSmallSort(sortIndex: number){
        this.changeMissingNumberArrow(sortIndex);

        this.missingNumberFirstSecondSumBigSmallData.sort((a, b)=>{
            if(b[sortIndex] == a[sortIndex]){
                return a[7] - b[7];
            }else{
                return b[sortIndex] - a[sortIndex];
            }
        });

        let firstSecondSumBigSmallString: string[] = ["冠亞和大", "冠亞和小"];
        for(let rowIndex: number = 0;rowIndex < 2;rowIndex++){
            for(let valueIndex: number = 0;valueIndex < 8;valueIndex++){
                if(valueIndex == 0){
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].setText(firstSecondSumBigSmallString[this.missingNumberFirstSecondSumBigSmallData[rowIndex][7]]);
                }else if(valueIndex == 1){
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].setText((this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1] > 0 ? "贏" : "輸") + Math.abs(this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1]));
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].tint = this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1] > 0 ? 0x00ff0d : 0xff0019;
                }else{
                    this.missingNumberFirstSecondSumBigSmallDataText[rowIndex][valueIndex].setText(this.missingNumberFirstSecondSumBigSmallData[rowIndex][valueIndex - 1] + "");
                }
            }
        }
    }

    private changeMissingNumberArrow(sortIndex: number){
        for(let i: number = 0;i < 7;i++){
            this.missingNumberArrow[i].tint = i == sortIndex ? 0xff0019 : 0x00ff0d;
            this.missingNumberArrow[i].angle = i == sortIndex ? 0 : 180;
        }
    }

    public updateTodayNumberData(data: TodayNumber[]){
        this.todayNumberData = new Array(16);
        for(let i: number = 0;i < 16;i++){
            this.todayNumberData[i] = new Array(8);
            let dataIndex: number = Math.floor(i / 2);
            for(let j: number = 0;j < 8;j++){
                this.todayNumberData[i][j] = (i % 2 == 0) ? data[dataIndex].Win[j] : data[dataIndex].Lose[j];
                this.todayNumberDataText[i][j].setText(this.todayNumberData[i][j] + "");
                this.todayNumberDataText[i][j].tint = 0xffffff;
            }
        }
    }

    private changeTodayNumberTextColor(firstMin: number, firstMax: number, secondMin?: number, secondMax?: number, thirdMin?: number, thirdMax?: number){
        for(let i: number = 0;i < 16;i++){
            for(let j: number = 0;j < 8;j++){
                if(this.todayNumberData[i][j] >= firstMin && this.todayNumberData[i][j] <= firstMax){
                    this.todayNumberDataText[i][j].tint = 0xff0019;
                }else if(this.todayNumberData[i][j] >= secondMin && this.todayNumberData[i][j] <= secondMax){
                    this.todayNumberDataText[i][j].tint = 0x0062ff;
                }else if(this.todayNumberData[i][j] >= thirdMin && this.todayNumberData[i][j] <= thirdMax){
                    this.todayNumberDataText[i][j].tint = 0x00ff0d;
                }else{
                    this.todayNumberDataText[i][j].tint = 0xffffff;
                }
            }
        }
    }

    public updateDailyDragonBigLongDragonData(data: DailyLongDragonData[][]){
        let formLength: number[] = new Array(8);
        this.dailyDragonBigLongDragonData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            let periodsLength: number[] = new Array(7);
            this.dailyDragonBigLongDragonData[rankIndex] = new Array(7);
            for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonBigLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);

            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }

        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            for(let formIndex: number = 0;formIndex < formLength[rankIndex];formIndex++){
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonBigLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.BIG_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    }

    public updateDailyDragonSmallLongDragonData(data: DailyLongDragonData[][]){
        let formLength: number[] = new Array(8);
        this.dailyDragonSmallLongDragonData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            let periodsLength: number[] = new Array(7);
            this.dailyDragonSmallLongDragonData[rankIndex] = new Array(7);
            for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonSmallLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);

            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }

        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            for(let formIndex: number = 0;formIndex < formLength[rankIndex];formIndex++){
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonSmallLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    }

    public updateDailyDragonSingleLongDragonData(data: DailyLongDragonData[][]){
        let formLength: number[] = new Array(8);
        this.dailyDragonSingleLongDragonData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            let periodsLength: number[] = new Array(7);
            this.dailyDragonSingleLongDragonData[rankIndex] = new Array(7);
            for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonSingleLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
 
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }

        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            for(let formIndex: number = 0;formIndex < formLength[rankIndex];formIndex++){
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonSingleLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    }

    public updateDailyDragonDoubleLongDragonData(data: DailyLongDragonData[][]){
        let formLength: number[] = new Array(8);
        this.dailyDragonDoubleLongDragonData = new Array(8);
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            let periodsLength: number[] = new Array(7);
            this.dailyDragonDoubleLongDragonData[rankIndex] = new Array(7);
            for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonDoubleLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
 
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }

        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            for(let formIndex: number = 0;formIndex < formLength[rankIndex];formIndex++){
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonDoubleLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    }

    public updateDailyDragonDragonLongDragonData(data: DailyLongDragonData[][]){
        let formLength: number[] = new Array(4);
        this.dailyDragonDragonLongDragonData = new Array(4);
        for(let rankIndex: number = 0;rankIndex < 4;rankIndex++){
            let periodsLength: number[] = new Array(7);
            this.dailyDragonDragonLongDragonData[rankIndex] = new Array(7);
            for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonDragonLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
 
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
 
        for(let rankIndex: number = 0;rankIndex < 4;rankIndex++){
            for(let formIndex: number = 0;formIndex < formLength[rankIndex];formIndex++){
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonDragonLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    }

    public updateDailyDragonTigerLongDragonData(data: DailyLongDragonData[][]){
        let formLength: number[] = new Array(4);
        this.dailyDragonTigerLongDragonData = new Array(4);
        for(let rankIndex: number = 0;rankIndex < 4;rankIndex++){
            let periodsLength: number[] = new Array(7);
            this.dailyDragonTigerLongDragonData[rankIndex] = new Array(7);
            for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                periodsLength[rowIndex] = data[rankIndex][rowIndex].periods.length;
                this.dailyDragonTigerLongDragonData[rankIndex][rowIndex] = data[rankIndex][rowIndex];
            }
            formLength[rankIndex] = Math.max.apply({}, periodsLength);
 
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex].children.length = 0;
            this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex].position.set(0);
            this.dailyDragonFormScrollLimit[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex] = formLength[rankIndex] > 6 ? -(formLength[rankIndex] - 6) * 250 : 0;
        }
 
        for(let rankIndex: number = 0;rankIndex < 8;rankIndex++){
            for(let formIndex: number = 0;formIndex < formLength[rankIndex];formIndex++){
                this.game.add.sprite(334 + 250 * formIndex, 435, 'Lobby', formIndex < formLength[rankIndex] - 1 ? 'DailyDragonFrame_Center' : 'DailyDragonFrame_Right', this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex]);
                this.game.add.text(460 + 250 * formIndex, 476, (formIndex + 2) + "期", {font: "36px Microsoft JhengHei", fill: "#f2e200"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
                    this.game.add.text(460 + 250 * formIndex, 543 + 70 * rowIndex, this.dailyDragonTigerLongDragonData[rankIndex][rowIndex].periods[formIndex] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.dailyDragonFormScrollGroup[DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON][rankIndex]).anchor.set(0.5);
                }
            }
        }
    }

    private changeDailyDragonFormDateText(sortIndex: number, rankIndex: number){
        for(let rowIndex: number = 0;rowIndex < 7;rowIndex++){
            switch(sortIndex){
                case DAILY_DRAGON_PAGE.BIG_LONG_DRAGON: 
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonBigLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.SMALL_LONG_DRAGON: 
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonSmallLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.SINGLE_LONG_DRAGON: 
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonSingleLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.DOUBLE_LONG_DRAGON: 
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonDoubleLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.DRAGON_LONG_DRAGON: 
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonDragonLongDragonData[rankIndex][rowIndex].date);
                    break;
                case DAILY_DRAGON_PAGE.TIGER_LONG_DRAGON: 
                    this.dailyDragonFormDateText[rowIndex].setText(this.dailyDragonTigerLongDragonData[rankIndex][rowIndex].date);
                    break;
            }
        }
    }

    private startDailyDragonFormScroll(sortIndex: number, rankIndex: number){
        if(this.dailyDragonFormScrollLimit[sortIndex][rankIndex] == 0){
            return;
        }
        let firstTouchPosX: number = this.game.input.x;
        let scrollPosX: number = this.dailyDragonFormScrollGroup[sortIndex][rankIndex].x;
        this.dailyDragonFormScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosX -= firstTouchPosX - this.game.input.x;
            if(scrollPosX > 0){
                scrollPosX = 0;
            }
            if(scrollPosX < this.dailyDragonFormScrollLimit[sortIndex][rankIndex]){
                scrollPosX = this.dailyDragonFormScrollLimit[sortIndex][rankIndex];
            }
            this.dailyDragonFormScrollGroup[sortIndex][rankIndex].position.set(scrollPosX, 0);
        }, this);
    }

    private stopDailyDragonFormScroll(){
        this.game.time.events.remove(this.dailyDragonFormScrollTimerEvent);
    }

    public updateTwoSideFormData(data: BigSmallSingleDoubleData[][]){
        this.twoSideFormData = data;
    }

    private changeTwoSideForm(formIndex: number){
        let dataLength: number = this.twoSideFormData[formIndex].length;
        this.twoSideFormScrollGroup.children.length = 0;
        this.twoSideFormScrollGroup.position.set(0);
        this.twoSideFormScrollLimit = dataLength > 8 ? -(dataLength - 8) * 70 : 0;
 
        for(let i: number = 0;i < dataLength;i++){
            this.game.add.sprite(960, 496 + 70 * i, 'Lobby', i < dataLength - 1 ? 'TwoSideFrame_Middle' : 'TwoSideFrame_Bottom', this.twoSideFormScrollGroup).anchor.set(0.5);
            for(let j: number = 0;j < 5;j++){
                if(j == 0){
                    this.game.add.text(297, 500 + 70 * i, this.twoSideFormData[formIndex][j].date, {font: "30px Microsoft JhengHei", fill: "#ffffff"}, this.twoSideFormScrollGroup).anchor.set(0.5);
                }else{
                    this.game.add.text(670 + 333 * (j - 1), 500 + 70 * i, this.twoSideFormData[formIndex][j].values[j - 1] + "", {font: "30px Microsoft JhengHei", fill: "#ffffff"}, this.twoSideFormScrollGroup).anchor.set(0.5);
                }
            }
        }
    }

    private startTwoSideFormScroll(){
        if(this.twoSideFormScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.twoSideFormScrollGroup.y;
        this.twoSideFormScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.twoSideFormScrollLimit){
                scrollPosY = this.twoSideFormScrollLimit;
            }
            this.twoSideFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    private stopTwoSideFormScroll(){
        this.game.time.events.remove(this.twoSideFormScrollTimerEvent);
    }

    public updateSingleDoubleFormData(data: BigSmallSingleDoubleData[][]){
        this.singleDoubleFormData = data;
    }

    private changeSingleDoubleForm(formIndex: number){
        let dataLength: number = this.singleDoubleFormData[formIndex].length;
        this.singleDoubleFormScrollGroup.children.length = 0;
        this.singleDoubleFormScrollGroup.position.set(0);
        this.singleDoubleFormScrollLimit = dataLength > 8 ? -(dataLength - 8) * 70 : 0;
 
        for(let i: number = 0;i < dataLength;i++){
            this.game.add.sprite(960, 496 + 70 * i, 'Lobby', i < dataLength - 1 ? 'TwoSideFrame_Middle' : 'TwoSideFrame_Bottom', this.singleDoubleFormScrollGroup).anchor.set(0.5);
            for(let j: number = 0;j < 5;j++){
                if(j == 0){
                    this.game.add.text(297, 500 + 70 * i, this.singleDoubleFormData[formIndex][j].date, {font: "30px Microsoft JhengHei", fill: "#ffffff"}, this.singleDoubleFormScrollGroup).anchor.set(0.5);
                }else{
                    this.game.add.text(670 + 333 * (j - 1), 500 + 70 * i, this.singleDoubleFormData[formIndex][j].values[j - 1] + "", {font: "30px Microsoft JhengHei", fill: "#ffffff"}, this.singleDoubleFormScrollGroup).anchor.set(0.5);
                }
            }
        }
    }

    private startSingleDoubleFormScroll(){
        if(this.singleDoubleFormScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.singleDoubleFormScrollGroup.y;
        this.singleDoubleFormScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.singleDoubleFormScrollLimit){
                scrollPosY = this.singleDoubleFormScrollLimit;
            }
            this.singleDoubleFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    private stopSingleDoubleFormScroll(){
        this.game.time.events.remove(this.singleDoubleFormScrollTimerEvent);
    }

    public updateFirstSecondSumForm(data: BigSmallSingleDoubleData[]){
        let dataLength: number = data.length;
        this.firstSecondSumFormScrollGroup.children.length = 0;
        this.firstSecondSumFormScrollGroup.position.set(0);
        this.firstSecondSumFormScrollLimit = dataLength > 9 ? -(dataLength - 9) * 70 : 0;
 
        for(let i: number = 0;i < dataLength;i++){
            this.game.add.sprite(960, 406 + 70 * i, 'Lobby', i < dataLength - 1 ? 'FirstSecondSumFrame_Middle' : 'FirstSecondSumFrame_Bottom', this.firstSecondSumFormScrollGroup).anchor.set(0.5);
            for(let j: number = 0;j < 5;j++){
                this.game.add.text(263 + 350 * j, 410 + 70 * i, j == 0 ? data[i].date : data[i].values[j - 1] + "", {font: "36px Microsoft JhengHei", fill: "#ffffff"}, this.firstSecondSumFormScrollGroup).anchor.set(0.5);
            }
        }
    }

    private startFirstSecondSumFormScroll(){
        if(this.firstSecondSumFormScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.firstSecondSumFormScrollGroup.y;
        this.firstSecondSumFormScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.firstSecondSumFormScrollLimit){
                scrollPosY = this.firstSecondSumFormScrollLimit;
            }
            this.firstSecondSumFormScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    private stopFirstSecondSumFormScroll(){
        this.game.time.events.remove(this.firstSecondSumFormScrollTimerEvent);
    }

    private changeStatPage(targetSpriteButton: SpriteButton, targetGroup: Phaser.Group){
        this.currentSelectedStatPage.ResetButton();
        this.currentSelectedStatPage = targetSpriteButton;
        this.currentSelectedStatPage.SelectButton();

        this.currentShowGroup.visible = false;
        this.currentShowGroup = targetGroup;
        this.currentShowGroup.visible = true;
    }
}