class GameEntry{
	
	private game: Phaser.Game;

	constructor(){
		this.game = new Phaser.Game(1920, 1080, Phaser.AUTO, "", { create: this.create });
	}

	private create(){
		this.game.stage.disableVisibilityChange = true;
		if (this.game.device.android) {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        } else {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        }
		this.game.scale.pageAlignHorizontally = true;
		this.game.scale.pageAlignVertically = true;
	
		this.game.state.add("Preloader", Preloader);
		this.game.state.start("Preloader");
	}
}
new GameEntry();