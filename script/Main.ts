class Main extends Phaser.State{

    private progressBar: Phaser.Sprite;
    private loginPage: LoginPage;
    private soundManager: SoundManager;
    private lobbyManager: LobbyManager;
    private gameManager: GameManager;
    private commonWindow: CommonWindow;
    private server: Server;
    private gameId: string;
    private currentKey: string;
    private userAccount: string;
    private handlingBetPage: BET_PAGE;
    private handlingUserBet: number;
    private updateBalanceTimerEvent: Phaser.TimerEvent;
    private passwordNeedChange: boolean;

    constructor(){
        super();
    }

    preload(){
        this.game.add.image(0, 0, 'MainBG');
        
        let progressBG1: Phaser.Sprite = this.game.add.sprite(960, 925, 'Preloader', 'LoadingProgressBG_1');
        progressBG1.anchor.set(0.5);

        let progressBG2: Phaser.Sprite = this.game.add.sprite(960, 930, 'Preloader', 'LoadingProgressBG_2');
        progressBG2.anchor.set(0.5);

        let progressTitle: Phaser.Sprite = this.game.add.sprite(960, 1000, 'Preloader', 'LoadingTitle');
        progressTitle.anchor.set(0.5);
        
        this.progressBar = this.game.add.sprite(172, 930, 'Preloader', 'LoadingProgressBar');
        this.progressBar.anchor.set(0, 0.5);
        this.progressBar.scale.set(0, 1);

        this.load.onFileComplete.add(this.allFileComplete, this);
        this.load.onLoadComplete.add(this.allLoadComplete, this);
        this.load.pack('main', 'assets/pack.json');
    }

    /**
     * 全部檔案已讀取完成的進度
     * @param progress 進度數字
     */
    private allFileComplete(progress: number){
        this.progressBar.scale.set(progress * 0.01195, 1);
    }

    /**全部資源讀取完成後執行 */
    private allLoadComplete(){
        this.load.onFileComplete.remove(this.allFileComplete, this);
        this.load.onLoadComplete.remove(this.allLoadComplete, this);

        this.initialize();
    }

    private initialize(){
        this.game.plugins.add(PhaserInput.Plugin);
        this.currentKey = localStorage.getItem('gameCurrentKey');
        this.passwordNeedChange = false;

        this.loginPage = new LoginPage(this.game);
        this.loginPage.loginSignal.add((account: string, password: string)=>{
            let postData = {
                str_acc: account,
                str_pwd: password
            };
            this.server.postData(DATA_URL.MEMBER_LOGIN, JSON.stringify(postData));
        }, this);
        
        this.soundManager = new SoundManager(this.game);

        this.lobbyManager = new LobbyManager(this.game, this.soundManager);
        this.lobbyManager.betSignal.add((betPage: BET_PAGE, userBet: number, betArray: any[])=>{
            this.lobbyManager.gameBetHandling = true;
            this.handlingBetPage = betPage;
            this.handlingUserBet = userBet;
            if(this.commonWindow.checkUserBetIsSuccessed(userBet)){
                let postData = {
                    oddsPrompt: 0,
                    str_cur_page: betPage,
                    ary_bet: betArray
                };
                this.server.postData(DATA_URL.GAME_BET + this.currentKey, JSON.stringify(postData));
            }else{
                this.receiveServerError(504);
                this.gameBetResult(false);
            }
        }, this);
        this.lobbyManager.clearUserTotalBetSignal.add(()=>{
            localStorage.setItem(this.userAccount + 'userTotalBet', "0");
            this.commonWindow.updateUserTotalBet("0");
        }, this);
        this.lobbyManager.visible = false;

        this.gameManager = new GameManager(this.game, this.soundManager);
        this.gameManager.endGameSignal.add(this.endGame, this);
        this.gameManager.visible = false;

        this.commonWindow = new CommonWindow(this.game, this.soundManager);
        this.commonWindow.switchGameSceneSignal.add(this.switchGameScene, this);
        this.commonWindow.logOutSignal.add(()=>{
            this.server.getData(DATA_URL.MEMBER_LOGOUT);
        }, this);
        this.commonWindow.requestDataSignal.add((url: DATA_URL, postData?: string)=>{
            if(url == DATA_URL.PASSWORD_CHANGE){
                this.passwordNeedChange = true;
            }
            this.server.postData(url + this.gameId, postData);
        }, this);
        this.commonWindow.visible = false;
        
        this.server = new Server(this);
        this.server.getData(DATA_URL.CHECK_LOGIN);

        //測試用
        // this.server.getTestData(TEST.MISSING_NUMBER); 
        // this.server.getTestData(TEST.TODAY_NUMBER); 
        // this.server.getTestData(TEST.DAILY_LONG_DRAGON); 
        // this.server.getTestData(TEST.TWO_SIDE); 
        // this.server.getTestData(TEST.SINGLE_DOUBLE); 
        // this.server.getTestData(TEST.FIRST_SECOND_SUM); 
    }

    /**
     * 開關遊戲頁
     * @param isShowGame 遊戲頁顯示狀態
     */
    private switchGameScene(isShowGame: boolean){
        this.gameManager.visible = isShowGame;
        this.lobbyManager.visible = !isShowGame;
        this.commonWindow.gameGroup.visible = isShowGame;
        this.commonWindow.lobbyGroup.visible = !isShowGame;
    }

    /**
     * 開始遊戲
     * @param data 資料
     */
    private startGame(data: any){
        let countdownTime: number = data.objCur.intSecLeft;
        this.lobbyManager.startNextGameCountdown(countdownTime);
        this.gameManager.startNextGameCountdown(countdownTime);
        if(this.currentKey != data.objCur.strKey){
            this.currentKey = data.objCur.strKey;
            localStorage.setItem('gameCurrentKey', this.currentKey);
            localStorage.setItem(this.userAccount + 'userTotalBet', "0");
            this.commonWindow.updateUserTotalBet("0");
        }

        let resultLength: number = data.objLast.aryNum.length;
        if(resultLength > 0){
            let result: number[] = new Array(resultLength);
            for(let i: number = 0; i < resultLength; i++){
                result[i] = data.objLast.aryNum[i].split("")[1];
            }
            this.gameManager.racingStop(result);
        }
    }

    /**結束遊戲 */
    private endGame(){
        this.soundManager.playSound(SOUND.GAME_END);
    }

    /**
     * 接收處理伺服器傳來的資料
     * @param url 資料路徑
     * @param packet 資料包裹
     */
    public receiveServerData(url: string, packet: Packet){
        if(packet.code != 100){
            console.error();
            return;
        }else{
            if(this.passwordNeedChange){
                this.passwordNeedChange = this.commonWindow.panelMask.visible = this.commonWindow.passwordChangeGroup.visible = false;
                window.dispatchEvent(new CustomEvent('Success', {'detail': "密碼變更成功！請重新登入"}));
                this.goToLoginPage();
            }
        }
        
        switch(url){
            case DATA_URL.MEMBER_LOGOUT:
                this.goToLoginPage();
                break;
            case DATA_URL.MEMBER_LOGIN:
            case DATA_URL.CHECK_LOGIN:
                this.userAccount = packet.data.str_acc;
                let userTotalBet: string = localStorage.getItem(this.userAccount + 'userTotalBet') != null ? localStorage.getItem(this.userAccount + 'userTotalBet') : "0";
                this.commonWindow.updateUserTotalBet(userTotalBet);

                this.goToGame(parseFloat(packet.data.objBase.num_balance));
                this.server.getData(DATA_URL.GAME_LIST);
                break;
            case DATA_URL.GAME_LIST:
                this.gameId = packet.data[0].id;
                this.server.getData(DATA_URL.GAME_RESULT + this.gameId);
                this.server.getData(DATA_URL.GAME_INFO + this.gameId);

                //每10秒更新賠率、場次結果資料
                this.game.time.events.loop(10000, ()=>{
                    this.server.getData(DATA_URL.GAME_RESULT + this.gameId);
                    this.server.getData(DATA_URL.GAME_INFO + this.gameId);
                }, this);

                //每5秒更新總押注、最低/高押注
                this.game.time.events.loop(5000, ()=>{
                    this.server.getData(DATA_URL.BET_DATA + this.currentKey);
                }, this);
                break;
            case DATA_URL.GAME_RESULT + this.gameId:
                this.startGame(packet.data);
                break;
            case DATA_URL.GAME_INFO + this.gameId:
                this.updateGameRate(packet.data.objRate);
                break;
            case DATA_URL.GAME_BET + this.currentKey:
                this.gameBetResult(true);
                this.commonWindow.updateUserBalance(packet.data.num_balance);
                //押注成功後，要求更新總押注、最低/最高押注
                this.server.getData(DATA_URL.BET_DATA + this.currentKey);
                break;
            case DATA_URL.DAY_BET_RECORD + this.gameId:
                if(packet.data.length == 0){
                    window.dispatchEvent(new CustomEvent('Warning', {'detail': "查詢無資料"}));
                }
                this.commonWindow.showBetRecord(packet.data);
                break;
            case DATA_URL.OPEN_HISTORY + this.gameId:
                let dataLength: number = packet.data.length > 20 ? 20 : packet.data.length;
                let historicalLotteryData: HistoricalLottery[] = new Array(dataLength);
                for(let i: number = 0; i < dataLength; i++){
                    historicalLotteryData[i] = {
                        periods: packet.data[i].strIssue,
                        time: packet.data[i].timeOpen,
                        result: packet.data[i].aryNum,
                        firstSecondSum: packet.data[i].num1p2,
                        dragonTiger: packet.data[i].aryDT
                    }
                }
                this.commonWindow.openHistoricalLotteryForm(historicalLotteryData);
                break;
            case DATA_URL.BET_DATA + this.currentKey:
                this.lobbyManager.updateTotalBet(Number(packet.data.numBetTol));
                this.gameManager.updateTotalBet(Number(packet.data.numBetTol));
                this.gameManager.updateLowestAndHighestBet(Number(packet.data.numBetMin), Number(packet.data.numBetMax));
                break;
            case DATA_URL.GET_USER_INFO:
                this.commonWindow.updateUserBalance(Number(packet.data.num_balance));
                break;
            // case TEST.MISSING_NUMBER:
            //     let missingNumber: MissingNumberTest = packet.data;
            //     this.commonWindow.updateMissingNumberRankingData(missingNumber.RankingData);
            //     this.commonWindow.updateMissingNumberSingleDoubleData(missingNumber.SingleDoubleData);
            //     this.commonWindow.updateMissingNumberBigSmallData(missingNumber.BigSmallData);
            //     this.commonWindow.updateMissingNumberDragonTigerData(missingNumber.DragonTigerData);
            //     this.commonWindow.updateMissingNumberFirstSecondSumSingleDoubleData(missingNumber.FirstSecondSumSingleDoubleData);
            //     this.commonWindow.updateMissingNumberFirstSecondSumBigSmallData(missingNumber.FirstSecondSumBigSmallData);
            //     break;
            // case TEST.TODAY_NUMBER:
            //     let todayNumber: TodayNumber[] = packet.data;
            //     this.commonWindow.updateTodayNumberData(todayNumber);
            //     break;
            // case TEST.DAILY_LONG_DRAGON:
            //     let dailyLongDragon: DailyLongDragonTest = packet.data;
            //     this.commonWindow.updateDailyDragonBigLongDragonData(dailyLongDragon.BigLongDragonData);
            //     this.commonWindow.updateDailyDragonSmallLongDragonData(dailyLongDragon.SmallLongDragonData);
            //     this.commonWindow.updateDailyDragonSingleLongDragonData(dailyLongDragon.SingleLongDragonData);
            //     this.commonWindow.updateDailyDragonDoubleLongDragonData(dailyLongDragon.DoubleLongDragonData);
            //     this.commonWindow.updateDailyDragonDragonLongDragonData(dailyLongDragon.DragonLongDragonData);
            //     this.commonWindow.updateDailyDragonTigerLongDragonData(dailyLongDragon.TigerLongDragonData);
            //     break;
            // case TEST.TWO_SIDE:
            //     let twoSide: BigSmallSingleDoubleData[][] = packet.data;
            //     this.commonWindow.updateTwoSideFormData(twoSide);
            //     break;
            // case TEST.SINGLE_DOUBLE:
            //     let singleDouble: BigSmallSingleDoubleData[][] = packet.data;
            //     this.commonWindow.updateSingleDoubleFormData(singleDouble);
            //     break;
            // case TEST.FIRST_SECOND_SUM:
            //     let firstSecondSum: BigSmallSingleDoubleData[] = packet.data;
            //     this.commonWindow.updateFirstSecondSumForm(firstSecondSum);
            //     break;
        }
    }

    public receiveServerError(errorCode: number){
        let errorMessage: string = "";
        switch(errorCode){
            case 200:
                errorMessage = "無此會員";
                break;
            case 201:
                errorMessage = "原密碼錯誤";
                this.passwordNeedChange = false;
                this.commonWindow.passwordChangeGroup.visible = true;
                break;
            case 202:
                errorMessage = "無此代理商";
                break;
            case 301:
                errorMessage = "帳號格式錯誤";
                break;
            case 401:
                errorMessage = "帳號密碼輸入錯誤";
                this.goToLoginPage();
                break;
            case 500:
                errorMessage = "一般錯誤";
                break;
            case 501:
                errorMessage = "下注期數錯誤";
                break;
            case 502:
                errorMessage = "下注期數已關閉";
                break;
            case 503:
                errorMessage = "無效的下注金額";
                break;
            case 504:
                errorMessage = "餘額不足";
                break;
            case 505:
                errorMessage = "超過單注限額";
                break;
            case 506:
                errorMessage = "超過單局限額";
                break;
            case 507:
                errorMessage = "其他下注錯誤";
                break;
            case 508:
                errorMessage = "遊戲設定異常";
                break;
        }
        window.dispatchEvent(new CustomEvent('Error', {'detail': errorMessage}));
        
        if(errorCode > 500){
            this.gameBetResult(false);
        }
    }

    private gameBetResult(betStatus: boolean){
        if(betStatus){
            let userTotalBet: number = localStorage.getItem(this.userAccount + 'userTotalBet') != null ? Number(localStorage.getItem(this.userAccount + 'userTotalBet')) : 0;
            userTotalBet += this.handlingUserBet;
            localStorage.setItem(this.userAccount + 'userTotalBet', userTotalBet + "");
            this.commonWindow.updateUserTotalBet(userTotalBet + "");
        }

        switch(this.handlingBetPage){
            case BET_PAGE.RANKING:
                this.lobbyManager.rankingBetResult(betStatus);
                break;
            case BET_PAGE.BIG_SMALL_SINGLE_DOUBLE:
                this.lobbyManager.bigSmallBetResult(betStatus);
                break;
            case BET_PAGE.FIRST_SECOND_SUM:
                this.lobbyManager.firstSecondSumResult(betStatus);
                break;
            case BET_PAGE.FIRST_SECOND:
                this.lobbyManager.firstSecondResult(betStatus);
                break;
            case BET_PAGE.FIRST_SECOND_THIRD:
                this.lobbyManager.firstSecondThirdResult(betStatus);
                break;
        }
        this.lobbyManager.gameBetHandling = false;
    }

    /**轉到登入頁 */
    public goToLoginPage(){
        this.game.time.events.remove(this.updateBalanceTimerEvent);
        this.game.time.removeAll();
        this.lobbyManager.visible = false;
        this.gameManager.visible = false;
        this.commonWindow.visible = false;
        this.loginPage.visible = true;
        this.loginPage.logInAgain();
    }

    /**轉到遊戲頁面 */
    private goToGame(balance: number){
        this.commonWindow.updateUserBalance(balance);
        this.commonWindow.visible = true;
        this.switchGameScene(true);
        this.updateBalanceTimerEvent = this.game.time.events.loop(10000, ()=>{
            this.server.getData(DATA_URL.GET_USER_INFO);
        }, this);
    }

    /**
     * 更新遊戲賠率
     * @param gameRate 遊戲賠率
     */
    private updateGameRate(gameRate: any){
        let rankingRate: number[][] = new Array(8);
        let bigSmallRate: number[][] = new Array(8);
        let firstSecondSumRate: number[] = new Array(17);
        let firstSecondRate: number[][] = new Array(8);
        let firstSecondThirdRate: number[][][] = new Array(8);
        for(let i: number = 0; i < 8; i++){
            rankingRate[i] = new Array(8);
            bigSmallRate[i] = new Array(6);
            firstSecondRate[i] = new Array(8);
            firstSecondThirdRate[i] = new Array(8);
            for(let j: number = 0; j < 8; j++){
                firstSecondThirdRate[i][j] = new Array(8);
            }
        }

        Object.keys(gameRate).forEach((key: string)=>{
            let firstByte: string = key.split("_")[0];
            let secondByte: string = key.split("_")[1];
            switch(firstByte){
                case "1p2":
                    if(secondByte.split("").length == 3){
                        firstByte = secondByte.split("n")[0];
                        secondByte = secondByte.split("n")[1];
                        firstSecondRate[parseInt(firstByte) - 1][parseInt(secondByte) - 1] = gameRate[key];
                    }else{
                        switch(secondByte){
                            case "b":
                                firstSecondSumRate[13] = gameRate[key];
                                break;
                            case "s":
                                firstSecondSumRate[14] = gameRate[key];
                                break;
                            case "o":
                                firstSecondSumRate[15] = gameRate[key];
                                break;
                            case "e":
                                firstSecondSumRate[16] = gameRate[key];
                                break;
                            default:
                                firstSecondSumRate[parseInt(secondByte) - 3] = gameRate[key];
                                break;
                        }
                    }
                    break;
                case "1p2p3":
                    let thirdByte: string = secondByte.split("n")[2];
                    firstByte = secondByte.split("n")[0];
                    secondByte = secondByte.split("n")[1];
                    firstSecondThirdRate[parseInt(firstByte) - 1][parseInt(secondByte) - 1][parseInt(thirdByte) - 1] = gameRate[key];
                    break;
                default:
                    switch(secondByte){
                        case "b":
                            bigSmallRate[parseInt(firstByte) - 1][0] = gameRate[key];
                            break;
                        case "s":
                            bigSmallRate[parseInt(firstByte) - 1][1] = gameRate[key];
                            break;
                        case "o":
                            bigSmallRate[parseInt(firstByte) - 1][2] = gameRate[key];
                            break;
                        case "e":
                            bigSmallRate[parseInt(firstByte) - 1][3] = gameRate[key];
                            break;
                        case "d":
                            bigSmallRate[parseInt(firstByte) - 1][4] = gameRate[key];
                            break;
                        case "t":
                            bigSmallRate[parseInt(firstByte) - 1][5] = gameRate[key];
                            break;
                        default:
                            rankingRate[parseInt(firstByte) - 1][parseInt(secondByte) - 1] = gameRate[key];
                            break;
                    }
                    break;
            }
        });
        this.lobbyManager.updateRankingRate(rankingRate);
        this.lobbyManager.updateBigSmallRate(bigSmallRate);
        this.lobbyManager.updateFirstSecondSumRate(firstSecondSumRate);
        this.lobbyManager.updateFirstSecondRate(firstSecondRate);
        this.lobbyManager.updateFirstSecondThirdRate(firstSecondThirdRate);
    }
}