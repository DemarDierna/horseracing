enum DATA_URL{
    CHECK_LOGIN = "MemberActs/chkLogin",
    MEMBER_LOGIN = "Member/login",
    MEMBER_LOGOUT = "MemberActs/logout",
    GAME_LIST = "MemberActs/gameList",
    GAME_RESULT = "MemberActs/gameResult/",
    GAME_INFO = "MemberActs/gameInfo/",
    GAME_BET = "MemberActs/gameBet/",
    DAY_BET_RECORD = "MemberActs/dayBetRec/",
    OPEN_HISTORY = "MemberActs/openHis/",
    BET_DATA = "MemberActs/betData/",
    GET_USER_INFO = "MemberActs/userInfo",
    PASSWORD_CHANGE = "MemberActs/pwdChg/"
}

enum BET_PAGE{
    RANKING = 0,
    BIG_SMALL_SINGLE_DOUBLE,
    FIRST_SECOND_SUM,
    FIRST_SECOND,
    FIRST_SECOND_THIRD
}

enum RANKING{
    FIRST = 0,
    SECOND,
    THIRD,
    FOURTH,
    FIFTH,
    SIXTH,
    SEVENTH,
    EIGHTH,
    TOTAL
}

enum SOUND{
    WAIT_FOR_NEXT_GAME = 0,
    GAME,
    GAME_END,
    GAME_COUNTDOWN
}

enum SOUND_EFFECT{
    BUTTON_DOWN = 0
}

enum MISSING_NUMBER_PAGE{
    RANKING = 0,
    SINGLE_DOUBLE,
    BIG_SMALL,
    DRAGON_TIGER,
    FIRST_SECOND_SUM_SINGLE_DOUBLE,
    FIRST_SECOND_SUM_BIG_SMALL
}

enum MISSING_NUMBER_SORT_OPTION{
    MISSING_LOSS_WIN = 0,
    TODAY_APPEAR,
    CURRENT_MISSING,
    WEEK_MISSING,
    MOUTH_MISSING,
    HISTORICAL_RECORD_MISSING
}

enum DAILY_DRAGON_PAGE{
    BIG_LONG_DRAGON = 0,
    SMALL_LONG_DRAGON,
    SINGLE_LONG_DRAGON,
    DOUBLE_LONG_DRAGON,
    DRAGON_LONG_DRAGON,
    TIGER_LONG_DRAGON
}

interface BetRecordData{
    play: string;
    combination: string;
    pay: number;
    bet: number;
    win: number;
}

interface HistoricalLottery{
    periods: number;
    time: string;
    result: string[];
    firstSecondSum: number;
    dragonTiger: string[];
}

interface DailyLongDragonData{
    date: string;
    periods: number[];
}

interface TodayNumber{
    Win: number[];
    Lose: number[];
}

interface BigSmallSingleDoubleData{
    date: string;
    values: number[];
}

interface SoundData{
    Key: string;
    Loop: boolean;
}

interface Packet{
    code: number;
    data: any;
}



enum TEST{
    MISSING_NUMBER = "getMissingNumber",
    TODAY_NUMBER = "getTodayNumber",
    DAILY_LONG_DRAGON = "getDailyLongDragon",
    TWO_SIDE = "getTwoSide",
    SINGLE_DOUBLE = "getSingleDouble",
    FIRST_SECOND_SUM = "getFirstSecondSum"
}

interface MissingNumberTest{
    RankingData: number[][][];
    SingleDoubleData: number[][][];
    BigSmallData: number[][][];
    DragonTigerData: number[][];
    FirstSecondSumSingleDoubleData: number[][];
    FirstSecondSumBigSmallData: number[][];
}

interface DailyLongDragonTest{
    BigLongDragonData: DailyLongDragonData[][];
    SmallLongDragonData: DailyLongDragonData[][];
    SingleLongDragonData: DailyLongDragonData[][];
    DoubleLongDragonData: DailyLongDragonData[][];
    DragonLongDragonData: DailyLongDragonData[][];
    TigerLongDragonData: DailyLongDragonData[][];
}