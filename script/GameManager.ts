class GameManager extends Phaser.Group{

    public endGameSignal: Phaser.Signal;
    private isGaming: boolean;
    private isReadyToShowResult: boolean;
    private soundManager: SoundManager;
    private startGroup: Phaser.Group;
    private loopGroup: Phaser.Group;
    private endGroup: Phaser.Group;
    private topTrackLoop: Phaser.TileSprite;
    private bottomTrackLoop: Phaser.TileSprite;
    private racingLoopTimerEvent: Phaser.TimerEvent;
    private horse: Phaser.Sprite[];
    private horseGroup: Phaser.Group;
    private horsePath: number[][];
    private horseRandomPathTimerEvent: Phaser.TimerEvent;
    private horseInterpolation: number;
    private horseFrame: Phaser.Sprite;
    private lowestAndHighestBetText: Phaser.BitmapText;
    private totalBetText: Phaser.BitmapText;
    private racingCountdownSprite: Phaser.Sprite;
    private nextGameCountdownBG: Phaser.Sprite;
    private nextGameCountdownText: Phaser.BitmapText;
    private nextGameCountdownTimerEvent: Phaser.TimerEvent;
    private showRankingTimerEvent: Phaser.TimerEvent;
    private rankingDisplay: Phaser.Sprite[];

    constructor(game: Phaser.Game, soundManager: SoundManager){
        super(game);
        this.soundManager = soundManager;
        this.initialize();
    }

    private initialize(){
        this.endGameSignal = new Phaser.Signal();
        this.isGaming = false;
        this.isReadyToShowResult = false;
        this.startGroup = this.game.add.group(this);
        this.loopGroup = this.game.add.group(this);
        this.loopGroup.position.set(-1920, 0);
        this.endGroup = this.game.add.group(this);
        this.endGroup.position.set(-1880, 0);
        this.horseGroup = this.game.add.group(this);

        this.game.add.image(1920, 118, 'TopTrackLoop', null, this.startGroup).anchor.set(1, 0);
        
        this.topTrackLoop = this.game.add.tileSprite(1920, 118, 1920, 306, 'TopTrackLoop', null, this.loopGroup);
        this.topTrackLoop.anchor.set(1, 0);

        this.game.add.image(1880, 118, 'TopTrackEnd', null, this.endGroup).anchor.set(1, 0);
        this.game.add.image(1920, 395, 'BottomTrackLoop', null, this.startGroup).anchor.set(1, 0);

        this.game.add.sprite(1630, 403, 'Game', 'StartAndFinishLine_1', this.startGroup);
        this.game.add.sprite(1600, 403, 'Game', 'StartAndFinishLine_2', this.startGroup);

        this.bottomTrackLoop = this.game.add.tileSprite(1920, 395, 1920, 574, 'BottomTrackLoop', null, this.loopGroup);
        this.bottomTrackLoop.anchor.set(1, 0);

        this.game.add.image(1880, 395, 'BottomTrackEnd', null, this.endGroup).anchor.set(1, 0);

        this.game.add.sprite(1370, 550, 'Game', 'BeforeFinishLine_300M', this.endGroup);
        this.game.add.sprite(1320, 403, 'Game', 'BeforeFinishLine', this.endGroup);
        
        this.game.add.sprite(955, 550, 'Game', 'BeforeFinishLine_200M', this.endGroup);
        this.game.add.sprite(905, 403, 'Game', 'BeforeFinishLine', this.endGroup);
        
        this.game.add.sprite(550, 550, 'Game', 'BeforeFinishLine_100M', this.endGroup);
        this.game.add.sprite(500, 403, 'Game', 'BeforeFinishLine', this.endGroup);

        this.game.add.sprite(70, 403, 'Game', 'StartAndFinishLine_1', this.endGroup);
        this.game.add.sprite(40, 403, 'Game', 'StartAndFinishLine_2', this.endGroup);

        this.horse = new Array(8);
        this.horsePath = new Array(8);
        for(let i: number = 0; i < 8; i++){
            this.horse[i] = this.game.add.sprite(0, 0, 'Horse_' + (i+1), null, this.horseGroup);
            this.horse[i].anchor.set(1, 1);
            this.horse[i].animations.add('Run', [0,1,2,3,4,5,6,7], 20, true);
        }
        this.horseFrame = this.game.add.sprite(1750, 200, 'Game', 'HorseFrame', this);
        this.horseReset();

        this.game.add.image(0, 0, 'GameTopFrame', null, this);
        this.game.add.image(0, 953, 'GameBottomFrame', null, this);

        this.nextGameCountdownBG = this.game.add.sprite(960, 70, 'Game', 'CountdownBG', this);
        this.nextGameCountdownBG.anchor.set(0.5);
        this.nextGameCountdownText = this.game.add.bitmapText(960, 65, 'CountdownNumber', "", 40, this);
        this.nextGameCountdownText.anchor.set(0.5);

        this.rankingDisplay = new Array(8);
        for(let i: number = 0; i < 8; i++){
            this.rankingDisplay[i] = this.game.add.sprite(585 + i * 100, 23, 'Lobby', 'HistoricalNumber_' + (i+1), this);
            this.rankingDisplay[i].visible = false;
        }

        this.racingCountdownSprite = this.game.add.sprite(960, 540, null, null, this);
        this.racingCountdownSprite.anchor.set(0.5);

        this.game.add.text(53, 990, "最低注-最高注", {font: "bold 26px Microsoft JhengHei", fill: "#ffffff"}, this);
        this.lowestAndHighestBetText = this.game.add.bitmapText(140, 1046, 'BetNumber', "", 34, this);
        this.lowestAndHighestBetText.anchor.set(0.5);
        
        this.game.add.text(317, 990, "本場押注金額", {font: "bold 26px Microsoft JhengHei", fill: "#ffffff"}, this);
        this.totalBetText = this.game.add.bitmapText(397, 1045, 'BetNumber', "", 34, this);
        this.totalBetText.anchor.set(0.5);

        this.game.add.text(605, 990, "您的押注", {font: "bold 26px Microsoft JhengHei", fill: "#ffffff"}, this);
    }

    /**
     * 更新總押注
     * @param totalBet 總押注
     */
    public updateTotalBet(totalBet: number){
        this.totalBetText.setText(totalBet + "");
    }
    
    /**
     * 更新最高跟最低押注顯示
     * @param lowestBet 最低押注
     * @param highest 最高押注
     */
    public updateLowestAndHighestBet(lowestBet: number, highest: number){
        this.lowestAndHighestBetText.setText(lowestBet + "-" + highest);
    }

    /**
     * 開始下一場比賽的倒數
     * @param countdownTime 倒數時間
     */
    public startNextGameCountdown(countdownTime: number){
        if(this.isGaming){
            return;
        }
        this.game.time.events.remove(this.nextGameCountdownTimerEvent);
        for(let i: number = 0; i < 8; i++){
            this.rankingDisplay[i].visible = false;
        }
        if(countdownTime < 4){
            this.nextGameCountdownBG.visible = false;
            this.nextGameCountdownText.visible = false;
            this.racingPrepare(countdownTime);
        }else{
            this.nextGameCountdownBG.visible = true;
            this.nextGameCountdownText.visible = true;
            this.nextGameCountdownText.setText(countdownTime + "");
            
            this.nextGameCountdownTimerEvent = this.game.time.events.loop(1000, ()=>{
                countdownTime--;
                this.nextGameCountdownText.setText(countdownTime + "");
                if(countdownTime < 4){
                    this.game.time.events.remove(this.nextGameCountdownTimerEvent);
                    this.nextGameCountdownBG.visible = false;
                    this.nextGameCountdownText.visible = false;
                    this.racingPrepare(countdownTime);
                }
            }, this);
        }
    }

    /**
     * 比賽前準備，重置並開啟即時排名顯示
     * @param countdownTime 倒數時間
     */
    private racingPrepare(countdownTime: number){
        this.isGaming = true;
        for(let i: number = 0; i < 8; i++){
            this.rankingDisplay[i].position.set(585 + i * 100, 23);
            this.rankingDisplay[i].visible = true;
        }
        this.racingCountdown(countdownTime);
    }

    /**
     * 比賽倒數
     * @param countdownTime 倒數時間
     */
    private racingCountdown(countdownTime: number){
        this.racingCountdownSprite.loadTexture('Game', countdownTime > 0 ? 'RacingCountdown_' + countdownTime : "RacingCountdown_Flag");
        this.racingCountdownSprite.visible = true;
        this.racingCountdownSprite.alpha = 1;
        this.racingCountdownSprite.scale.set(1);
        this.game.add.tween(this.racingCountdownSprite).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, countdownTime);
        this.game.add.tween(this.racingCountdownSprite.scale).to({x: 4, y: 4}, 1000, Phaser.Easing.Linear.None, true, 0, countdownTime);
        this.soundManager.playSound(SOUND.GAME_COUNTDOWN);
        
        let countdownTimerEvent: Phaser.TimerEvent = this.game.time.events.loop(1000, ()=>{
            if(countdownTime == 0){
                this.soundManager.stopSound(SOUND.GAME_COUNTDOWN);
                this.game.time.events.remove(countdownTimerEvent);
                this.racingStart();
                this.racingCountdownSprite.visible = false;
                this.game.tweens.removeAll();
                return;
            }
            countdownTime--;
            this.racingCountdownSprite.loadTexture('Game', countdownTime > 0 ? 'RacingCountdown_' + countdownTime : "RacingCountdown_Flag");
        }, this);
    }

    /**比賽開始 */
    private racingStart(){
        this.soundManager.playSound(SOUND.GAME);
        for(let i: number = 0; i < 8; i++){
            this.horse[i].play('Run');
        }
        this.horseRandomPath();
        this.showRankingTimerEvent = this.game.time.events.loop(1000, this.updateInstantRanking, this);
        
        let racingStartTimerEvent: Phaser.TimerEvent = this.game.time.events.loop(20, ()=>{
            if(this.loopGroup.position.x < 0){
                if(this.horseFrame.position.x < 2000){
                    this.horseFrame.position.x += 20;
                }
                this.startGroup.position.x += 20;
                this.loopGroup.position.x += 20;
                this.updateHorsePosition();
            }else{
                this.game.time.events.remove(racingStartTimerEvent);
                this.loopGroup.position.set(0);
                this.horseRandomPath();
                this.racingLoop();
            }
        }, this);
    }

    /**比賽中 */
    private racingLoop(){
        this.isReadyToShowResult = true;
        this.horseRandomPathTimerEvent = this.game.time.events.loop(2000, this.horseRandomPath, this);

        this.racingLoopTimerEvent = this.game.time.events.loop(20, ()=>{
            this.topTrackLoop.tilePosition.x += 20;
            this.bottomTrackLoop.tilePosition.x += 20;
            this.updateHorsePosition();
        }, this);
    }

    /**
     * 比賽停止
     * @param result 排名結果
     */
    public racingStop(result: number[]){
        if(!this.isReadyToShowResult){
            return;
        }
        this.isReadyToShowResult = false;
        this.game.time.events.remove(this.racingLoopTimerEvent);
        this.game.time.events.remove(this.horseRandomPathTimerEvent);

        this.horsePathDesignated(result);

        let racingStopTimerEvent: Phaser.TimerEvent = this.game.time.events.loop(20, ()=>{
            this.topTrackLoop.tilePosition.x += 20;
            this.bottomTrackLoop.tilePosition.x += 20;
            this.updateHorsePosition();
            if(this.horseInterpolation == 1){
                this.game.time.events.remove(racingStopTimerEvent);
                this.connectToEndTrack();
            }
        }, this);
    }

    /**連結到最終衝刺賽道 */
    private connectToEndTrack(){
        let remainingTile: number = 1920 - this.topTrackLoop.tilePosition.x;
        let connectTimerEvent: Phaser.TimerEvent = this.game.time.events.loop(20, ()=>{
            this.topTrackLoop.tilePosition.x += 20;
            this.bottomTrackLoop.tilePosition.x += 20;
            remainingTile -= 20;
            if(remainingTile <= 0){
                this.game.time.events.remove(connectTimerEvent);
                this.topTrackLoop.tilePosition.set(0, 0);
                this.bottomTrackLoop.tilePosition.set(0, 0);
                this.racingEnd();
            }
        }, this);
    }

    /**比賽結束 */
    private racingEnd(){
        let racingEndTimerEvent: Phaser.TimerEvent = this.game.time.events.loop(20, ()=>{
            if(this.endGroup.position.x < 0){
                this.loopGroup.position.x += 20;
                this.endGroup.position.x += 20;
            }else{
                this.game.time.events.remove(racingEndTimerEvent);
                this.endGroup.position.set(0);
                this.horseToFinishLine();
            }
        }, this);
    }

    /**賽馬衝線表演 */
    private horseToFinishLine(){
        let horseToFinishLineTimerEvent: Phaser.TimerEvent = this.game.time.events.loop(20, ()=>{
            if(this.horseGroup.position.x > - 2000){
                this.horseGroup.position.x -= 20;
            }else{
                this.soundManager.stopSound(SOUND.GAME);
                this.game.time.events.remove(horseToFinishLineTimerEvent);
                this.horseGroup.position.set(-2000, 0);
                this.game.time.events.remove(this.showRankingTimerEvent);
                this.horseReset();
                this.startGroup.position.set(0);
                this.loopGroup.position.set(-1920, 0);
                this.endGroup.position.set(-1880, 0);
                this.endGameSignal.dispatch();
                this.isGaming = false;
            }
        }, this);
    }

    /**賽馬位置跟狀態重置 */
    private horseReset(){
        this.horseGroup.position.set(0);
        this.horseFrame.position.set(1750, 200);
        for(let i: number = 0; i < 8; i++){
            this.horse[i].position.set(2000, 490 + i * 70);
            this.horse[i].animations.stop("Run", true);
        }
    }

    /**賽馬路徑隨機生成 */
    private horseRandomPath(){
        for(let i: number = 0; i < 8; i++){
            this.horsePath[i] = new Array(2);
            this.horsePath[i][0] = Math.floor(this.horse[i].position.x);
            this.horsePath[i][1] = Math.floor(Math.random() * 1500) + 400;
        }
        this.horseInterpolation = 0;
    }

    /**
     * 賽馬路徑依照排名結果生成
     * @param result 號碼排名結果
     */
    private horsePathDesignated(result: number[]){
        let rankingPath: number[] = [500, 650, 800 ,950, 1100, 1250 ,1400, 1550];
        for(let i: number = 0; i < 8; i++){
            let horseNumber: number = result[i] - 1;
            this.horsePath[horseNumber] = new Array(2);
            this.horsePath[horseNumber][0] = Math.floor(this.horse[horseNumber].position.x);
            this.horsePath[horseNumber][1] = rankingPath[i];
        }
        this.horseInterpolation = 0;
    }
    
    /**更新賽馬位置 */
    private updateHorsePosition(){
        this.horseInterpolation = this.horseInterpolation > 1 ? 1 : this.horseInterpolation + 0.01;
        for(let i: number = 0; i < 8; i++){
            this.horse[i].position.set(Phaser.Math.linearInterpolation(this.horsePath[i], this.horseInterpolation), this.horse[i].position.y);
        }
    }

    /**更新即時排名 */
    private updateInstantRanking(){
        let horseInfo: any[] = new Array(8);
        for(let i: number = 0; i < 8; i++){
            horseInfo[i] = {
                Numbering: i,
                PosX: this.horse[i].position.x
            };
        }
        horseInfo = horseInfo.sort((a, b)=>{
            return a.PosX > b.PosX ? 1 : -1;
        });

        for(let i: number = 0; i < 8; i++){
            this.rankingDisplay[horseInfo[i].Numbering].position.set(585 + i * 100, 23);
        }
    }
}