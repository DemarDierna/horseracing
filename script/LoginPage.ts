class LoginPage extends Phaser.Group{
    
    public loginSignal: Phaser.Signal;
    private loginPanel: Phaser.Group;

    constructor(game: Phaser.Game){
        super(game);
        this.initialize();
    }

    private initialize(){
        this.loginSignal = new Phaser.Signal();
        this.game.add.image(0, 0, 'MainBG', null, this);
        this.loginPanel = this.game.add.group(this);
        this.loginPanel.visible = false;
        
        this.game.add.image(960, 540, 'LoginPanel', null, this.loginPanel).anchor.set(0.5);
        
        let userAccountInput: PhaserInput.InputField = this.game.add.inputField(725, 443, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 640,
            cursorColor: '#ffffff',
            textAlign: 'left',
            type: PhaserInput.InputType.text
        }, this.loginPanel);
        
        let userPasswordInput: PhaserInput.InputField = this.game.add.inputField(725, 625, {
            font: '60px Microsoft JhengHei',
            fill: '#ffffff',
            fillAlpha: 0,
            width: 640,
            cursorColor: '#ffffff',
            textAlign: 'left',
            type: PhaserInput.InputType.password
        }, this.loginPanel);

        let loginButton: SpriteButton = new SpriteButton(this.game, 960, 820, 'Button', 'LoginButton_Up', {
            Key: 'Button',
            OverFrame: 'LoginButton_Over',
            OutFrame: 'LoginButton_Up',
            UpFrame: 'LoginButton_Up',
            DownFrame: 'LoginButton_Down'
        });
        loginButton.anchor.set(0.5);
        loginButton.OnUp.add(()=>{
            if(userAccountInput.value == "" || userPasswordInput.value == ""){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "帳號及密碼不能為空"}));
                return;
            }
            this.loginSignal.dispatch(userAccountInput.value, userPasswordInput.value);
            userAccountInput.resetText();
            userPasswordInput.resetText();
            this.loginPanel.visible = false;
        }, this.loginPanel);
        this.loginPanel.add(loginButton);
    }

    /**重新登入 */
    public logInAgain(){
        this.loginPanel.visible = true;
    }
}