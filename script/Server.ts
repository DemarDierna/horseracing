class Server{

    private main: Main;

    constructor(main: Main){
        this.main = main;
    }

    public postData(url: string, data: string){
        let server: XMLHttpRequest = new XMLHttpRequest;
        server.onreadystatechange = ()=>{
            if(server.readyState === XMLHttpRequest.DONE){
                if(server.status === 200){
                    this.main.receiveServerData(url, JSON.parse(server.response));
                }else{
                    this.main.receiveServerError(server.status);
                }
            }
        }
        server.open("POST", "http://172.105.114.236/lotteryhorse/api/" + url, true);
        server.withCredentials = true;
        server.setRequestHeader("Content-type", "application/json");
        server.send(data);
    }

    public getData(url: string){
        let server: XMLHttpRequest = new XMLHttpRequest;
        server.onreadystatechange = ()=>{
            if(server.readyState === XMLHttpRequest.DONE){
                if(server.status === 200){
                    this.main.receiveServerData(url, JSON.parse(server.response));
                }else if(server.status == 400){
                    this.main.goToLoginPage();
                }
            }
        }
        server.open("GET", "http://172.105.114.236/lotteryhorse/api/" + url, true);
        server.withCredentials = true;
        server.send();
    }

    // public getTestData(url: string){
    //     let server: XMLHttpRequest = new XMLHttpRequest;
    //     server.onreadystatechange = ()=>{
    //         if(server.readyState === XMLHttpRequest.DONE){
    //             if(server.status === 200){
    //                 this.main.receiveServerData(url, JSON.parse(server.response));
    //             }
    //         }
    //     }
    //     server.open("GET", "http://192.168.1.246:5000/" + url, true);
    //     server.send();
    // }
}