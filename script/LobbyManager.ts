class LobbyManager extends Phaser.Group{
    
    public betSignal: Phaser.Signal;
    public clearUserTotalBetSignal: Phaser.Signal;
    public gameBetHandling: boolean;
    private soundManager: SoundManager;
    private nextGameCountdownText: Phaser.BitmapText;
    private nextGameCountdownTimerEvent: Phaser.TimerEvent;
    private totalBetText: Phaser.BitmapText;
    private confirmBet: SpriteButton;
    private keepBet: SpriteButton;
    private resetBet: SpriteButton;
    private currentSelectedPage: SpriteButton;
    private currentShowGroup: Phaser.Group;
    private currentChip: number;
    private chipSpriteButton: SpriteButton[];
    private chipSize: number[];
    private rankingRate: number[][];
    private rankingRateText: Phaser.Text[][];
    private rankingTempBet: number[][];
    private rankingTempBetText: Phaser.Text[][];
    private rankingConfirmBet: number[][];
    private rankingConfirmBetText: Phaser.BitmapText[][];
    private rankingLastBet: number[][];
    private bigSmallRate: number[][];
    private bigSmallRateText: Phaser.Text[][];
    private bigSmallTempBet: number[][];
    private bigSmallTempBetText: Phaser.Text[][];
    private bigSmallConfirmBet: number[][];
    private bigSmallConfirmBetText: Phaser.BitmapText[][];
    private bigSmallLastBet: number[][];
    private firstSecondSumRate: number[];
    private firstSecondSumRateText: Phaser.Text[];
    private firstSecondSumTempBet: number[];
    private firstSecondSumTempBetText: Phaser.Text[];
    private firstSecondSumConfirmBet: number[];
    private firstSecondSumConfirmBetText: Phaser.BitmapText[];
    private firstSecondSumLastBet: number[];
    private firstSecondSpriteButton: SpriteButton[][];
    private firstSecondCurrentHorseNumber: number[];
    private firstSecondLastHorseNumber: number[];
    private firstSecondLastBet: number;
    private firstSecondRate: number[][];
    private firstSecondBettingBoard: FirstSecondBettingBoard[];
    private firstSecondScrollGroup: Phaser.Group;
    private firstSecondScrollLimit: number;
    private firstSecondNewBettingBoardIndex: number;
    private firstSecondCurrentBettingBoardIndex: number;
    private firstSecondLastBettingBoardIndex: number;
    private firstSecondScrollTimerEvent: Phaser.TimerEvent;
    private firstSecondLockBet: boolean;
    private firstSecondThirdSpriteButton: SpriteButton[][];
    private firstSecondThirdCurrentHorseNumber: number[];
    private firstSecondThirdLastHorseNumber: number[];
    private firstSecondThirdLastBet: number;
    private firstSecondThirdRate: number[][][];
    private firstSecondThirdBettingBoard: FirstSecondThirdBettingBoard[];
    private firstSecondThirdScrollGroup: Phaser.Group;
    private firstSecondThirdScrollLimit: number;
    private firstSecondThirdNewBettingBoardIndex: number;
    private firstSecondThirdCurrentBettingBoardIndex: number;
    private firstSecondThirdLastBettingBoardIndex: number;
    private firstSecondThirdScrollTimerEvent: Phaser.TimerEvent;
    private firstSecondThirdLockBet: boolean;

    constructor(game: Phaser.Game, soundManager: SoundManager){
        super(game);
        this.soundManager = soundManager;
        this.initialize();
    }

    private initialize(){
        this.betSignal = new Phaser.Signal();
        this.clearUserTotalBetSignal = new Phaser.Signal();
        this.gameBetHandling = false;
        this.game.add.image(0, 0, 'MainBG', null, this);
        this.game.add.image(0, 0, 'LobbyTopFrame', null, this);
        this.game.add.image(0, 948, 'LobbyBottomFrame', null, this);
        this.game.add.image(0, 140, 'LobbyButtonBG', null, this);
        
        let rankingGroup: Phaser.Group = this.game.add.group(this);
        
        let bigSmallGroup: Phaser.Group = this.game.add.group(this);
        bigSmallGroup.visible = false;
        
        let firstSecondSumGroup: Phaser.Group = this.game.add.group(this);
        firstSecondSumGroup.visible = false;

        let firstSecondGroup: Phaser.Group = this.game.add.group(this);
        firstSecondGroup.visible = false;

        let firstSecondThirdGroup: Phaser.Group = this.game.add.group(this);
        firstSecondThirdGroup.visible = false;

        let selectRankingPage: SpriteButton = new SpriteButton(this.game, 20, 150, 'Button', 'LobbyButton_Up_1', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_1',
            OutFrame: 'LobbyButton_Up_1',
            UpFrame: 'LobbyButton_Up_1',
            DownFrame: 'LobbyButton_Down_1'
        });
        this.add(selectRankingPage);

        let selectBigSmall: SpriteButton = new SpriteButton(this.game, 401, 150, 'Button', 'LobbyButton_Up_2', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_2',
            OutFrame: 'LobbyButton_Up_2',
            UpFrame: 'LobbyButton_Up_2',
            DownFrame: 'LobbyButton_Down_2'
        });
        this.add(selectBigSmall);

        let selectFirstSecondSum: SpriteButton = new SpriteButton(this.game, 782, 150, 'Button', 'LobbyButton_Up_3', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_3',
            OutFrame: 'LobbyButton_Up_3',
            UpFrame: 'LobbyButton_Up_3',
            DownFrame: 'LobbyButton_Down_3'
        });
        this.add(selectFirstSecondSum);

        let selectFirstSecond: SpriteButton = new SpriteButton(this.game, 1163, 150, 'Button', 'LobbyButton_Up_4', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_4',
            OutFrame: 'LobbyButton_Up_4',
            UpFrame: 'LobbyButton_Up_4',
            DownFrame: 'LobbyButton_Down_4'
        });
        this.add(selectFirstSecond);

        let selectFirstSecondThird: SpriteButton = new SpriteButton(this.game, 1544, 150, 'Button', 'LobbyButton_Up_5', {
            Key: 'Button',
            OverFrame: 'LobbyButton_Up_5',
            OutFrame: 'LobbyButton_Up_5',
            UpFrame: 'LobbyButton_Up_5',
            DownFrame: 'LobbyButton_Down_5'
        });
        this.add(selectFirstSecondThird);

        let rankingTitleBG: Phaser.Sprite[] = new Array(8);
        let rankingTitle: Phaser.Text[] = new Array(8);
        let rankingName: string[] = ["冠軍", "亞軍", "季軍", "第四名", "第五名", "第六名", "第七名", "第八名"];
        let rankingSpriteButton: SpriteButton[][] = new Array(8);
        this.rankingRateText = new Array(8);
        this.rankingTempBet = new Array(8);
        this.rankingTempBetText = new Array(8);
        this.rankingConfirmBet = new Array(8);
        this.rankingConfirmBetText = new Array(8);
        this.rankingLastBet = new Array(8);
        for(let rank: number = 0; rank < 8; rank++){
            rankingTitleBG[rank] = this.game.add.sprite(20, 245 + 88 * rank, 'Lobby', 'Ranking_' + (rank+1), rankingGroup);
            rankingTitle[rank] = this.game.add.text(165, 285 + 88 * rank, rankingName[rank], {font: "bold 40px Microsoft JhengHei", fill: "#ffffff"}, rankingGroup);
            rankingTitle[rank].anchor.set(0.5);
            rankingSpriteButton[rank] = new Array(8);
            this.rankingRateText[rank] = new Array(8);
            this.rankingTempBet[rank] = new Array(8);
            this.rankingTempBetText[rank] = new Array(8);
            this.rankingConfirmBet[rank] = new Array(8);
            this.rankingConfirmBetText[rank] = new Array(8);
            this.rankingLastBet[rank] = new Array(8);
            for(let horseNumber: number = 0; horseNumber < 8; horseNumber++){
                rankingSpriteButton[rank][horseNumber] = new SpriteButton(this.game, 290 + 202 * horseNumber, 246 + 88 * rank, 'Button', 'BetAndOdds_Up_' + (horseNumber+1), {
                    Key: 'Button',
                    OverFrame: 'BetAndOdds_Up_' + (horseNumber + 1),
                    OutFrame: 'BetAndOdds_Up_' + (horseNumber + 1),
                    UpFrame: 'BetAndOdds_Up_' + (horseNumber + 1),
                    DownFrame: 'BetAndOdds_Down_' + (horseNumber + 1)
                });
                rankingSpriteButton[rank][horseNumber].OnDown.add(()=>{
                    this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this);
                rankingSpriteButton[rank][horseNumber].OnUp.add(()=>{
                    this.startRankingBet(rank, horseNumber);
                }, this);
                rankingGroup.add(rankingSpriteButton[rank][horseNumber]);

                this.rankingRateText[rank][horseNumber] = this.game.add.text(472 + 202 * horseNumber, 288 + 88 * rank, "-", {font: "24px Microsoft JhengHei", fill: "#ffffff"}, rankingGroup);
                this.rankingRateText[rank][horseNumber].anchor.set(1, 0);

                this.rankingTempBet[rank][horseNumber] = 0;
                this.rankingTempBetText[rank][horseNumber] = this.game.add.text(347 + 202 * horseNumber, 285 + 88 * rank, "", {font: "24px Microsoft JhengHei", fill: "#00fc26"}, rankingGroup);
                
                this.rankingConfirmBet[rank][horseNumber] = 0;
                this.rankingConfirmBetText[rank][horseNumber] = this.game.add.bitmapText(352 + 202 * horseNumber, 253 + 88 * rank, 'BetNumber', "0", 30, rankingGroup);

                this.rankingLastBet[rank][horseNumber] = 0;
            }
        }

        let bigSmallTitleBG: Phaser.Sprite[] = new Array(8);
        let bigSmallTitle: Phaser.Text[] = new Array(8);
        let bigSmallSpriteButton: SpriteButton[][] = new Array(8);
        let bigSmallFrameName: string[] = ["SizeBig", "SizeSmall", "Single", "Double", "Dragon", "Tiger"];
        this.bigSmallRateText = new Array(8);
        this.bigSmallTempBet = new Array(8);
        this.bigSmallTempBetText = new Array(8);
        this.bigSmallConfirmBet = new Array(8);
        this.bigSmallConfirmBetText = new Array(8);
        this.bigSmallLastBet = new Array(8);
        for(let i: number = 0; i < 8; i++){
            bigSmallTitleBG[i] = this.game.add.sprite(20, 245 + 88 * i, 'Lobby', 'Ranking_' + (i+1), bigSmallGroup);
            bigSmallTitle[i] = this.game.add.text(165, 285 + 88 * i, rankingName[i], {font: "bold 40px Microsoft JhengHei", fill: "#ffffff"}, bigSmallGroup);
            bigSmallTitle[i].anchor.set(0.5);
            bigSmallSpriteButton[i] = new Array(6);
            this.bigSmallRateText[i] = new Array(6);
            this.bigSmallTempBet[i] = new Array(6);
            this.bigSmallTempBetText[i] = new Array(6);
            this.bigSmallConfirmBet[i] = new Array(6);
            this.bigSmallConfirmBetText[i] = new Array(6);
            this.bigSmallLastBet[i] = new Array(6);
            for(let j: number = 0; j < 6; j++){
                if(i > 3 && j > 3){
                    break;
                }
                bigSmallSpriteButton[i][j] = new SpriteButton(this.game, 290 + 273 * j, 246 + 88 * i, 'Button', bigSmallFrameName[j]);
                bigSmallSpriteButton[i][j].OnDown.add(()=>{
                    this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this);
                bigSmallSpriteButton[i][j].OnUp.add(()=>{
                    this.startBigSmallBet(i, j);
                }, this);
                bigSmallGroup.add(bigSmallSpriteButton[i][j]);

                this.bigSmallRateText[i][j] = this.game.add.text(521 + 273 * j, 288 + 88 * i, "-", {font: "24px Microsoft JhengHei", fill: "#ffffff"}, bigSmallGroup);
                this.bigSmallRateText[i][j].anchor.set(1, 0);

                this.bigSmallTempBet[i][j] = 0;
                this.bigSmallTempBetText[i][j] = this.game.add.text(358 + 273 * j, 285 + 88 * i, "", {font: "24px Microsoft JhengHei", fill: "#00fc26"}, bigSmallGroup);
                
                this.bigSmallConfirmBet[i][j] = 0;
                this.bigSmallConfirmBetText[i][j] = this.game.add.bitmapText(360 + 273 * j, 254 + 88 * i, 'BetNumber', "0", 30, bigSmallGroup);
                
                this.bigSmallLastBet[i][j] = 0;
            }
        }

        let firstSecondSumSpriteButton: SpriteButton[] = new Array(17);
        let firstSecondSumFrameName: string[] = ["3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "SizeBig", "SizeSmall", "Single", "Double"];
        let spriteButtonPosX: number[] = [20, 320, 620, 920, 1220, 1520];
        let spriteButtonPosY: number[] = [246, 334, 422, 510];
        let payTextPosX: number[] = [285, 585, 885, 1185, 1485, 1785];
        let payTextPosY: number[] = [288, 376, 464, 552];
        let tempBetTextPosX: number[] = [82, 382, 682, 982, 1282, 1582];
        let tempBetTextPosY: number[] = [285, 373, 461, 549];
        let confirmBetTextPosX: number[] = [84, 384, 684, 984, 1284, 1584];
        let confirmBetTextPosY: number[] = [254, 342, 430, 518];
        let rowIndex: number = 0;
        this.firstSecondSumRateText = new Array(17);
        this.firstSecondSumTempBet = new Array(17);
        this.firstSecondSumTempBetText = new Array(17);
        this.firstSecondSumConfirmBet = new Array(17);
        this.firstSecondSumConfirmBetText = new Array(17);
        this.firstSecondSumLastBet = new Array(17);
        for(let i: number = 0; i < 17; i++){
            firstSecondSumSpriteButton[i] = new SpriteButton(this.game, spriteButtonPosX[i % 6], spriteButtonPosY[rowIndex], 'Button', "Sum_" + firstSecondSumFrameName[i]);
            firstSecondSumSpriteButton[i].OnDown.add(()=>{
                this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this);
            firstSecondSumSpriteButton[i].OnUp.add(()=>{
                this.startFirstSecondSumBet(i);
            }, this);
            firstSecondSumGroup.add(firstSecondSumSpriteButton[i]);

            this.firstSecondSumRateText[i] = this.game.add.text(payTextPosX[i % 6], payTextPosY[rowIndex], "-", {font: "24px Microsoft JhengHei", fill: "#ffffff"}, firstSecondSumGroup);
            this.firstSecondSumRateText[i].anchor.set(1, 0);

            this.firstSecondSumTempBet[i] = 0;
            this.firstSecondSumTempBetText[i] = this.game.add.text(tempBetTextPosX[i % 6], tempBetTextPosY[rowIndex], "", {font: "24px Microsoft JhengHei", fill: "#00fc26"}, firstSecondSumGroup);

            this.firstSecondSumConfirmBet[i] = 0;
            this.firstSecondSumConfirmBetText[i] = this.game.add.bitmapText(confirmBetTextPosX[i % 6], confirmBetTextPosY[rowIndex], 'BetNumber', "0", 30, firstSecondSumGroup);

            this.firstSecondSumLastBet[i] = 0;

            if(i % 6 == 5){
                rowIndex++;
            }
        }

        let firstSecondTitleBG: Phaser.Sprite[] = new Array(2);
        let firstSecondTitle: Phaser.Text[] = new Array(2);
        this.game.add.image(20, 425, 'BettingBG_1', null, firstSecondGroup);
        this.firstSecondSpriteButton = new Array(2);
        this.firstSecondCurrentHorseNumber = new Array(2);
        this.firstSecondNewBettingBoardIndex = 0;
        this.firstSecondCurrentBettingBoardIndex = 0;
        this.firstSecondLastBettingBoardIndex = 0;
        this.firstSecondLastBet = 0;
        this.firstSecondLockBet = false;
        this.firstSecondBettingBoard = new Array(56);
        this.firstSecondScrollGroup = this.game.add.group(firstSecondGroup);
        for(let i: number = 0; i < 2; i++){
            firstSecondTitleBG[i] = this.game.add.sprite(20, 245 + 88 * i, 'Lobby', 'Ranking_' + (i+1), firstSecondGroup);
            firstSecondTitle[i] = this.game.add.text(165, 285 + 88 * i, rankingName[i], {font: "bold 40px Microsoft JhengHei", fill: "#ffffff"}, firstSecondGroup);
            firstSecondTitle[i].anchor.set(0.5);
            this.firstSecondSpriteButton[i] = new Array(8);
            for(let j: number = 0; j < 8; j++){
                this.firstSecondSpriteButton[i][j] = new SpriteButton(this.game, 290 + 202 * j, 246 + 88 * i, 'Button', 'BetAndOdds_Up_' + (j+1), {
                    Key: 'Button',
                    OverFrame: 'BetAndOdds_Up_' + (j+1),
                    OutFrame: 'BetAndOdds_Up_' + (j+1),
                    UpFrame: 'BetAndOdds_Up_' + (j+1),
                    DownFrame: 'BetAndOdds_Down_' + (j+1)
                });
                this.firstSecondSpriteButton[i][j].OnDown.add(()=>{
                    this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this);
                this.firstSecondSpriteButton[i][j].OnUp.add(()=>{
                    this.changeFirstSecondSelected(i, j);
                }, this);
                firstSecondGroup.add(this.firstSecondSpriteButton[i][j]);
            }
        }
        let firstSecondMask: Phaser.Graphics = this.game.add.graphics(20, 440, firstSecondGroup);
        firstSecondMask.beginFill(0xffffff, 0);
        firstSecondMask.drawRect(0, 0, 1883, 442);
        firstSecondMask.endFill();
        firstSecondMask.inputEnabled = true;
        firstSecondMask.events.onInputDown.add(this.startFirstSecondScroll, this);
        firstSecondMask.events.onInputUp.add(this.stopFirstSecondScroll, this);
        firstSecondMask.events.onInputOut.add(this.stopFirstSecondScroll, this);
        this.firstSecondScrollGroup.mask = firstSecondMask;

        let firstSecondThirdTitleBG: Phaser.Sprite[] = new Array(3);
        let firstSecondThirdTitle: Phaser.Text[] = new Array(3);
        this.game.add.image(20, 510, 'BettingBG_2', null, firstSecondThirdGroup);
        this.firstSecondThirdSpriteButton = new Array(3);
        this.firstSecondThirdCurrentHorseNumber = new Array(3);
        this.firstSecondThirdNewBettingBoardIndex = 0;
        this.firstSecondThirdCurrentBettingBoardIndex = 0;
        this.firstSecondThirdLastBettingBoardIndex = 0;
        this.firstSecondThirdLastBet = 0;
        this.firstSecondThirdLockBet = false;
        this.firstSecondThirdBettingBoard = new Array(336);
        this.firstSecondThirdScrollGroup = this.game.add.group(firstSecondThirdGroup);
        for(let i: number = 0; i < 3; i++){
            firstSecondThirdTitleBG[i] = this.game.add.sprite(20, 245 + 88 * i, 'Lobby', 'Ranking_' + (i+1), firstSecondThirdGroup);
            firstSecondThirdTitle[i] = this.game.add.text(165, 285 + 88 * i, rankingName[i], {font: "bold 40px Microsoft JhengHei", fill: "#ffffff"}, firstSecondThirdGroup);
            firstSecondThirdTitle[i].anchor.set(0.5);
            this.firstSecondThirdSpriteButton[i] = new Array(8);
            for(let j: number = 0; j < 8; j++){
                this.firstSecondThirdSpriteButton[i][j] = new SpriteButton(this.game, 290 + 202 * j, 246 + 88 * i, 'Button', 'BetAndOdds_Up_' + (j+1), {
                    Key: 'Button',
                    OverFrame: 'BetAndOdds_Up_' + (j+1),
                    OutFrame: 'BetAndOdds_Up_' + (j+1),
                    UpFrame: 'BetAndOdds_Up_' + (j+1),
                    DownFrame: 'BetAndOdds_Down_' + (j+1)
                });
                this.firstSecondThirdSpriteButton[i][j].OnDown.add(()=>{
                    this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
                }, this);
                this.firstSecondThirdSpriteButton[i][j].OnUp.add(()=>{
                    this.changeFirstSecondThirdSelected(i, j);
                }, this);
                firstSecondThirdGroup.add(this.firstSecondThirdSpriteButton[i][j]);
            }
        }
        let firstSecondThirdMask: Phaser.Graphics = this.game.add.graphics(20, 525, firstSecondThirdGroup);
        firstSecondThirdMask.beginFill(0xffffff, 0);
        firstSecondThirdMask.drawRect(0, 0, 1883, 349);
        firstSecondThirdMask.endFill();
        firstSecondThirdMask.inputEnabled = true;
        firstSecondThirdMask.events.onInputDown.add(this.startFirstSecondThirdScroll, this);
        firstSecondThirdMask.events.onInputUp.add(this.stopFirstSecondThirdScroll, this);
        firstSecondThirdMask.events.onInputOut.add(this.stopFirstSecondThirdScroll, this);
        this.firstSecondThirdScrollGroup.mask = firstSecondThirdMask;

        this.game.add.text(630, 63, "本場總押注", {font: "bold 36px Microsoft JhengHei", fill: "#000f54"}, this);
        this.totalBetText = this.game.add.bitmapText(1023, 88, 'BetNumber', "", 46, this);
        this.totalBetText.anchor.set(0.5);

        this.game.add.text(95, 990, "您的押注", {font: "bold 26px Microsoft JhengHei", fill: "#ffffff"}, this);

        this.nextGameCountdownText= this.game.add.bitmapText(1316, 75, 'CountdownNumber', "", 30, this);
        this.nextGameCountdownText.anchor.set(0.5);

        this.chipSpriteButton = new Array(6);
        this.chipSize = [10, 50, 100, 500, 1000, 5000];
        for(let i: number = 0; i < 6; i++){
            this.chipSpriteButton[i] = new SpriteButton(this.game, 285 + 138.5 * i, 950, 'Button', 'Chip_' + this.chipSize[i]);
            this.chipSpriteButton[i].OnDown.add(()=>{
                this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
            }, this);
            this.chipSpriteButton[i].OnUp.add(()=>{
                this.changeChipSeleted(i);
            }, this);
            this.add(this.chipSpriteButton[i]);
        }

        this.confirmBet = new SpriteButton(this.game, 1135, 975, 'Button', 'ConfirmBet');
        this.add(this.confirmBet);
        
        this.keepBet = new SpriteButton(this.game, 1330, 975, 'Button', 'KeepBet');
        this.add(this.keepBet);
        
        this.resetBet = new SpriteButton(this.game, 1525, 975, 'Button', 'ResetBet');
        this.add(this.resetBet);
        
        this.currentSelectedPage = selectRankingPage;
        this.currentSelectedPage.SelectButton();
        this.currentShowGroup = rankingGroup;
        this.currentChip = 0;
        for(let i: number = 0; i < 6; i++){
            if(i == this.currentChip){
                this.chipSpriteButton[i].SelectButton();
            }else{
                this.chipSpriteButton[i].UnselectedButton();
            }
            this.chipSpriteButton[i].isSingleSeleted = true;
        }

        this.confirmBet.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.confirmBet.OnUp.add(this.confirmRankingBet, this);

        this.keepBet.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.keepBet.OnUp.add(this.keepRankingBet, this);

        this.resetBet.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        this.resetBet.OnUp.add(this.resetRankingBet, this);
        
        selectRankingPage.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectRankingPage.OnUp.add(()=>{
            this.changeBetPage(selectRankingPage, rankingGroup, BET_PAGE.RANKING);
        }, this);

        selectBigSmall.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectBigSmall.OnUp.add(()=>{
            this.changeBetPage(selectBigSmall, bigSmallGroup, BET_PAGE.BIG_SMALL_SINGLE_DOUBLE);
        }, this);

        selectFirstSecondSum.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectFirstSecondSum.OnUp.add(()=>{
            this.changeBetPage(selectFirstSecondSum, firstSecondSumGroup, BET_PAGE.FIRST_SECOND_SUM);
        }, this);

        selectFirstSecond.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectFirstSecond.OnUp.add(()=>{
            this.changeBetPage(selectFirstSecond, firstSecondGroup, BET_PAGE.FIRST_SECOND);
        }, this);

        selectFirstSecondThird.OnDown.add(()=>{
            this.soundManager.playSoundEffect(SOUND_EFFECT.BUTTON_DOWN);
        }, this);
        selectFirstSecondThird.OnUp.add(()=>{
            this.changeBetPage(selectFirstSecondThird, firstSecondThirdGroup, BET_PAGE.FIRST_SECOND_THIRD);
        }, this);
    }

    /**
     * 開始下一場比賽的倒數
     * @param countdownTime 倒數時間
     */
    public startNextGameCountdown(countdownTime: number){
        this.game.time.events.remove(this.nextGameCountdownTimerEvent);
        this.nextGameCountdownText.setText(countdownTime + "");
        if(countdownTime > 0){
            this.nextGameCountdownTimerEvent = this.game.time.events.loop(1000, ()=>{
                countdownTime--;
                this.nextGameCountdownText.setText(countdownTime + "");
                if(countdownTime == 0){
                    this.game.time.events.remove(this.nextGameCountdownTimerEvent);
                    this.clearAllPageBet();
                }
            }, this);
        }
    }

    /**
     * 更新本場全部用戶的總押注
     * @param totalBet 總押注
     */
    public updateTotalBet(totalBet: number){
        this.totalBetText.setText(totalBet + "");
    }

    /**
     * 更新名次頁的賠率
     * @param rate 賠率
     */
    public updateRankingRate(rate: number[][]){
        this.rankingRate = rate;
        for(let rank: number = 0; rank < 8; rank++){
            for(let horseNumber: number = 0; horseNumber < 8; horseNumber++){
                this.rankingRateText[rank][horseNumber].setText(rate[rank][horseNumber] + "");
            }
        }
    }

    /**
     * 依選擇的籌碼押注名次頁的選項
     * @param rank 名次
     * @param horseNumber 馬的號碼
     */
    private startRankingBet(rank: number, horseNumber: number){
        if(this.gameBetHandling){
            return;
        }
        this.rankingTempBet[rank][horseNumber] += this.chipSize[this.currentChip];
        this.rankingTempBetText[rank][horseNumber].setText("+" + this.rankingTempBet[rank][horseNumber]);
    }

    /**確認名次頁的押注 */
    private confirmRankingBet(){
        let userBet: number = 0;
        let betArray = new Array();
        for(let rank: number = 0; rank < 8; rank++){
            for(let horseNumber: number = 0; horseNumber < 8; horseNumber++){
                if(this.rankingTempBet[rank][horseNumber] > 0){
                    userBet += this.rankingTempBet[rank][horseNumber];
                    betArray.push({
                        str_type: (rank + 1) + "_" + (horseNumber + 1),
                        num_val: this.rankingTempBet[rank][horseNumber],
                        num_rate: this.rankingRate[rank][horseNumber]
                    });
                }
            }
        }
        this.betSignal.dispatch(BET_PAGE.RANKING, userBet, betArray);
    }

    /**
     * 名次頁面的押注結果
     * @param betStatus 押注狀態(成功／失敗)
     */
    public rankingBetResult(betStatus: boolean){
        if(betStatus){
            for(let rank: number = 0; rank < RANKING.TOTAL; rank++){
                for(let horseNumber: number = 0; horseNumber < 8; horseNumber++){
                    this.rankingLastBet[rank][horseNumber] = this.rankingTempBet[rank][horseNumber];
                    this.rankingConfirmBet[rank][horseNumber] += this.rankingTempBet[rank][horseNumber];
                    this.rankingConfirmBetText[rank][horseNumber].setText(this.rankingConfirmBet[rank][horseNumber] + "");
                }
            }
        }
        this.resetRankingBet();
    }

    /**續押名次頁的上一注 */
    private keepRankingBet(){
        for(let i: number = 0; i < 8; i++){
            for(let j: number = 0; j < 8; j++){
                this.rankingTempBet[i][j] = this.rankingLastBet[i][j];
                this.rankingTempBetText[i][j].setText(this.rankingLastBet[i][j] > 0 ? "+" + this.rankingLastBet[i][j] : "");
            }
        }
    }

    /**重置名次頁尚未確認的押注 */
    private resetRankingBet(){
        for(let i: number = 0; i < 8; i++){
            for(let j: number = 0; j < 8; j++){
                this.rankingTempBet[i][j] = 0;
                this.rankingTempBetText[i][j].setText("");
            }
        }
    }

    /**清除名次頁所有的押注 */
    private clearRankingBet(){
        for(let i: number = 0; i < 8; i++){
            for(let j: number = 0; j < 8; j++){
                this.rankingTempBet[i][j] = 0;
                this.rankingTempBetText[i][j].setText("");
                this.rankingConfirmBet[i][j] = 0;
                this.rankingConfirmBetText[i][j].setText("0");
            }
        }
    }

    /**
     * 更新大小單雙頁的賠率
     * @param rate 賠率
     */
    public updateBigSmallRate(rate: number[][]){
        this.bigSmallRate = rate;
        for(let rank: number = 0; rank < RANKING.TOTAL; rank++){
            for(let optionIndex: number = 0; optionIndex < 6; optionIndex++){
                if(rank > 3 && optionIndex > 3){
                    break;
                }
                this.bigSmallRateText[rank][optionIndex].setText(rate[rank][optionIndex] + "");
            }
        }
    }

    /**
     * 依選擇的籌碼押注大小單雙頁的選項
     * @param rank 名次
     * @param horseNumber 馬的號碼
     */
    private startBigSmallBet(rank: number, horseNumber: number){
        if(this.gameBetHandling){
            return;
        }
        this.bigSmallTempBet[rank][horseNumber] += this.chipSize[this.currentChip];
        this.bigSmallTempBetText[rank][horseNumber].setText("+" + this.bigSmallTempBet[rank][horseNumber]);
    }

    /**確認大小單雙頁的押注 */
    private confirmBigSmallBet(){
        //押注選項縮寫：大、小、單、雙、龍、虎
        let options: string[] = ["b","s","o","e","d","t"];
        let userBet: number = 0;
        let betArray = new Array();
        for(let rank: number = 0; rank < 8; rank++){
            for(let optionIndex: number = 0; optionIndex < 6; optionIndex++){
                if(rank > 3 && optionIndex > 3){
                    break;
                }
                if(this.bigSmallTempBet[rank][optionIndex] > 0){
                    userBet += this.bigSmallTempBet[rank][optionIndex];
                    betArray.push({
                        str_type: (rank + 1) + "_" + options[optionIndex],
                        num_val: this.bigSmallTempBet[rank][optionIndex],
                        num_rate: this.bigSmallRate[rank][optionIndex]
                    });
                }
            }
        }
        this.betSignal.dispatch(BET_PAGE.BIG_SMALL_SINGLE_DOUBLE, userBet, betArray);
    }

    /**
     * 大小單雙頁面的押注結果
     * @param betStatus 押注狀態(成功／失敗)
     */
    public bigSmallBetResult(betStatus: boolean){
        if(betStatus){
            for(let rank: number = 0; rank < 8; rank++){
                for(let optionIndex: number = 0; optionIndex < 6; optionIndex++){
                    if(rank > 3 && optionIndex > 3){
                        break;
                    }
                    this.bigSmallLastBet[rank][optionIndex] = this.bigSmallTempBet[rank][optionIndex];
                    this.bigSmallConfirmBet[rank][optionIndex] += this.bigSmallTempBet[rank][optionIndex];
                    this.bigSmallConfirmBetText[rank][optionIndex].setText(this.bigSmallConfirmBet[rank][optionIndex] + "");
                }
            }
        }
        this.resetBigSmallBet();
    }

    /**續押大小單雙頁的上一注 */
    private keepBigSmallBet(){
        for(let i: number = 0; i < 8; i++){
            for(let j: number = 0; j < 6; j++){
                if(i > 3 && j > 3){
                    break;
                }
                this.bigSmallTempBet[i][j] = this.bigSmallLastBet[i][j];
                this.bigSmallTempBetText[i][j].setText(this.bigSmallLastBet[i][j] > 0 ? "+" + this.bigSmallLastBet[i][j] : "");
            }
        }
    }

    /**重置大小單雙頁尚未確認的押注 */
    private resetBigSmallBet(){
        for(let i: number = 0; i < 8; i++){
            for(let j: number = 0; j < 6; j++){
                if(i > 3 && j > 3){
                    break;
                }
                this.bigSmallTempBet[i][j] = 0;
                this.bigSmallTempBetText[i][j].setText("");
            }
        }
    }

    /**清除大小單雙頁所有的押注 */
    private clearBigSmallBet(){
        for(let i: number = 0; i < 8; i++){
            for(let j: number = 0; j < 6; j++){
                if(i > 3 && j > 3){
                    break;
                }
                this.bigSmallTempBet[i][j] = 0;
                this.bigSmallTempBetText[i][j].setText("");
                this.bigSmallConfirmBet[i][j] = 0;
                this.bigSmallConfirmBetText[i][j].setText("0");
            }
        }
    }

    /**
     * 更新冠亞和頁的賠率
     * @param rate 賠率
     */
    public updateFirstSecondSumRate(rate: number[]){
        this.firstSecondSumRate = rate;
        for(let i: number = 0; i < 17; i++){
            this.firstSecondSumRateText[i].setText(rate[i] + "");
        }
    }

    /**
     * 依選擇的籌碼押注冠亞和頁的選項
     * @param selectIndex 選擇項目的編號
     */
    private startFirstSecondSumBet(selectIndex: number){
        if(this.gameBetHandling){
            return;
        }
        this.firstSecondSumTempBet[selectIndex] += this.chipSize[this.currentChip];
        this.firstSecondSumTempBetText[selectIndex].setText("+" + this.firstSecondSumTempBet[selectIndex]);
    }

    /**確認冠亞和頁的押注 */
    private confirmFirstSecondSumBet(){
        //押注選項縮寫：號碼和、大、小、單、雙
        let options: string[] = ["3","4","5","6","7","8","9","10","11","12","13","14","15","b","s","o","e"];
        let userBet: number = 0;
        let betArray = new Array();
        for(let i: number = 0; i < 17; i++){
            if(this.firstSecondSumTempBet[i] > 0){
                userBet += this.firstSecondSumTempBet[i];
                betArray.push({
                    str_type: "1p2_" + options[i],
                    num_val: this.firstSecondSumTempBet[i],
                    num_rate: this.firstSecondSumRate[i]
                });
            }
        }
        this.betSignal.dispatch(BET_PAGE.FIRST_SECOND_SUM, userBet, betArray);
    }

    /**
     * 冠亞和頁面的押注結果
     * @param betStatus 押注狀態(成功／失敗)
     */
    public firstSecondSumResult(betStatus: boolean){
        if(betStatus){
            for(let i: number = 0; i < 17; i++){
                this.firstSecondSumLastBet[i] = this.firstSecondSumTempBet[i];
                this.firstSecondSumConfirmBet[i] += this.firstSecondSumTempBet[i];
                this.firstSecondSumConfirmBetText[i].setText(this.firstSecondSumConfirmBet[i] + "");
            }
        }
        this.resetFirstSecondSumBet();
    }

    /**續押冠亞和頁的上一注 */
    private keepFirstSecondSumBet(){
        for(let i: number = 0; i < 17; i++){
            this.firstSecondSumTempBet[i] = this.firstSecondSumLastBet[i];
            this.firstSecondSumTempBetText[i].setText(this.firstSecondSumLastBet[i] > 0 ? "+" + this.firstSecondSumLastBet[i] : "");
        }
    }

    /**重置冠亞和頁尚未確認的押注 */
    private resetFirstSecondSumBet(){
        for(let i: number = 0; i < 17; i++){
            this.firstSecondSumTempBet[i] = 0;
            this.firstSecondSumTempBetText[i].setText("");
        }
    }

    /**清除冠亞和頁所有的押注 */
    private clearFirstSecondSumBet(){
        for(let i: number = 0; i < 17; i++){
            this.firstSecondSumTempBet[i] = 0;
            this.firstSecondSumTempBetText[i].setText("");
            this.firstSecondSumConfirmBet[i] = 0;
            this.firstSecondSumConfirmBetText[i].setText("0");
        }
    }

    /**
     * 更新冠亞頁的賠率
     * @param rate 賠率
     */
    public updateFirstSecondRate(rate: number[][]){
        this.firstSecondRate = rate;
    }

    /**
     * 更改冠亞頁當前押注的選項
     * @param rank 名次
     * @param horseNumber 馬的號碼
     */
    private changeFirstSecondSelected(rank: number, horseNumber: number){
        if(this.firstSecondLockBet){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請完成或取消當前下注"}));
            return;
        }
        if(this.firstSecondCurrentHorseNumber[rank] != undefined){
            this.firstSecondSpriteButton[rank][this.firstSecondCurrentHorseNumber[rank]].ResetButton();
        }
        this.firstSecondSpriteButton[rank][horseNumber].SelectButton();
        this.firstSecondCurrentHorseNumber[rank] = horseNumber;
    }

    /**
     * 按籌碼的編號對目前在冠亞頁選擇的項目下注
     * @param targetChip 籌碼的編號
     */
    private betFirstSecond(targetChip: number){
        if(this.gameBetHandling){
            return;
        }
        for(let i: number = 0; i < 2; i++){
            if(this.firstSecondCurrentHorseNumber[i] == undefined){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇冠軍、亞軍的馬號"}));
                return;
            }
        }

        if(this.firstSecondCurrentHorseNumber[0] == this.firstSecondCurrentHorseNumber[1]){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇不同的馬號"}));
            return;
        }

        if(this.firstSecondLockBet && this.firstSecondCurrentBettingBoardIndex != this.firstSecondLastBettingBoardIndex){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請完成或取消當前下注"}));
            return;
        }

        this.currentChip = targetChip;
        this.firstSecondLockBet = true;

        let alreadyExist: boolean = false;
        for(let i: number = 0; i < this.firstSecondNewBettingBoardIndex; i++){
            if(this.firstSecondBettingBoard[i].checkHorseNumberIsMatch(this.firstSecondCurrentHorseNumber)){
                alreadyExist = true;
                this.firstSecondCurrentBettingBoardIndex = i;
                break;
            }
        }

        if(alreadyExist){
            this.startFirstSecondBet();
        }else{
            this.newFirstSecondBettingBoard();
        }
    }
    
    /**在冠亞頁生成新的押注面板 */
    private newFirstSecondBettingBoard(){
        let rowIndex: number = Math.floor(this.firstSecondNewBettingBoardIndex / 4);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex] = new FirstSecondBettingBoard(this.game, this.firstSecondScrollGroup);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].position.set(35 + 477 * (this.firstSecondNewBettingBoardIndex % 4), 440 + 90 * rowIndex);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].updateHorseNumber(this.firstSecondCurrentHorseNumber);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].updateRate(this.firstSecondRate[this.firstSecondCurrentHorseNumber[0]][this.firstSecondCurrentHorseNumber[1]]);
        this.firstSecondBettingBoard[this.firstSecondNewBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
        this.firstSecondCurrentBettingBoardIndex = this.firstSecondNewBettingBoardIndex;
        this.firstSecondNewBettingBoardIndex++;
        this.firstSecondScrollLimit = rowIndex >= 5 ? -(rowIndex - 4) * 90 : 0;
        this.firstSecondScrollGroup.position.set(0, this.firstSecondScrollLimit);
    }

    /**在冠亞頁已生成押注面板顯示押注 */
    private startFirstSecondBet(){
        this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
    }
    
    /**確認冠亞頁的押注 */
    private confirmFirstSecondBet(){
        for(let i: number = 0; i < 2; i++){
            if(this.firstSecondCurrentHorseNumber[i] == undefined){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇冠軍、亞軍的馬號"}));
                return;
            }
        }

        if(this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex] == undefined || this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].parent == undefined){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇押注"}));
            return;
        }

        if(this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastBet() == 0){
            return;
        }

        let userBet: number = this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastBet();
        let betArray = [{
            str_type: "1p2_" + (this.firstSecondCurrentHorseNumber[0] + 1) + "n" + (this.firstSecondCurrentHorseNumber[1] + 1),
            num_val: userBet,
            num_rate: this.firstSecondRate[this.firstSecondCurrentHorseNumber[0]][this.firstSecondCurrentHorseNumber[1]]
        }];
        this.betSignal.dispatch(BET_PAGE.FIRST_SECOND, userBet, betArray);
    }

    /**
     * 冠亞頁面的押注結果
     * @param betStatus 押注狀態(成功／失敗)
     */
    public firstSecondResult(betStatus: boolean){
        if(betStatus){
            this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].updateConfirmBet();
            this.firstSecondLastBet = this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastBet();
            this.firstSecondLastHorseNumber = this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getLastHorseNumber();
            this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].clearTempBet();
            this.firstSecondLastBettingBoardIndex = this.firstSecondCurrentBettingBoardIndex;
            this.firstSecondLockBet = false;
        }else{
            this.resetFirstSecondBet();
        }
    }

    /**續押冠亞頁的上一注 */
    private keepFirstSecondBet(){
        if(this.firstSecondLastBet == 0){
            return;
        }

        if(this.firstSecondLockBet){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請完成或取消當前下注"}));
            return;
        }
        this.firstSecondLockBet = true;
        
        if(this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex] == undefined || this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].parent == undefined){
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex] = new FirstSecondBettingBoard(this.game, this.firstSecondScrollGroup);
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].position.set(35, 440);
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].updateHorseNumber(this.firstSecondLastHorseNumber);
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].keepTempBet(this.firstSecondLastBet);
            this.firstSecondNewBettingBoardIndex++;
        }else{
            this.firstSecondBettingBoard[this.firstSecondLastBettingBoardIndex].keepTempBet(this.firstSecondLastBet);
        }
        this.firstSecondCurrentBettingBoardIndex = this.firstSecondLastBettingBoardIndex;
    }

    /**重置冠亞頁尚未確認的押注 */
    private resetFirstSecondBet(){
        if(this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex] != undefined){
            if(this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].getConfirmBet() > 0){
                this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].clearTempBet();
            }else{
                this.firstSecondBettingBoard[this.firstSecondCurrentBettingBoardIndex].destroy();
                this.firstSecondNewBettingBoardIndex = this.firstSecondCurrentBettingBoardIndex;

                let rowIndex: number = Math.floor((this.firstSecondCurrentBettingBoardIndex - 1) / 4); 
                this.firstSecondScrollLimit = rowIndex >= 5 ? -(rowIndex - 4) * 90 : 0; 
                this.firstSecondScrollGroup.position.set(0, this.firstSecondScrollLimit);
            }
            this.firstSecondLockBet = false;
        }
    }

    /**清除冠亞頁所有的押注 */
    public clearFirstSecondBet(){
        this.firstSecondCurrentBettingBoardIndex = 0;
        this.firstSecondNewBettingBoardIndex = 0;
        this.firstSecondScrollGroup.children.length = 0;
        this.firstSecondScrollGroup.position.set(0); 
        this.firstSecondScrollLimit = 0;
        this.firstSecondBettingBoard = new Array(56);
        this.firstSecondLockBet = false;
    }
    
    /**開始捲動冠亞頁的押注區 */
    private startFirstSecondScroll(){
        if(this.firstSecondScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.firstSecondScrollGroup.y;
        this.firstSecondScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.firstSecondScrollLimit){
                scrollPosY = this.firstSecondScrollLimit;
            }
            this.firstSecondScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    /**停止捲動冠亞頁的押注區 */
    private stopFirstSecondScroll(){
        this.game.time.events.remove(this.firstSecondScrollTimerEvent);
    }

    /**
     * 更新冠亞季頁的賠率
     * @param rate 賠率
     */
    public updateFirstSecondThirdRate(rate: number[][][]){
        this.firstSecondThirdRate = rate;
    }

    /**
     * 更改冠亞季頁當前押注的選項
     * @param rank 名次
     * @param horseNumber 馬的號碼
     */
    private changeFirstSecondThirdSelected(rank: number, horseNumber: number){
        if(this.firstSecondThirdLockBet){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請完成或取消當前下注"}));
            return;
        }
        if(this.firstSecondThirdCurrentHorseNumber[rank] != undefined){
            this.firstSecondThirdSpriteButton[rank][this.firstSecondThirdCurrentHorseNumber[rank]].ResetButton();
        }
        this.firstSecondThirdSpriteButton[rank][horseNumber].SelectButton();
        this.firstSecondThirdCurrentHorseNumber[rank] = horseNumber;
    }

    /**
     * 按籌碼的編號對目前在冠亞季頁選擇的項目下注
     * @param targetChip 籌碼的編號
     */
    private betFirstSecondThird(targetChip: number){
        if(this.gameBetHandling){
            return;
        }
        for(let i: number = 0; i < 3; i++){
            if(this.firstSecondThirdCurrentHorseNumber[i] == undefined){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇冠軍、亞軍、季軍的馬號"}));
                return;
            }
        }

        if(this.firstSecondThirdCurrentHorseNumber[0] == this.firstSecondThirdCurrentHorseNumber[1]
        || this.firstSecondThirdCurrentHorseNumber[0] == this.firstSecondThirdCurrentHorseNumber[2]
        || this.firstSecondThirdCurrentHorseNumber[1] == this.firstSecondThirdCurrentHorseNumber[2]){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇不同的馬號"}));
            return;
        }

        if(this.firstSecondThirdLockBet && this.firstSecondThirdCurrentBettingBoardIndex != this.firstSecondThirdLastBettingBoardIndex){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請完成或取消當前下注"}));
            return;
        }

        this.currentChip = targetChip;
        this.firstSecondThirdLockBet = true;

        let alreadyExist: boolean = false;
        for(let i: number = 0; i < this.firstSecondThirdNewBettingBoardIndex; i++){
            if(this.firstSecondThirdBettingBoard[i].checkHorseNumberIsMatch(this.firstSecondThirdCurrentHorseNumber)){
                alreadyExist = true;
                this.firstSecondThirdCurrentBettingBoardIndex = i;
                break;
            }
        }

        if(alreadyExist){
            this.startFirstSecondThirdBet();
        }else{
            this.newFirstSecondThirdBettingBoard();
        }
    }
    
    /**在冠亞季頁生成新的押注面板 */
    private newFirstSecondThirdBettingBoard(){
        let rowIndex: number = Math.floor(this.firstSecondThirdNewBettingBoardIndex / 3);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex] = new FirstSecondThirdBettingBoard(this.game, this.firstSecondThirdScrollGroup);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].updateHorseNumber(this.firstSecondThirdCurrentHorseNumber);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].updateRate(this.firstSecondThirdRate[this.firstSecondThirdCurrentHorseNumber[0]][this.firstSecondThirdCurrentHorseNumber[1]][this.firstSecondThirdCurrentHorseNumber[2]]);
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
        this.firstSecondThirdCurrentBettingBoardIndex = this.firstSecondThirdNewBettingBoardIndex;
        this.firstSecondThirdBettingBoard[this.firstSecondThirdNewBettingBoardIndex].position.set(35 + 638 * (this.firstSecondThirdNewBettingBoardIndex % 3), 525 + 90 * rowIndex);
        this.firstSecondThirdNewBettingBoardIndex++;
        this.firstSecondThirdScrollLimit = rowIndex >= 4 ? -(rowIndex - 3) * 90 : 0;
        this.firstSecondThirdScrollGroup.position.set(0, this.firstSecondThirdScrollLimit);
    }

    /**在冠亞季頁已生成押注面板顯示押注 */
    private startFirstSecondThirdBet(){
        this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].showTempBet(this.chipSize[this.currentChip]);
    }
    
    /**確認冠亞季頁的押注 */
    private confirmFirstSecondThirdBet(){
        for(let i: number = 0; i < 3; i++){
            if(this.firstSecondThirdCurrentHorseNumber[i] == undefined){
                window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇冠軍、亞軍、季軍的馬號"}));
                return;
            }
        }

        if(this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex] == undefined || this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].parent == undefined){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請選擇押注"}));
            return;
        }

        if(this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastBet() == 0){
            return;
        }

        let userBet: number = this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastBet();
        let betArray = [{
            str_type: "1p2p3_" + (this.firstSecondThirdCurrentHorseNumber[0] + 1) + "n" + (this.firstSecondThirdCurrentHorseNumber[1] + 1) + "n" + (this.firstSecondThirdCurrentHorseNumber[2] + 1),
            num_val: userBet,
            num_rate: this.firstSecondThirdRate[this.firstSecondThirdCurrentHorseNumber[0]][this.firstSecondThirdCurrentHorseNumber[1]][this.firstSecondThirdCurrentHorseNumber[2]]
        }];
        this.betSignal.dispatch(BET_PAGE.FIRST_SECOND_THIRD, userBet, betArray);
    }

    /**
     * 冠亞季頁面的押注結果
     * @param betStatus 押注狀態(成功／失敗)
     */
    public firstSecondThirdResult(betStatus: boolean){
        if(betStatus){
            this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].updateConfirmBet();
            this.firstSecondThirdLastBet = this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastBet();
            this.firstSecondThirdLastHorseNumber = this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getLastHorseNumber();
            this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].clearTempBet();
            this.firstSecondThirdLastBettingBoardIndex = this.firstSecondThirdCurrentBettingBoardIndex;
            this.firstSecondThirdLockBet = false;
        }else{
            this.resetFirstSecondThirdBet();
        }
    }

    /**續押冠亞季頁的上一注 */
    private keepFirstSecondThirdBet(){
        if(this.firstSecondThirdLastBet == 0){
            return;
        }

        if(this.firstSecondThirdLockBet){
            window.dispatchEvent(new CustomEvent('Warning', {'detail': "請完成或取消當前下注"}));
            return; 
        } 
        this.firstSecondThirdLockBet = true;

        if(this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex] == undefined || this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].parent == undefined){
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex] = new FirstSecondThirdBettingBoard(this.game, this.firstSecondThirdScrollGroup);
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].position.set(35, 525);
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].updateHorseNumber(this.firstSecondThirdLastHorseNumber);
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].keepTempBet(this.firstSecondThirdLastBet);
            this.firstSecondThirdNewBettingBoardIndex++;
        }else{
            this.firstSecondThirdBettingBoard[this.firstSecondThirdLastBettingBoardIndex].keepTempBet(this.firstSecondThirdLastBet);
        }
        this.firstSecondThirdCurrentBettingBoardIndex = this.firstSecondThirdLastBettingBoardIndex;
    }

    /**重置冠亞季頁尚未確認的押注 */
    private resetFirstSecondThirdBet(){
        if(this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex] != undefined){
            if(this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].getConfirmBet() > 0){
                this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].clearTempBet();
            }else{
                this.firstSecondThirdBettingBoard[this.firstSecondThirdCurrentBettingBoardIndex].destroy();
                this.firstSecondThirdNewBettingBoardIndex = this.firstSecondThirdCurrentBettingBoardIndex;

                let rowIndex: number = Math.floor((this.firstSecondThirdCurrentBettingBoardIndex - 1) / 3);
                this.firstSecondThirdScrollLimit = rowIndex >= 4 ? -(rowIndex - 3) * 90 : 0;
                this.firstSecondThirdScrollGroup.position.set(0, this.firstSecondThirdScrollLimit);
            }
            this.firstSecondThirdLockBet = false;
        }
    }

    /**清除冠亞季頁所有的押注 */
    public clearFirstSecondThirdBet(){
        this.firstSecondThirdCurrentBettingBoardIndex = 0;
        this.firstSecondThirdNewBettingBoardIndex = 0;
        this.firstSecondThirdScrollGroup.children.length = 0;
        this.firstSecondThirdScrollGroup.position.set(0);
        this.firstSecondThirdScrollLimit = 0;
        this.firstSecondThirdBettingBoard = new Array(336);
        this.firstSecondThirdLockBet = false;
    }
    
    /**開始捲動冠亞季頁的押注區 */
    private startFirstSecondThirdScroll(){
        if(this.firstSecondThirdScrollLimit == 0){
            return;
        }
        let firstTouchPosY: number = this.game.input.y;
        let scrollPosY: number = this.firstSecondThirdScrollGroup.y;
        this.firstSecondThirdScrollTimerEvent = this.game.time.events.loop(50, ()=>{
            scrollPosY -= firstTouchPosY - this.game.input.y;
            if(scrollPosY > 0){
                scrollPosY = 0;
            }
            if(scrollPosY < this.firstSecondThirdScrollLimit){
                scrollPosY = this.firstSecondThirdScrollLimit;
            }
            this.firstSecondThirdScrollGroup.position.set(0, scrollPosY);
        }, this);
    }

    /**停止捲動冠亞季頁的押注區 */
    private stopFirstSecondThirdScroll(){
        this.game.time.events.remove(this.firstSecondThirdScrollTimerEvent);
    }

    /**清除所有頁面的押注 */
    private clearAllPageBet(){
        this.gameBetHandling = false;
        this.clearUserTotalBetSignal.dispatch();
        this.clearRankingBet();
        this.clearBigSmallBet();
        this.clearFirstSecondSumBet();
        this.clearFirstSecondBet();
        this.clearFirstSecondThirdBet();
    }

    /**更改當前顯示的押注頁 */
    private changeBetPage(targetSpriteButton: SpriteButton, targetGroup: Phaser.Group, targetBetPage: BET_PAGE){
        this.currentSelectedPage.ResetButton();
        this.currentSelectedPage = targetSpriteButton;
        this.currentSelectedPage.SelectButton();

        this.currentShowGroup.visible = false;
        this.currentShowGroup = targetGroup;
        this.currentShowGroup.visible = true;
        
        this.confirmBet.OnUp.removeAll();
        this.keepBet.OnUp.removeAll();
        this.resetBet.OnUp.removeAll();

        switch(targetBetPage){
            case BET_PAGE.RANKING:
                this.confirmBet.OnUp.add(this.confirmRankingBet, this);
                this.keepBet.OnUp.add(this.keepRankingBet, this);
                this.resetBet.OnUp.add(this.resetRankingBet, this);
                break;
            case BET_PAGE.BIG_SMALL_SINGLE_DOUBLE:
                this.confirmBet.OnUp.add(this.confirmBigSmallBet, this);
                this.keepBet.OnUp.add(this.keepBigSmallBet, this);
                this.resetBet.OnUp.add(this.resetBigSmallBet, this);
                break;
            case BET_PAGE.FIRST_SECOND_SUM:
                this.confirmBet.OnUp.add(this.confirmFirstSecondSumBet, this);
                this.keepBet.OnUp.add(this.keepFirstSecondSumBet, this);
                this.resetBet.OnUp.add(this.resetFirstSecondSumBet, this);
                break;
            case BET_PAGE.FIRST_SECOND:
                this.confirmBet.OnUp.add(this.confirmFirstSecondBet, this);
                this.keepBet.OnUp.add(this.keepFirstSecondBet, this);
                this.resetBet.OnUp.add(this.resetFirstSecondBet, this);
                break;
            case BET_PAGE.FIRST_SECOND_THIRD:
                this.confirmBet.OnUp.add(this.confirmFirstSecondThirdBet, this);
                this.keepBet.OnUp.add(this.keepFirstSecondThirdBet, this);
                this.resetBet.OnUp.add(this.resetFirstSecondThirdBet, this);
                break;
        }

        for(let i: number = 0; i < 6; i++){
            this.chipSpriteButton[i].OnUp.removeAll();
            if(targetBetPage < 3){
                this.chipSpriteButton[i].OnUp.add(()=>{
                    this.changeChipSeleted(i);
                }, this);
            }else if(targetBetPage < 4){
                this.chipSpriteButton[i].OnUp.add(()=>{
                    this.betFirstSecond(i);
                }, this);
            }else{
                this.chipSpriteButton[i].OnUp.add(()=>{
                    this.betFirstSecondThird(i);
                }, this);
            }
        }

        if(targetBetPage < 3){
            for(let i: number = 0; i < 6; i++){
                if(i == this.currentChip){
                    this.chipSpriteButton[i].SelectButton();
                }else{
                    this.chipSpriteButton[i].UnselectedButton();
                }
                this.chipSpriteButton[i].isSingleSeleted = true;
            }
        }else{
            for(let i: number = 0; i < 6; i++){
                this.chipSpriteButton[i].ResetButton();
                this.chipSpriteButton[i].isSingleSeleted = false;
            }
        }
    }

    /**更改當前選擇的籌碼編號 */
    private changeChipSeleted(targetChip: number){
        this.chipSpriteButton[this.currentChip].UnselectedButton();
        this.currentChip = targetChip;
        this.chipSpriteButton[this.currentChip].SelectButton();
    }
}