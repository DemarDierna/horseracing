class SpriteButton extends Phaser.Sprite {
    /**移到按鈕上的事件 */
    public OnOver: Phaser.Signal = new Phaser.Signal();
    /**移出按鈕的事件 */
    public OnOut: Phaser.Signal = new Phaser.Signal();
    /**按下按鈕的事件 */
    public OnDown: Phaser.Signal = new Phaser.Signal();
    /**放開按鈕的事件 */
    public OnUp: Phaser.Signal = new Phaser.Signal();
    /**長按按鈕的事件 */
    public OnLongClick: Phaser.Signal = new Phaser.Signal();
    /**長按的間隔時間 */
    public LongClickTimer: Phaser.TimerEvent;
    /**是否正在長按 */
    public isLongClicking: boolean = false;
    /**是否允許長按，預設為false */
    public EnableLongClick: Boolean = false;
    /**按鈕的圖片設定 */
    private SpriteButtonStyle: SpriteButtonStyle;
    /**是否為單選設定 */
    public isSingleSeleted: boolean = false;

    constructor(game: Phaser.Game, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number, SpriteButtonStyle?: SpriteButtonStyle) {
        super(game, x, y, key, frame);
        this.inputEnabled = true;
        this.input.useHandCursor = true;
        this.SpriteButtonStyle = SpriteButtonStyle;
        this.events.onInputOver.add(this.onInputOver, this);
        this.events.onInputOut.add(this.onInputOut, this);
        this.events.onInputDown.add(this.onInputDown, this);
        this.events.onInputUp.add(this.onInputUp, this);
    }

    /**移入按鈕的事件 */
    public onInputOver(){
        if(this.isSingleSeleted){
            return;
        }
        if(this.SpriteButtonStyle != undefined){
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.OverFrame);
        }else{
            this.tint = 0xbababa;
        }
        this.OnOver.dispatch(this);
    }

    /**移出按鈕的事件 */
    public onInputOut(){
        if(this.isSingleSeleted){
            return;
        }
        if(this.SpriteButtonStyle != undefined){
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.OutFrame);
        }else{
            this.tint = 0xffffff;
        }
        this.OnOut.dispatch(this);
    }

    /**按下按鈕的事件 */
    public onInputDown() {
        if (this.EnableLongClick) {
            this.LongClickTimer = this.game.time.events.add(1000, this.StartLongClick, this);
        }

        if(this.SpriteButtonStyle != undefined){
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.DownFrame);
        }else{
            this.tint = 0x666666;
        }
        this.OnDown.dispatch(this);
    }

    /**放開按鈕的事件 */
    public onInputUp() {
        if(this.SpriteButtonStyle != undefined){
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.UpFrame);
        }else{
            this.tint = 0xffffff;
        }

        this.game.time.events.remove(this.LongClickTimer);

        if (this.isLongClicking) {
            this.isLongClicking = false;
        } else {
            this.OnUp.dispatch(this);
        }
    }

    /**開始長按 */
    private StartLongClick() {
        this.game.time.events.remove(this.LongClickTimer);
        this.isLongClicking = true;
        this.LongClickTimer = this.game.time.events.loop(60, this.LongClick, this);
    }

    /**點擊長按事件回傳 */
    private LongClick() {
        this.OnLongClick.dispatch(this);
    }

    /**選擇的狀態 */
    public SelectButton(){
        if(this.SpriteButtonStyle == undefined){
            this.tint = 0xffffff;
        }else{
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.DownFrame);
        }
        this.inputEnabled = false;
        this.input.useHandCursor = false;
    }

    /**未被選擇的狀態 */
    public UnselectedButton(){
        if(this.SpriteButtonStyle == undefined){
            this.tint = 0x949494;
        }else{
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.DownFrame);
        }
        this.inputEnabled = true;
        this.input.useHandCursor = true;
    }

    /**重設按鈕的狀態和圖片 */
    public ResetButton() {
        this.game.time.events.remove(this.LongClickTimer);
        if(this.SpriteButtonStyle == undefined){
            this.tint = 0xffffff;
        }else{
            this.loadTexture(this.SpriteButtonStyle.Key ,this.SpriteButtonStyle.UpFrame);
        }
        this.inputEnabled = true;
        this.input.useHandCursor = true;
    }

    public ButtonEnable(value: boolean) {
        this.game.time.events.remove(this.LongClickTimer);
        if (value) {
            this.input.useHandCursor = this.inputEnabled = true;
            this.tint = 0xffffff;
        } else {
            this.ResetButton();
            this.inputEnabled = false;
            this.tint = 0x666666;
        }
    }
}

interface SpriteButtonStyle {
    Key: string;
    OverFrame: string;
    OutFrame: string;
    UpFrame: string;
    DownFrame: string;
}