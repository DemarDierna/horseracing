class Preloader extends Phaser.State{

    constructor(){
        super();
    }

    preload(){
        this.load.onLoadComplete.add(this.preloadComplete, this);
        this.load.pack('preloader', 'assets/pack.json');
    }

    /**讀取頁的資源預載完成後執行 */
    private preloadComplete(){
        this.load.onLoadComplete.remove(this.preloadComplete, this);
        
        this.game.state.add("Main", Main);
        this.game.state.start("Main", true, false);
    }
}