class SoundManager{

    private game: Phaser.Game;
    private sound: Phaser.Sound[];
    private soundEffect: Phaser.Sound[];

    constructor(game: Phaser.Game){
        this.game = game;
        this.initialize();
    }

    private initialize(){
        let soundData: SoundData[] = this.game.cache.getJSON('SoundKey').Sound;
        let soundDataLength: number = soundData.length;
        this.sound = new Array(soundDataLength);
        for(let i: number = 0; i < soundDataLength; i++){
            this.sound[i] = this.game.add.audio(soundData[i].Key, 1, soundData[i].Loop);
        }

        let soundEffectData: SoundData[] = this.game.cache.getJSON('SoundKey').SoundEffect;
        let soundEffectDataLength: number = soundEffectData.length;
        this.soundEffect = new Array(soundEffectDataLength);
        for(let i: number = 0; i < soundEffectDataLength; i++){
            this.soundEffect[i] = this.game.add.audio(soundEffectData[i].Key, 1, soundEffectData[i].Loop);
        }
    }

    public playSound(soundIndex: SOUND){
        this.sound[soundIndex].play();
    }

    public stopSound(soundIndex: SOUND){
        this.sound[soundIndex].stop();
    }

    public changeSoundVolume(newVolume: number){
        let soundLength: number = this.sound.length;
        for(let i: number = 0; i < soundLength; i++){
            this.sound[i].volume = newVolume;
        }
    }

    public playSoundEffect(soundEffectIndex: SOUND_EFFECT){
        this.soundEffect[soundEffectIndex].play();
    }

    public stopSoundEffect(soundEffectIndex: SOUND_EFFECT){
        this.soundEffect[soundEffectIndex].stop();
    }

    public changeSoundEffectVolume(newVolume: number){
        let soundEffectLength: number = this.soundEffect.length;
        for(let i: number = 0; i < soundEffectLength; i++){
            this.soundEffect[i].volume = newVolume;
        }
    }
}